/**
<Overview>
 
Copyright 2017 Metadata Technology North America Inc.
 
This software is the result of collaborative efforts from all participants of the C2Metadata project (http://www.c2metadata.org)
 
C2Metadata is supported by the Data Infrastructure Building Blocks (DIBBs) program of the National Science Foundation through grant NSF ACI-1640575.
 
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 
**/
package us.mtna;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import us.mtna.config.ComparisonConfig;
import us.mtna.pojo.DataSet;
import us.mtna.reporting.ComparisonResult;
import us.mtna.reporting.DataSetComparisonManager;
import us.mtna.reporting.Report;

public class DSCMTest extends DataSets {
	
	Report report = new Report();
	List<ComparisonResult> varComparisonresultList;
	List<ComparisonResult> classComparisonResultList;
	ComparisonConfig configuration = new ComparisonConfig();

	@Before
	public void setUp() {
		super.setUp();
		DataSet ds1 = setUpDataSetOne();
		DataSet ds2 = setUpDataSetTwo();
		DataSetComparisonManager cds = new DataSetComparisonManager();
		report = cds.compareDataSets(ds1, ds2, configuration);
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(SerializationFeature.WRITE_NULL_MAP_VALUES, false);

		try {
			System.out.println(mapper.writeValueAsString(report)); //null key for a map not allowed in json
		} catch (Exception e) {
			e.printStackTrace();
		}
		varComparisonresultList = report.getVarComparisonResultList();
		classComparisonResultList = report.getClassComparisonResultList();
	}

	@Test
	public void testingCorrectNumberOfClassComparisonResultsInList() {
		int size = classComparisonResultList.size();
		assertEquals(1, size);
	}

	@Test
	public void testingCorrectNumberOfVarComparisonResultsinList() {
		int size = varComparisonresultList.size();
		assertEquals(4, size); 
		//one match, 2 no target, 1 no source 
	}

	@Test
	public void testNumberOfChildResults() {
		if(!classComparisonResultList.isEmpty()){
		ComparisonResult cr = classComparisonResultList.get(0);
		int childResults = cr.getChildResults().size();
		//code count att + 3 code results(2 match, 1 no source)
		
		assertEquals(4, childResults);
		}
	}

	@Test
	public void testSrcVsTargetAtt() {
		boolean srcMatchesTarg = true;
		for (ComparisonResult cr : varComparisonresultList) {
			if(cr.getSourceAttribute() != null && cr.getTargetAttribute() !=null){
			if (cr.getSourceAttribute().getValue() == cr.getTargetAttribute().getValue()) {
				srcMatchesTarg = true;
			}
			} else {
				srcMatchesTarg = false;
				break;
			}
		}
		assertFalse(srcMatchesTarg);
	}

}

package us.mtna;

import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import us.mtna.updater.parser.DataFileDescription;
import us.mtna.updater.parser.ParserParameters;
import us.mtna.updater.parser.ParserInput;

public class ParserInputSandbox {
	@Test
	public void test() throws JsonProcessingException {
		ObjectMapper mapper = new ObjectMapper();
		mapper.findAndRegisterModules();

		DataFileDescription dd = new DataFileDescription();
		dd.setDdiXmlFile("ddixml");
		dd.setFileNameDdi("filenameddi");
		// dd.setVariables(variables);
		dd.setInputFileName("inputfile");
//		ParserParameters params = new ParserParameters();
//		params.setPython("pythonstring");
//
//		params.addDataFileDescriptions(dd);
//
//		ParserInput input = new ParserInput();
//		input.setParameters(params);
//
//		System.out.println(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(input));

	}

}

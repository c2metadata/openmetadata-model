/**
<Overview>
 
Copyright 2017 Metadata Technology North America Inc.
 
This software is the result of collaborative efforts from all participants of the C2Metadata project (http://www.c2metadata.org)
 
C2Metadata is supported by the Data Infrastructure Building Blocks (DIBBs) program of the National Science Foundation through grant NSF ACI-1640575.
 
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 
**/
package us.mtna.reporting;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;

import us.mtna.pojo.Classification;
import us.mtna.pojo.Code;

public class ClassificationComparisonTest {

	private Classification source;
	private Classification targetOne;
	private Classification targetTwo;
	private Classification targetLower;

	@Before
	public void setUp() {
		source = new Classification();
		targetOne = new Classification();
		targetTwo = new Classification();
		targetLower = new Classification();

		Code oneMale = new Code();
		oneMale.setCodeValue("01");
		oneMale.setLabel("Male");

		Code twoFemale = new Code();
		twoFemale.setCodeValue("02");
		twoFemale.setLabel("Female");

		Code nineUn = new Code();
		nineUn.setCodeValue("09");
		nineUn.setLabel("Unknown");

		Code oneMaleLower = new Code();
		oneMaleLower.setCodeValue("01");
		oneMaleLower.setLabel("male");

		Code twoFemaleLower = new Code();
		twoFemaleLower.setCodeValue("02");
		twoFemaleLower.setLabel("Fe male");

		Code nineUnLower = new Code();
		nineUnLower.setCodeValue("09");
		nineUnLower.setLabel("un known");

		Code oneFemale = new Code();
		oneFemale.setCodeValue("01");
		oneFemale.setLabel("Female");

		Code twoMale = new Code();
		twoMale.setCodeValue("02");
		twoMale.setLabel("Male");

		Code ninetyNineUn = new Code();
		ninetyNineUn.setCodeValue("99");
		ninetyNineUn.setLabel("Unknown");

		source.addCodesToCodeList(new ArrayList<>(Arrays.asList(new Code[] { oneMale, twoFemale, nineUn })));
		targetOne.addCodesToCodeList(new ArrayList<>(Arrays.asList(new Code[] { oneFemale, twoMale, nineUn })));
		targetTwo.addCodesToCodeList(new ArrayList<>(Arrays.asList(new Code[] { oneMale, twoFemale, ninetyNineUn })));
		targetLower.addCodesToCodeList(new ArrayList<>(Arrays.asList(new Code[] { oneMaleLower, twoFemaleLower, nineUnLower})));
	}

	@Test
	public void compareSourceAndTargetOne() {
		DataSetComparisonManager dscm = new DataSetComparisonManager();
		ComparisonResult classResult = dscm.compareCodes(source, targetOne);

		// 1 for the size comparsion and 1 for each code comparison
		assertTrue(classResult.getChildResults().size() == 4);
		assertTrue(classResult.getChildResults().get(0).getType() == ResultType.MATCH);
		assertTrue(classResult.getChildResults().get(1).getType() == ResultType.MISMATCH);
		assertTrue(classResult.getChildResults().get(2).getType() == ResultType.MISMATCH);
		assertTrue(classResult.getChildResults().get(3).getType() == ResultType.MATCH);
	}

	@Test
	public void compareSourceAndTargetTwo() {
		DataSetComparisonManager dscm = new DataSetComparisonManager();
		ComparisonResult classResult = dscm.compareCodes(source, targetTwo);

		// 1 for the size comparsion and 1 for each code comparison
		assertEquals(5, classResult.getChildResults().size());
		assertTrue(classResult.getChildResults().get(0).getType() == ResultType.MATCH);
		assertTrue(classResult.getChildResults().get(1).getType() == ResultType.MATCH);
		assertTrue(classResult.getChildResults().get(2).getType() == ResultType.MATCH);
		assertTrue(classResult.getChildResults().get(3).getType() == ResultType.NO_TARGET);
		assertTrue(classResult.getChildResults().get(4).getType() == ResultType.NO_SOURCE);
	}

	@Test
	public void compareSourceAndTargetLower() {
		DataSetComparisonManager dscm = new DataSetComparisonManager();
		ComparisonResult classResult = dscm.compareCodes(source, targetLower);

		// 1 for the size comparsion and 1 for each code comparison
		assertEquals(4, classResult.getChildResults().size());
		assertTrue(classResult.getChildResults().get(0).getType() == ResultType.MATCH);
		assertTrue(classResult.getChildResults().get(1).getType() == ResultType.CASE_MISMATCH);
		assertTrue(classResult.getChildResults().get(2).getType() == ResultType.SPACE_MISMATCH);
		assertTrue(classResult.getChildResults().get(3).getType() == ResultType.MISMATCH);
	}

}

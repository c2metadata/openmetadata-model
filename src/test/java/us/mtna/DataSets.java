/**
<Overview>
 
Copyright 2017 Metadata Technology North America Inc.
 
This software is the result of collaborative efforts from all participants of the C2Metadata project (http://www.c2metadata.org)
 
C2Metadata is supported by the Data Infrastructure Building Blocks (DIBBs) program of the National Science Foundation through grant NSF ACI-1640575.
 
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 
**/
package us.mtna;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import us.mtna.pojo.Classification;
import us.mtna.pojo.Code;
import us.mtna.pojo.DataSet;
import us.mtna.pojo.DataType;
import us.mtna.pojo.Datum;
import us.mtna.pojo.Metadata;
import us.mtna.pojo.Record;
import us.mtna.pojo.Variable;

/**
 * Two programmatically-created data sets used for testing
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public class DataSets{  
											
	private DataSet dataSetOne;
	private DataSet dataSetTwo;
	
	public void setUp(){
		setUpDataSetOne(); 
		setUpDataSetTwo();
	}

	public DataSet setUpDataSetOne() { 
		Metadata md1 = new Metadata();
		DataSet dataSetOne = new DataSet();
		dataSetOne.setDatasetId("12345");
		ArrayList<Code> codesList = new ArrayList<>();
		ArrayList<Code> codesList2 = new ArrayList<>();

		Map<String, Classification> classifs = new LinkedHashMap<>();
		ArrayList<Classification> classificationList = new ArrayList<>();
		ArrayList<Variable> variablesList = new ArrayList<>();
		ArrayList<Datum> datumListOne = new ArrayList<>();
		ArrayList<Datum> datumListTwo = new ArrayList<>();
		ArrayList<Record> recordsList = new ArrayList<>();

		Classification classification1 = new Classification();
		Classification classification2 = new Classification();
		
		Code code1 = new Code();
		code1.setLabel("Yes");
		code1.setCodeValue("1");
		codesList.add(code1);
		codesList2.add(code1);
		
		Code code2 = new Code();
		code2.setLabel("No");
		code2.setCodeValue("2");
		codesList.add(code2);
		codesList2.add(code2);
		
		Code code3 = new Code();
		code3.setCodeValue("0");
		code3.setLabel("Not Specified");
		codesList.add(code3);
	
		classification1.addCodesToCodeList(codesList);		
		classification2.addCodesToCodeList(codesList2);
		
		Variable variableWeight = new Variable();
		variableWeight.setId("3");
		variableWeight.setName("Weight");
		variableWeight.setDataType(DataType.NUMBER);
		variableWeight.setClassification(classification2);
		variablesList.add(variableWeight); 

		Variable variableHeight = new Variable();
		variableHeight.setId("0");
		variableHeight.setName("Height");
		variableHeight.setDataType(DataType.NUMBER);
		variableHeight.setClassification(classification2);
		variablesList.add(variableHeight);
		
		Variable variableCirc = new Variable();
		variableCirc.setId("4");
		variableCirc.setName("Circumference");
		variableCirc.setDataType(DataType.NUMBER);
		variablesList.add(variableCirc);

		Record recordOne = new Record();
		Datum datumA = new Datum();
		datumA.setValue(34);
		datumA.setVariable(variableHeight);
		datumListOne.add(datumA);

		Datum datumB = new Datum();
		datumB.setValue(11);
		datumB.setVariable(variableWeight);
		datumListOne.add(datumB);
		recordOne.setDatumList(datumListOne);

		Record recordTwo = new Record();
		Datum datumC = new Datum();
		datumC.setValue(9);
		datumC.setVariable(variableHeight);
		datumListTwo.add(datumC);

		Datum datumD = new Datum();
		datumD.setValue(43);
		datumD.setVariable(variableWeight);
		datumListTwo.add(datumD);
		recordTwo.setDatumList(datumListTwo);
		recordsList.add(recordTwo);

		classifs.put(classification1.getId(), classification1);
		classifs.put(classification2.getId(), classification2);
		classificationList.add(classification1);
		classificationList.add(classification2);
		md1.setVariables(variablesList);
		//md1.setClassifs(classificationList);
		md1.setClassifs(classifs);
		//dataSetOne.setRecords(recordsList);
		dataSetOne.setMetadata(md1);
		
		return dataSetOne;
	}

	public DataSet setUpDataSetTwo() {
		DataSet dataSetTwo = new DataSet();
		dataSetTwo.setDatasetId("01234");
		Metadata md2 = new Metadata();
		ArrayList<Code> codesList = new ArrayList<>();
		ArrayList<Classification> classificationList = new ArrayList<>();
		ArrayList<Variable> variablesListTwo = new ArrayList<>();
		ArrayList<Datum> datumListThree = new ArrayList<>();
		ArrayList<Datum> datumListFour = new ArrayList<>();
		ArrayList<Record> recordsListTwo = new ArrayList<>();
		Map<String, Classification> classifs = new LinkedHashMap<>();
		
		Classification classification2 = new Classification();
		Code code4 = new Code();
		code4.setLabel("Yes");
		code4.setCodeValue("1");
		codesList.add(code4);
	
		Code code5 = new Code();
		code5.setLabel("No");
		code5.setCodeValue("2");
		codesList.add(code5);
		
		Code code6 = new Code();
		code6.setCodeValue("0");
		code6.setLabel("Not Specified");
		codesList.add(code6);
		
		classification2.addCodesToCodeList(codesList);
		classificationList.add(classification2);
		classifs.put(classification2.getId(), classification2);
		md2.setClassifs(classifs);

		Variable variableWeight2 = new Variable();
		variableWeight2.setId("7");
		variableWeight2.setName("Weight");
		variableWeight2.setDataType(DataType.NUMBER);
		variableWeight2.setClassification(classification2);
		variablesListTwo.add(variableWeight2);

		Variable variableLength = new Variable();
		variableLength.setId("1");
		variableLength.setName("Length");
		variableLength.setDataType(DataType.NUMBER);
		variableLength.setClassification(classification2);
		variablesListTwo.add(variableLength);

		Record recordThree = new Record();

		Datum datumE = new Datum();
		datumE.setValue(34);
		datumE.setVariable(variableLength);
		datumListThree.add(datumE);

		Datum datumF = new Datum();
		datumF.setValue(11);
		datumF.setVariable(variableWeight2);
		datumListThree.add(datumF);

		Record recordFour = new Record();
		Datum datumG = new Datum();
		datumG.setValue(1);
		datumG.setVariable(variableLength);
		datumListFour.add(datumG);

		Datum datumH = new Datum();
		datumH.setValue(3);
		datumH.setVariable(variableWeight2);
		datumListFour.add(datumH);

		classifs.put(classification2.getId(), classification2);
		md2.setClassifs(classifs);
		md2.setVariables(variablesListTwo);
		recordThree.setDatumList(datumListThree);
		recordFour.setDatumList(datumListFour);
		recordsListTwo.add(recordThree);
		recordsListTwo.add(recordFour);
		//dataSetTwo.setRecords(recordsListTwo);
		dataSetTwo.setMetadata(md2);

		return dataSetTwo;
	}

	public DataSet getDataSetOne() {
		return dataSetOne;
	}

	public void setDataSetOne(DataSet dataSetOne) {
		this.dataSetOne = dataSetOne;
	}

	public DataSet getDataSetTwo() {
		return dataSetTwo;
	}

	public DataSet setDataSetTwo(DataSet dataSetTwo) {
		return this.dataSetTwo = dataSetTwo;
	}

}

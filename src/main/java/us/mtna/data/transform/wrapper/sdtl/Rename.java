/**
<Overview>
 
Copyright 2017 Metadata Technology North America Inc.
 
This software is the result of collaborative efforts from all participants of the C2Metadata project (http://www.c2metadata.org)
 
C2Metadata is supported by the Data Infrastructure Building Blocks (DIBBs) program of the National Science Foundation through grant NSF ACI-1640575.
 
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 
**/
package us.mtna.data.transform.wrapper.sdtl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.c2metadata.sdtl.pojo.RenamePair;
import org.c2metadata.sdtl.pojo.command.TransformBase;
import org.c2metadata.sdtl.pojo.expression.VariableSymbolExpression;

import us.mtna.data.transform.command.SelectsVariables;
import us.mtna.data.transform.command.UpdatesVariables;
import us.mtna.data.transform.command.object.Range;
import us.mtna.data.transform.command.object.VariableNamePair;
import us.mtna.pojo.DataType;

public class Rename implements SelectsVariables, UpdatesVariables {

	private org.c2metadata.sdtl.pojo.command.Rename sdtl;

	public Rename(org.c2metadata.sdtl.pojo.command.Rename sdtl) {
		this.sdtl = sdtl;
	}

	@Override
	public ValidationResult validate() {
		ValidationResult result = new ValidationResult();
		if (sdtl.getRenames() == null || sdtl.getRenames().length < 1) {
			result.setValid(false);
			result.addMessages(
					"No RenamePairs found in the SDTL when trying to execute command [" + sdtl.getSourceInformation().getOriginalSourceText() + "] ");
		}
		return result;
	}

	@Override
	public TransformBase getOriginalCommand() {
		return sdtl;
	}

	@Override
	public List<Range> getRanges() {
		return new ArrayList<>();
	}

	/**
	 * The Rename SDTL needs to have at least one {@link RenamePair} for the
	 * correct variables to be selected for renaming.
	 * 
	 */
	@Override
	public Set<String> getVariables() {
		Set<String> vars = new HashSet<>();
		if (sdtl.getRenames() != null) {
			for (RenamePair rp : sdtl.getRenames()) {
				VariableSymbolExpression vse = rp.getOldVariable();
				vars.add(vse.getVariableName());
			}
		}
		return vars;
	}

	/**
	 * The Rename SDTL needs to have at least one {@link RenamePair} for any
	 * variables to be updated.
	 */
	@Override
	public VariableNamePair[] getUpdatedVariables() {
		List<VariableNamePair> pairs = new ArrayList<>();
		VariableNamePair pair;
		if (sdtl.getRenames() != null) {
			for (RenamePair rp : sdtl.getRenames()) {
				VariableSymbolExpression newVar = rp.getNewVariable();
				VariableSymbolExpression oldVar = rp.getOldVariable();

				pair = new VariableNamePair(oldVar.getVariableName(), newVar.getVariableName());
				pairs.add(pair);
			}
		}
		VariableNamePair[] arr = new VariableNamePair[pairs.size()];
		arr = pairs.toArray(arr);
		return arr;
	}

	@Override
	public DataType getDataType() {
		// intentionally null
		return null;
	}

}

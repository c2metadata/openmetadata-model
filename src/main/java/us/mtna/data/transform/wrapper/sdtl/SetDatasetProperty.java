package us.mtna.data.transform.wrapper.sdtl;

import org.c2metadata.sdtl.pojo.command.TransformBase;

import us.mtna.data.transform.command.ds.UpdatesDataset;

//updates dataset interface?
public class SetDatasetProperty implements UpdatesDataset{

	private final org.c2metadata.sdtl.pojo.command.SetDatasetProperty sdtl;
	
	public SetDatasetProperty(org.c2metadata.sdtl.pojo.command.SetDatasetProperty sdtl) {
		this.sdtl = sdtl;
	}

	@Override
	public TransformBase getOriginalCommand() {
		return sdtl;
	}

	@Override
	public ValidationResult validate() {
		ValidationResult validation = new ValidationResult();
		if (sdtl.getPropertyName() == null) {
			validation.setValid(false);
			validation.addMessages("No propertyName found on the SetDatasetProperty command ["+sdtl.getSourceInformation().getOriginalSourceText()+"].");
		} else if (sdtl.getValue() == null) {
			validation.setValid(false);
			validation.addMessages("No value found on the SetDatasetProperty command. ["+sdtl.getSourceInformation().getOriginalSourceText()+"].");
		}
		return validation;
	}

	@Override
	public String getUpdatedProperty() {
		//TODO add and return an enum 
		//maybe have a name value pair for "other properties" that we don't know
		return sdtl.getPropertyName();
	}

	@Override
	public String getUpdatedValue() {
		return getUpdatedValue();
	}
}

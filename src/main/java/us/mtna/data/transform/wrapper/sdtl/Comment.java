package us.mtna.data.transform.wrapper.sdtl;

import org.c2metadata.sdtl.pojo.command.InformBase;

import us.mtna.data.transform.command.ds.NonTransform;

public class Comment implements NonTransform{

	private final org.c2metadata.sdtl.pojo.command.Comment sdtl;
	
	@Override
	public ValidationResult validate() {
		return new ValidationResult();
	}
	
	public Comment(org.c2metadata.sdtl.pojo.command.Comment sdtl) {
		this.sdtl = sdtl;
	}

	@Override
	public InformBase getOriginalCommand() {
		return sdtl;
	}
	
	

}

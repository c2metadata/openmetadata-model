package us.mtna.data.transform.wrapper.sdtl;

import org.c2metadata.sdtl.pojo.command.TransformBase;

import us.mtna.data.transform.command.ds.Transform;

public class Execute implements Transform{

	private final org.c2metadata.sdtl.pojo.command.Execute sdtl;

	public Execute(org.c2metadata.sdtl.pojo.command.Execute sdtl) {
		this.sdtl = sdtl;
	}
	
	@Override
	public TransformBase getOriginalCommand() {
		return sdtl;
	}

	@Override
	public ValidationResult validate() {
		return new ValidationResult();
	}

}

/**
<Overview>
 
Copyright 2017 Metadata Technology North America Inc.
 
This software is the result of collaborative efforts from all participants of the C2Metadata project (http://www.c2metadata.org)
 
C2Metadata is supported by the Data Infrastructure Building Blocks (DIBBs) program of the National Science Foundation through grant NSF ACI-1640575.
 
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 
**/
package us.mtna.data.transform.wrapper.sdtl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.c2metadata.sdtl.pojo.AppendFileDescription;
import org.c2metadata.sdtl.pojo.RenamePair;
import org.c2metadata.sdtl.pojo.command.TransformBase;
import org.c2metadata.sdtl.pojo.expression.VariableReferenceBase;
import org.c2metadata.sdtl.pojo.expression.VariableSymbolExpression;

import us.mtna.data.transform.command.DeletesVariable;
import us.mtna.data.transform.command.SelectsVariables;
import us.mtna.data.transform.command.UpdatesVariables;
import us.mtna.data.transform.command.ds.AppendsDatasets;
import us.mtna.data.transform.command.ds.UpdatesCases;
import us.mtna.data.transform.command.object.Range;
import us.mtna.data.transform.command.object.VariableNamePair;
import us.mtna.pojo.DataType;

public class AppendDatasets implements AppendsDatasets, UpdatesVariables,  DeletesVariable, SelectsVariables {

	private final org.c2metadata.sdtl.pojo.command.AppendDatasets sdtl;

	public AppendDatasets(org.c2metadata.sdtl.pojo.command.AppendDatasets sdtl) {
		this.sdtl = sdtl;
	}

	@Override
	public ValidationResult validate() {
		ValidationResult result = new ValidationResult();
		if (sdtl.getAppendFiles() == null || sdtl.getAppendFiles().length < 1) {
			result.setValid(false);
			result.addMessages("No AppendFileDescriptions were found on the AppenDatasets command.");
		} else {
			for (AppendFileDescription desc : sdtl.getAppendFiles()) {
				if (desc.getFileName() == null) {
					result.setValid(false);
					result.addMessages(
							"One or more of the AppendFileDescriptions on the AppendDatasets command is missing the fileName.");
				}
			}
		}
		return result;
	}

	@Override
	public TransformBase getOriginalCommand() {
		return sdtl;
	}

	@Override
	public String[] getFileNames() {
		List<String> fileNames = new ArrayList<>();
		for (AppendFileDescription description : sdtl.getAppendFiles()) {
			fileNames.add(description.getFileName());
		}
		return fileNames.toArray(new String[fileNames.size()]);
	}

	@Override
	public VariableNamePair[] getUpdatedVariables() {
		List<VariableNamePair> pairs = new ArrayList<>();
		for (AppendFileDescription file : sdtl.getAppendFiles()) {
			VariableNamePair pair;
			if (file.getRenameVariables() != null) {
				for (RenamePair rp : file.getRenameVariables()) {
					VariableSymbolExpression newVar = rp.getNewVariable();
					VariableSymbolExpression oldVar = rp.getOldVariable();
					pair = new VariableNamePair(oldVar.getVariableName(), newVar.getVariableName());
					pairs.add(pair);
				}
			}
		}
		VariableNamePair[] pairArray = new VariableNamePair[pairs.size()];
		return pairs.toArray(pairArray);
	}

	@Override
	public Set<String> getDeletedVars() {
		Set<String> deleted = new HashSet<>();
		for (AppendFileDescription file : sdtl.getAppendFiles()) {
			deleted.addAll(getVariablesFromVariableReferenceBaseArray(
					file.getDropVariables().toArray(new VariableReferenceBase[file.getDropVariables().size()])));
		}
		return deleted;
	}

	@Override
	public List<Range> getDeletedVariableRanges() {
		List<Range> deleted = new ArrayList<>();
		for (AppendFileDescription file : sdtl.getAppendFiles()) {
			deleted.addAll(getRangesFromVariableReferenceBaseArray(
					file.getDropVariables().toArray(new VariableReferenceBase[file.getDropVariables().size()])));
		}
		return deleted;

	}

	@Override
	public String getAppendFlagVariable() {
		return sdtl.getAppendFlagVariable();
	}

	@Override
	public Set<String> getKeepVars() {
		Set<String> kept = new HashSet<>();
		for (AppendFileDescription file : sdtl.getAppendFiles()) {
			kept.addAll(getVariablesFromVariableReferenceBaseArray(
					file.getKeepVariables().toArray(new VariableReferenceBase[file.getKeepVariables().size()])));
		}
		return kept;
	}

	@Override
	public List<Range> getKeepVariableRanges() {
		List<Range> kept = new ArrayList<>();
		for (AppendFileDescription file : sdtl.getAppendFiles()) {
			kept.addAll(getRangesFromVariableReferenceBaseArray(
					file.getKeepVariables().toArray(new VariableReferenceBase[file.getKeepVariables().size()])));
		}
		return kept;
	}

	@Override
	public List<Range> getRanges() {
		List<Range> allRanges = new ArrayList<>();
		allRanges.addAll(getKeepVariableRanges());
		allRanges.addAll(getDeletedVariableRanges());
		return allRanges;
	}

	@Override
	public Set<String> getVariables() {
		Set<String> allVariables = new HashSet<>();
		allVariables.addAll(getKeepVars());
		allVariables.addAll(getDeletedVars());
		for(VariableNamePair pair : getUpdatedVariables()){
			allVariables.add(pair.getSource());
		}
		return allVariables;
	}

	@Override
	public DataType getDataType() {
		// Intentionally null
		return null;
	}

}

package us.mtna.data.transform.wrapper.sdtl;

import org.c2metadata.sdtl.pojo.command.TransformBase;
import org.c2metadata.sdtl.pojo.expression.ExpressionBase;

import us.mtna.data.transform.command.ds.UpdatesCases;

public class IfRows implements UpdatesCases{

	private final org.c2metadata.sdtl.pojo.command.IfRows sdtl;

	public IfRows(org.c2metadata.sdtl.pojo.command.IfRows sdtl){
		this.sdtl = sdtl;
	}
	
	@Override
	public TransformBase getOriginalCommand() {
		return sdtl;
	}

	@Override
	public ValidationResult validate() {
		ValidationResult result = new ValidationResult();
		boolean valid = true;
		if (sdtl.getCondition() == null) {
			String message = "IfRows SDTL is missing a condition in command ["
					+ sdtl.getSourceInformation().getOriginalSourceText() + "]";
			valid = false;
			result.addMessages(message);
		}
		if (sdtl.getThenCommands() == null || sdtl.getThenCommands().length<1) {
			String message = "IfRows SDTL is missing a condition in command ["
					+ sdtl.getSourceInformation().getOriginalSourceText() + "]";
			valid = false;
			result.addMessages(message);
		}
		
		
		result.setValid(valid);
		return result;
	}

	@Override
	public ExpressionBase getCondition() {
		return sdtl.getCondition();
	}

}

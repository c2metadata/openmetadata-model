/**
<Overview>
 
Copyright 2017 Metadata Technology North America Inc.
 
This software is the result of collaborative efforts from all participants of the C2Metadata project (http://www.c2metadata.org)
 
C2Metadata is supported by the Data Infrastructure Building Blocks (DIBBs) program of the National Science Foundation through grant NSF ACI-1640575.
 
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 
**/
package us.mtna.data.transform.wrapper.sdtl;

import java.util.List;
import java.util.Set;

import org.c2metadata.sdtl.pojo.command.TransformBase;
import org.c2metadata.sdtl.pojo.expression.VariableSymbolExpression;

import us.mtna.data.transform.command.SelectsVariables;
import us.mtna.data.transform.command.UpdatesVariables;
import us.mtna.data.transform.command.object.Range;
import us.mtna.data.transform.command.object.VariableNamePair;
import us.mtna.pojo.DataType;

public class SetVariableLabel implements SelectsVariables, UpdatesVariables {

	private final org.c2metadata.sdtl.pojo.command.SetVariableLabel sdtl;

	public SetVariableLabel(org.c2metadata.sdtl.pojo.command.SetVariableLabel sdtl) {
		this.sdtl = sdtl;
	}

	@Override
	public ValidationResult validate() {
		ValidationResult result = new ValidationResult();
		if (sdtl.getLabel() == null) {
			result.setValid(false);
			result.addMessages("SetVariableLabels SDTL does not have any variable labels to assign in command ["
					+ sdtl.getSourceInformation().getOriginalSourceText() + "]");
		}
		if (sdtl.getVariable() == null) {
			result.setValid(false);
			result.addMessages("SetVariableLabels SDTL does not provide a variable to assign a label to in command ["
					+ sdtl.getSourceInformation().getOriginalSourceText() + "]");
		}
		return result;
	}

	@Override
	public TransformBase getOriginalCommand() {
		return sdtl;
	}

	@Override
	public List<Range> getRanges() {
		return getRangesFromVariableReferenceBase(sdtl.getVariable());
	}

	@Override
	public Set<String> getVariables() {
		return getVariablesFromVariableReferenceBase(sdtl.getVariable());
	}

	/**
	 * SetVariableLabel SDTL should have a string value for both the variable
	 * name and label
	 */
	@Override
	public VariableNamePair[] getUpdatedVariables() {
		VariableNamePair pair = null;
		if (VariableSymbolExpression.class.isAssignableFrom(sdtl.getVariable().getClass())) {
			VariableSymbolExpression var = (VariableSymbolExpression) sdtl.getVariable();
			if (var.getVariableName() != null) {
				// set the source and target names to be the same since you
				// are only modifying the label.
				pair = new VariableNamePair(var.getVariableName(), var.getVariableName());
				if (sdtl.getLabel() != null) {
					pair.setLabel(sdtl.getLabel());
				}
			}

		}
		return new VariableNamePair[] { pair };
	}

	@Override
	public DataType getDataType() {
		// intentionally null
		return null;
	}

}

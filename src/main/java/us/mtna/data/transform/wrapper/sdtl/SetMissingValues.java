/**
<Overview>
 
Copyright 2017 Metadata Technology North America Inc.
 
This software is the result of collaborative efforts from all participants of the C2Metadata project (http://www.c2metadata.org)
 
C2Metadata is supported by the Data Infrastructure Building Blocks (DIBBs) program of the National Science Foundation through grant NSF ACI-1640575.
 
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 
**/
package us.mtna.data.transform.wrapper.sdtl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.c2metadata.sdtl.pojo.command.TransformBase;
import org.c2metadata.sdtl.pojo.expression.ExpressionBase;
import org.c2metadata.sdtl.pojo.expression.NumberRangeExpression;
import org.c2metadata.sdtl.pojo.expression.StringRangeExpression;

import us.mtna.data.transform.command.SelectsVariables;
import us.mtna.data.transform.command.UpdatesClassification;
import us.mtna.data.transform.command.object.ClassificationUpdate;
import us.mtna.data.transform.command.object.CodeDetail;
import us.mtna.data.transform.command.object.CodeRangeDetail;
import us.mtna.data.transform.command.object.Range;

public class SetMissingValues implements SelectsVariables, UpdatesClassification {

	private final org.c2metadata.sdtl.pojo.command.SetMissingValues sdtl;
	private boolean requiresCopyOfClassification;
	private boolean copyFloatingCodes;

	public SetMissingValues(org.c2metadata.sdtl.pojo.command.SetMissingValues sdtl) {
		this.sdtl = sdtl;
		requiresCopyOfClassification = true;
		copyFloatingCodes = true;
	}

	@Override
	public ValidationResult validate() {
		ValidationResult validation = new ValidationResult();
		if (sdtl.getValues() == null || sdtl.getValues().length<1) {
			validation.setValid(false);
			validation.addMessages("No values found on the SetMissingValues command.[" + sdtl.getSourceInformation().getOriginalSourceText() + "].");
		} else if (sdtl.getVariables() == null || sdtl.getVariables().length<1) {
			validation.setValid(false);
			validation.addMessages("No variables found on the SetMissingValues command. [" + sdtl.getSourceInformation().getOriginalSourceText() + "].");
		}
		return validation;
	}

	@Override
	public TransformBase getOriginalCommand() {
		return sdtl;
	}

	@Override
	public List<Range> getRanges() {
		return getRangesFromVariableReferenceBaseArray(sdtl.getVariables());
	}

	/**
	 * SetMissingValue SDTL should have either a variable range or array telling
	 * which variables to select
	 */
	@Override
	public Set<String> getVariables() {
		return getVariablesFromVariableReferenceBaseArray(sdtl.getVariables());
	}

	@Override
	public ClassificationUpdate getUpdate() {
		ClassificationUpdate update = new ClassificationUpdate();
		List<CodeDetail> details = new ArrayList<>();
		List<CodeRangeDetail> rangeDetails = new ArrayList<>();
		Set<String> values = new HashSet<>();
		for (ExpressionBase base : sdtl.getValues()) {
			if (NumberRangeExpression.class.isAssignableFrom(base.getClass())
					|| StringRangeExpression.class.isAssignableFrom(base.getClass())) {
				Range range = new Range();
				if (NumberRangeExpression.class.isAssignableFrom(base.getClass())) {
					NumberRangeExpression nre = (NumberRangeExpression) base;
					ExpressionBase rangeEnd = nre.getNumberRangeEnd();
					ExpressionBase rangeStart = nre.getNumberRangeStart();
					range.setEnd(checkConstantExpressions(rangeEnd));
					range.setStart(checkConstantExpressions(rangeStart));
				} else if (StringRangeExpression.class.isAssignableFrom(base.getClass())) {
					StringRangeExpression nre = (StringRangeExpression) base;
					String rangeEnd = nre.getRangeEnd();
					String rangeStart = nre.getRangeStart();
					range.setEnd(rangeEnd);
					range.setStart(rangeStart);
				}
				CodeRangeDetail rangeDetail = new CodeRangeDetail();
				rangeDetail.setFromRange(range);
				rangeDetail.setMissing(true);
				rangeDetails.add(rangeDetail);
			} else {
				if (checkConstantExpressions(base) != null) {
					String value = checkConstantExpressions(base);
					values.add(value);
				} else {
					values = parseExpression(base);
				}
				CodeDetail detail = new CodeDetail();
				detail.setMissing(true);
				//detail.setFromValue(values.iterator().next());
				detail.setFromValue(null);
				detail.setNewValue(values.iterator().next());
				details.add(detail);
			}
		}
		update.setUpdatesCodeRange(rangeDetails.toArray(new CodeRangeDetail[rangeDetails.size()]));
		update.setUpdatesCodes(details.toArray(new CodeDetail[details.size()]));
		return update;
	}

	/**
	 * Defaults to true.
	 */
	@Override
	public boolean requiresCopyOfClassification() {
		return requiresCopyOfClassification;
	}

	/**
	 * Defaults to true. {@inheritDoc}
	 */
	@Override
	public boolean copyFloatingCodes() {
		return copyFloatingCodes;
	}

	/**
	 * Sets whether a classification's floating/unused codes should be carried
	 * over. Default to true. {@inheritDoc}
	 * 
	 * @param copyFloatingCodes
	 */
	public void setCopyFloatingCodes(boolean copyFloatingCodes) {
		this.copyFloatingCodes = copyFloatingCodes;
	}

	/**
	 * Defaults to true.
	 */
	public void setRequiresCopyOfClassification(boolean requiresCopyOfClassification) {
		this.requiresCopyOfClassification = requiresCopyOfClassification;
	}

}

/**
<Overview>
 
Copyright 2017 Metadata Technology North America Inc.
 
This software is the result of collaborative efforts from all participants of the C2Metadata project (http://www.c2metadata.org)
 
C2Metadata is supported by the Data Infrastructure Building Blocks (DIBBs) program of the National Science Foundation through grant NSF ACI-1640575.
 
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 
**/
package us.mtna.data.transform.wrapper.sdtl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.c2metadata.sdtl.pojo.MergeFileDescription;
import org.c2metadata.sdtl.pojo.RenamePair;
import org.c2metadata.sdtl.pojo.command.TransformBase;
import org.c2metadata.sdtl.pojo.expression.VariableSymbolExpression;

import us.mtna.data.transform.command.CreatesVariables;
import us.mtna.data.transform.command.DeletesVariable;
import us.mtna.data.transform.command.SelectsVariables;
import us.mtna.data.transform.command.UpdatesVariables;
import us.mtna.data.transform.command.ds.MergesDatasets;
import us.mtna.data.transform.command.ds.UpdatesCases;
import us.mtna.data.transform.command.object.NewVariable;
import us.mtna.data.transform.command.object.Range;
import us.mtna.data.transform.command.object.UpdaterMergeFileDescription;
import us.mtna.data.transform.command.object.UpdaterMergeFileDescription.MergeType;
import us.mtna.data.transform.command.object.UpdaterMergeFileDescription.UpdateType;
import us.mtna.data.transform.command.object.VariableNamePair;
import us.mtna.pojo.DataType;

/**
 * Join/Merge 2 or more datasets.
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public class MergeDatasets
		implements MergesDatasets, CreatesVariables, DeletesVariable, UpdatesVariables, SelectsVariables {

	private org.c2metadata.sdtl.pojo.command.MergeDatasets sdtl;

	public MergeDatasets(org.c2metadata.sdtl.pojo.command.MergeDatasets sdtl) {
		this.sdtl = sdtl;
	}

	@Override
	public boolean preserveOriginalVariable() {
		return true;
	}
	
	@Override
	public ValidationResult validate() {
		ValidationResult result = new ValidationResult();
		if (sdtl.getMergeFiles() == null || sdtl.getMergeFiles().length < 2) {
			result.setValid(false);
			result.addMessages("MergeDatasets command must have at least two MergeFileDescriptions. Command: ["
					+ sdtl.getSourceInformation().getOriginalSourceText() + "]");
		} else {
			// validate each merge file description
			int i = 0;
			for (MergeFileDescription desc : sdtl.getMergeFiles()) {
				if (desc.getFileName() == null) {
					result.setValid(false);
					result.addMessages("No file name found on the merge file description at index [" + i
							+ "] in command [" + sdtl.getSourceInformation().getOriginalSourceText() + "]");
				}
				if (desc.getMergeType() == null) {
					result.setValid(false);
					result.addMessages("No merge type found on the merge file description at index [" + i
							+ "] in command [" + sdtl.getSourceInformation().getOriginalSourceText() + "]");
				} else if (parseMergeType(desc.getMergeType()) == null) {
					result.setValid(false);
					StringBuilder sb = new StringBuilder();
					for (MergeType type : MergeType.values()) {
						sb.append(type.toString()).append(" ");
					}
					result.addMessages(
							"The mergeType [" + desc.getMergeType() + "] on the merge file description at index [" + i
									+ "] in command [" + sdtl.getSourceInformation().getOriginalSourceText()
									+ "] did not match one of the expected values: [" + sb.toString() + "]");

				}
				if (desc.getUpdate() == null) {
					result.setValid(false);
					result.addMessages("No update string found on the merge file description at index [" + i
							+ "] in command [" + sdtl.getSourceInformation().getOriginalSourceText() + "]");
				} else if (parseUpdateType(desc.getUpdate()) == null) {
					result.setValid(false);
					StringBuilder sb = new StringBuilder();
					for (UpdateType type : UpdateType.values()) {
						sb.append(type.toString()).append(" ");
					}
					result.addMessages(
							"The update type [" + desc.getUpdate() + "] on the merge file description at index [" + i
									+ "] in command [" + sdtl.getSourceInformation().getOriginalSourceText()
									+ "] did not match one of the expected values: [" + sb.toString() + "]");
				}
				i++;
			}
		}
		return result;
	}

	@Override
	public TransformBase getOriginalCommand() {
		return sdtl;
	}

	@Override
	public Set<String> getMergeByVariables() {
		return getVariablesFromVariableReferenceBaseArray(sdtl.getMergeByVariables());
	}

	@Override
	public Set<String> getKeepVariables() {
		Set<String> keeps = new HashSet<>();
		for (MergeFileDescription desc : sdtl.getMergeFiles()) {
			keeps.addAll(getVariablesFromVariableReferenceBaseArray(desc.getKeepVariables()));
		}
		return keeps;
	}

	@Override
	public List<UpdaterMergeFileDescription> getMergeFileDscr() {
		List<UpdaterMergeFileDescription> list = new ArrayList<>();
		for (MergeFileDescription dscr : sdtl.getMergeFiles()) {
			UpdaterMergeFileDescription file = new UpdaterMergeFileDescription();
			file.setDropVariables(dscr.getDropVariables());
			file.setFileName(dscr.getFileName());
			file.setKeepVariables(dscr.getKeepVariables());
			file.setMergeFlagVariable(dscr.getMergeFlagVariable());
			file.setMergeType(parseMergeType(dscr.getMergeType()));
			file.setNewRow(dscr.isNewRow());
			file.setUpdate(parseUpdateType(dscr.getUpdate()));
			file.setRenameVariables(getVarNamePairsFromRenamePairs(dscr.getRenameVariables()));
			list.add(file);
		}
		return list;
	}

	public MergeType parseMergeType(String text) {
		switch (text.toUpperCase().trim()) {
		case "SEQUENTIAL":
			return MergeType.SEQUENTIAL;
		case "ONETOONE":
			return MergeType.ONE_TO_ONE;
		case "MANYTOONE":
			return MergeType.MANY_TO_ONE;
		case "ONETOMANY":
			return MergeType.ONE_TO_MANY;
		case "CARTESIAN":
			return MergeType.CARTESIAN;
		case "UNMATCHED":
			return MergeType.UNMATCHED;
		case "SASMATCHMERGE":
			return MergeType.SAS_MATCH_MERGE;
		default:
			return null;
		}
	}

	public UpdateType parseUpdateType(String text) {
		switch (text.toUpperCase().trim()) {
		case "MASTER":
			return UpdateType.MASTER;
		case "IGNORE":
			return UpdateType.IGNORE;
		case "FILLNEW":
			return UpdateType.FILL_NEW;
		case "UPDATEMISSING":
			return UpdateType.UPDATE_MISSING;
		case "REPLACE":
			return UpdateType.REPLACE;
		default:
			return null;
		}
	}

	private List<VariableNamePair> getVarNamePairsFromRenamePairs(RenamePair[] pairs) {
		List<VariableNamePair> list = new ArrayList<>();
		if (pairs != null) {
			for (RenamePair pair : pairs) {
				if (pair.getNewVariable() != null && pair.getOldVariable() != null) {
					if (pair.getNewVariable().getVariableName() != null
							&& pair.getOldVariable().getVariableName() != null) {
						list.add(new VariableNamePair(pair.getOldVariable().getVariableName(),
								pair.getNewVariable().getVariableName()));
					}
				}
			}
		}
		return list;
	}

	@Override
	public String getFirstVariable() {
		return sdtl.getFirstVariable();
	}

	@Override
	public String getLastVariable() {
		return sdtl.getLastVariable();
	}

	@Override
	public NewVariable[] getNewVariables() {
		List<NewVariable> newVars = new ArrayList<>();
		if (sdtl.getFirstVariable() != null) {
			NewVariable first = new NewVariable();
			first.setNewVariableName(sdtl.getFirstVariable());
			newVars.add(first);
		}
		if (sdtl.getLastVariable() != null) {
			NewVariable last = new NewVariable();
			last.setNewVariableName(sdtl.getLastVariable());
			newVars.add(last);
		}

		return newVars.toArray(new NewVariable[0]);
	}

	@Override
	public Set<String> getDeletedVars() {
		Set<String> drops = new HashSet<>();
		for (MergeFileDescription desc : sdtl.getMergeFiles()) {
			drops.addAll(getVariablesFromVariableReferenceBaseArray(desc.getDropVariables()));

		}
		return drops;
	}

	@Override
	public List<Range> getDeletedVariableRanges() {
		List<Range> drops = new ArrayList<>();
		for (MergeFileDescription desc : sdtl.getMergeFiles()) {
			drops.addAll(getRangesFromVariableReferenceBaseArray(desc.getDropVariables()));
		}
		return drops;
	}

	@Override
	public Set<String> getKeepVars() {
		Set<String> kept = new HashSet<>();
		for (MergeFileDescription file : sdtl.getMergeFiles()) {
			kept.addAll(getVariablesFromVariableReferenceBaseArray(file.getKeepVariables()));
		}
		return kept;
	}

	@Override
	public List<Range> getKeepVariableRanges() {
		List<Range> kept = new ArrayList<>();
		for (MergeFileDescription file : sdtl.getMergeFiles()) {
			kept.addAll(getRangesFromVariableReferenceBaseArray(file.getKeepVariables()));
		}
		return kept;
	}

	@Override
	public List<Range> getRanges() {
		List<Range> allRanges = new ArrayList<>();
		allRanges.addAll(getKeepVariableRanges());
		allRanges.addAll(getDeletedVariableRanges());
		return allRanges;
	}

	@Override
	public Set<String> getVariables() {
		Set<String> allVariables = new HashSet<>();
		allVariables.addAll(getKeepVars());
		allVariables.addAll(getDeletedVars());
		if (sdtl.getFirstVariable() != null) {
			allVariables.add(sdtl.getFirstVariable());
		}
		if (sdtl.getLastVariable() != null) {
			allVariables.add(sdtl.getLastVariable());
		}
		for (VariableNamePair pair : getUpdatedVariables()) {
			allVariables.add(pair.getSource());
		}
		return allVariables;
	}

	@Override
	public VariableNamePair[] getUpdatedVariables() {
		List<VariableNamePair> pairs = new ArrayList<>();
		for (MergeFileDescription file : sdtl.getMergeFiles()) {
			VariableNamePair pair;
			if (file.getRenameVariables() != null) {
				for (RenamePair rp : file.getRenameVariables()) {
					VariableSymbolExpression newVar = rp.getNewVariable();
					VariableSymbolExpression oldVar = rp.getOldVariable();
					pair = new VariableNamePair(oldVar.getVariableName(), newVar.getVariableName());
					pairs.add(pair);
				}
			}
		}
		VariableNamePair[] pairArray = new VariableNamePair[pairs.size()];
		return pairs.toArray(pairArray);
	}

	@Override
	public DataType getDataType() {
		return null;
	}

}

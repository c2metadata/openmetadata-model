/**
<Overview>
 
Copyright 2017 Metadata Technology North America Inc.
 
This software is the result of collaborative efforts from all participants of the C2Metadata project (http://www.c2metadata.org)
 
C2Metadata is supported by the Data Infrastructure Building Blocks (DIBBs) program of the National Science Foundation through grant NSF ACI-1640575.
 
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 
**/
package us.mtna.data.transform.wrapper.sdtl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.c2metadata.sdtl.pojo.command.TransformBase;

import us.mtna.data.transform.command.DeletesVariable;
import us.mtna.data.transform.command.SelectsVariables;
import us.mtna.data.transform.command.object.Range;

/**
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public class DropVariables implements SelectsVariables, DeletesVariable {

	private org.c2metadata.sdtl.pojo.command.DropVariables sdtl;

	private Set<String> deletedVars;
	private List<Range> ranges;
	private Set<String> variables;
	private List<Range> deletedVariableRanges;
	private Set<String> keepVars;
	private List<Range> keepVariableRanges;

	public DropVariables(org.c2metadata.sdtl.pojo.command.DropVariables sdtl) {
		this.sdtl = sdtl;
		this.deletedVars = getVariablesFromVariableReferenceBaseArray(sdtl.getVariables());
		this.ranges = getRangesFromVariableReferenceBaseArray(sdtl.getVariables());
		this.variables = getVariablesFromVariableReferenceBaseArray(sdtl.getVariables());
		this.deletedVariableRanges = getRangesFromVariableReferenceBaseArray(sdtl.getVariables());
		this.keepVariableRanges = new ArrayList<>();
		this.keepVars = new HashSet<>();
	}

	@Override
	public ValidationResult validate() {
		ValidationResult result = new ValidationResult();
		if (sdtl.getVariables() == null || sdtl.getVariables().length < 1) {
			result.setValid(false);
			result.addMessages("No variables found on command [" + sdtl.getSourceInformation().getOriginalSourceText() + "]");
		}
		return result;
	}

	@Override
	public TransformBase getOriginalCommand() {
		return sdtl;
	}

	@Override
	public Set<String> getDeletedVars() {
		// return
		// getVariablesFromVariableReferenceBaseArray(sdtl.getVariables());
		return deletedVars;
	}

	@Override
	public List<Range> getRanges() {
		// return getRangesFromVariableReferenceBaseArray(sdtl.getVariables());
		return ranges;

	}

	/**
	 * Delete SDTL should have either a variable range expression or string
	 * array of variable names to be removed
	 */
	@Override
	public Set<String> getVariables() {
		// return
		// getVariablesFromVariableReferenceBaseArray(sdtl.getVariables());
		return variables;
	}

	@Override
	public List<Range> getDeletedVariableRanges() {
		// return getRangesFromVariableReferenceBaseArray(sdtl.getVariables());
		return deletedVariableRanges;
	}

	@Override
	public Set<String> getKeepVars() {
		// return new HashSet<>();
		return keepVars;

	}

	@Override
	public List<Range> getKeepVariableRanges() {
		// return new ArrayList<>();
		return keepVariableRanges;
	}

	public org.c2metadata.sdtl.pojo.command.DropVariables getSdtl() {
		return sdtl;
	}

	public void setSdtl(org.c2metadata.sdtl.pojo.command.DropVariables sdtl) {
		this.sdtl = sdtl;
	}

	public void setDeletedVars(Set<String> deletedVars) {
		this.deletedVars = deletedVars;
	}

	public void setRanges(List<Range> ranges) {
		this.ranges = ranges;
	}

	public void setVariables(Set<String> variables) {
		this.variables = variables;
	}

	public void setDeletedVariableRanges(List<Range> deletedVariableRanges) {
		this.deletedVariableRanges = deletedVariableRanges;
	}

	public void setKeepVars(Set<String> keepVars) {
		this.keepVars = keepVars;
	}

	public void setKeepVariableRanges(List<Range> keepVariableRanges) {
		this.keepVariableRanges = keepVariableRanges;
	}

}

package us.mtna.data.transform.wrapper.sdtl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.c2metadata.sdtl.pojo.command.TransformBase;

import us.mtna.data.transform.command.CreatesVariables;
import us.mtna.data.transform.command.DeletesVariable;
import us.mtna.data.transform.command.SelectsVariables;
import us.mtna.data.transform.command.object.NewVariable;
import us.mtna.data.transform.command.object.Range;

/**
 * ReshapeWide is not supported in the current version of SDTL, because it
 * depends on values in the data. However, it may be useful when values of the
 * index variable are available in the metadata file or the data can be
 * processed
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public class ReshapeWide implements CreatesVariables, DeletesVariable, SelectsVariables {

	private final org.c2metadata.sdtl.pojo.command.ReshapeWide sdtl;

	public ReshapeWide(org.c2metadata.sdtl.pojo.command.ReshapeWide sdtl) {
		this.sdtl = sdtl;
	}
	// TODO implement methods

	@Override
	public boolean preserveOriginalVariable() {
		return false;
	}
	
	@Override
	public TransformBase getOriginalCommand() {
		return sdtl;
	}

	@Override
	public ValidationResult validate() {
		return new ValidationResult();
	}

	@Override
	public List<Range> getRanges() {
		return new ArrayList<>();
	}

	@Override
	public Set<String> getVariables() {
		return new HashSet<>();
	}

	@Override
	public Set<String> getDeletedVars() {
		return new HashSet<>();
	}

	@Override
	public NewVariable[] getNewVariables() {
		return new NewVariable[0];
	}

	@Override
	public List<Range> getDeletedVariableRanges() {
		return new ArrayList<>();
	}

	@Override
	public Set<String> getKeepVars() {
		return new HashSet<>();
	}

	@Override
	public List<Range> getKeepVariableRanges() {
		return new ArrayList<>();
	}

}

/**
<Overview>
 
Copyright 2017 Metadata Technology North America Inc.
 
This software is the result of collaborative efforts from all participants of the C2Metadata project (http://www.c2metadata.org)
 
C2Metadata is supported by the Data Infrastructure Building Blocks (DIBBs) program of the National Science Foundation through grant NSF ACI-1640575.
 
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 
**/
package us.mtna.data.transform.wrapper.sdtl;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.c2metadata.sdtl.pojo.FunctionArgument;
import org.c2metadata.sdtl.pojo.command.TransformBase;
import org.c2metadata.sdtl.pojo.expression.FunctionCallExpression;
import org.c2metadata.sdtl.pojo.expression.NumericConstantExpression;
import org.c2metadata.sdtl.pojo.expression.VariableRangeExpression;
import org.c2metadata.sdtl.pojo.expression.VariableSymbolExpression;

import us.mtna.data.transform.command.CreatesVariables;
import us.mtna.data.transform.command.SelectsVariables;
import us.mtna.data.transform.command.object.NewVariable;
import us.mtna.data.transform.command.object.Range;

public class Compute implements SelectsVariables, CreatesVariables{

	private final org.c2metadata.sdtl.pojo.command.Compute sdtl;

	public Compute(org.c2metadata.sdtl.pojo.command.Compute sdtl) {
		this.sdtl = sdtl;
	}

	@Override
	public ValidationResult validate() {
		ValidationResult result = new ValidationResult();
		if (sdtl.getExpression() == null) {
			result.addMessages("No expression to evaluate on command ["
					+ sdtl.getSourceInformation().getOriginalSourceText() + "]");
			result.setValid(false);
		}
		if (sdtl.getVariable() == null) {
			result.addMessages("No variable to compute found on command ["
					+ sdtl.getSourceInformation().getOriginalSourceText() + "]");
			result.setValid(false);
		}
		return result;
	}
	
	@Override
	public boolean preserveOriginalVariable() {
		return true;
	}

	@Override
	public TransformBase getOriginalCommand() {
		return sdtl;
	}

	@Override
	public List<Range> getRanges() {
		return getRangesFromVariableReferenceBase(sdtl.getVariable());
	}

	@Override
	public Set<String> getVariables() {
		return getVariablesFromVariableReferenceBase(sdtl.getVariable());
	}

	/**
	 * Gets the list of new variables from the expression on the sdtl. We only
	 * really care about the creation of one or more variables, not really the
	 * expressions/conditions.
	 */
	@Override
	public NewVariable[] getNewVariables() {
		List<NewVariable> newVars = new ArrayList<>();

		if (sdtl.getVariable() != null) {
			NewVariable newVar = new NewVariable();

			if (VariableSymbolExpression.class.isAssignableFrom(sdtl.getVariable().getClass())) {
				VariableSymbolExpression symbol = (VariableSymbolExpression) sdtl.getVariable();
				if (symbol.getVariableName() != null) {
					newVar.setNewVariableName(symbol.getVariableName());
				}
				newVar = parseSourceVariables(sdtl.getExpression(), newVar);
				// if this is a numeric constant
//				if(FunctionCallExpression.class.isAssignableFrom(sdtl.getExpression().getClass())){
//					FunctionCallExpression expr = (FunctionCallExpression) sdtl.getExpression();
//					for (FunctionArgument arg : expr.getArguments()) {
//						
////						value.addAll(parseExpression(arg.getArgumentValue()));
//					}
//				}
//				if (!NumericConstantExpression.class.isAssignableFrom(sdtl.getExpression().getClass())) {
//					Set<String> vars = parseExpression(sdtl.getExpression());
//					for (String v : vars) {
//						newVar.addSourceVariables(v);
//						newVar.setBasisVariableName(v);
//					}
//				}
				newVars.add(newVar);

			} else if (VariableRangeExpression.class.isAssignableFrom(sdtl.getVariable().getClass())) {

				VariableRangeExpression rangeExpr = (VariableRangeExpression) sdtl.getVariable();
				Range range = new Range();
				if (rangeExpr.getFirst() != null) {
					range.setStart(rangeExpr.getFirst());
				} 
				if (rangeExpr.getLast() != null) {
					range.setEnd(rangeExpr.getLast());
				}
				newVar.setSourceVariableRange(range);
				newVars.add(newVar);
			}
		}
		
		return newVars.toArray(new NewVariable[newVars.size()]);
	}

	


}

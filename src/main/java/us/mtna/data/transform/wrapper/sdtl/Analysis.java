package us.mtna.data.transform.wrapper.sdtl;

import org.c2metadata.sdtl.pojo.command.InformBase;

import us.mtna.data.transform.command.ds.NonTransform;

/**
 * Describes an analysis command. An analysis command does not result in any
 * data transformation.
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public class Analysis implements NonTransform {

	private final org.c2metadata.sdtl.pojo.command.Analysis sdtl;

	public Analysis(org.c2metadata.sdtl.pojo.command.Analysis sdtl) {
		this.sdtl = sdtl;
	}

	@Override
	public InformBase getOriginalCommand() {
		return sdtl;
	}

	@Override
	public ValidationResult validate() {
		return new ValidationResult();
	}

}

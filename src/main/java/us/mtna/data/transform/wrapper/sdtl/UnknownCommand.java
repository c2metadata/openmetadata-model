/**
<Overview>
 
Copyright 2017 Metadata Technology North America Inc.
 
This software is the result of collaborative efforts from all participants of the C2Metadata project (http://www.c2metadata.org)
 
C2Metadata is supported by the Data Infrastructure Building Blocks (DIBBs) program of the National Science Foundation through grant NSF ACI-1640575.
 
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 
**/
package us.mtna.data.transform.wrapper.sdtl;

import org.c2metadata.sdtl.pojo.command.CommandBase;
import org.c2metadata.sdtl.pojo.command.TransformBase;

import us.mtna.data.transform.command.SdtlWrapper;

/**
 * For use with the {@link WrapperFactory}, this will be instantiated if there
 * is not a cogs pojo to match the requested command, and will just use the
 * generic TransformBase instead of any implementation.
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public class UnknownCommand implements SdtlWrapper {

	private final CommandBase cogs;

	@Override
	public ValidationResult validate() {
		ValidationResult result = new ValidationResult();
		result.setValid(false);
		result.addMessages("Unknown command [" + cogs.getCommand() + "]");
		return result;
	}

	public UnknownCommand(CommandBase cogs) {
		this.cogs = cogs;
	}

	@Override
	public CommandBase getOriginalCommand() {
		return cogs;
	}

}

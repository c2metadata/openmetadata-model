/**
<Overview>
 
Copyright 2017 Metadata Technology North America Inc.
 
This software is the result of collaborative efforts from all participants of the C2Metadata project (http://www.c2metadata.org)
 
C2Metadata is supported by the Data Infrastructure Building Blocks (DIBBs) program of the National Science Foundation through grant NSF ACI-1640575.
 
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 
**/
package us.mtna.data.transform.wrapper.sdtl;

import org.c2metadata.sdtl.pojo.command.TransformBase;

import us.mtna.data.transform.command.ds.SavesDataset;

public class Save implements SavesDataset {

	private org.c2metadata.sdtl.pojo.command.Save sdtl;

	public Save(org.c2metadata.sdtl.pojo.command.Save sdtl) {
		this.sdtl = sdtl;
	}

	@Override
	public ValidationResult validate() {
		ValidationResult result = new ValidationResult();
		if (sdtl.getFileName() == null) {
			result.setValid(false);
			result.addMessages(
					"Save SDTL is missing a file name to save to in command " + sdtl.getSourceInformation().getOriginalSourceText() + "]");
		}
		return result;
	}

	/**
	 * Save sdtl should have a file name string
	 */
	@Override
	public String getDatasetId() {
		//already null checked in validate()
		return sdtl.getFileName();
	}

	@Override
	public TransformBase getOriginalCommand() {
		return sdtl;
	}
}
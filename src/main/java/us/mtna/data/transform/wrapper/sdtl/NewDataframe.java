package us.mtna.data.transform.wrapper.sdtl;

import org.c2metadata.sdtl.pojo.command.TransformBase;

import us.mtna.data.transform.command.ds.Transform;

public class NewDataframe implements Transform{

	private final org.c2metadata.sdtl.pojo.command.NewDataframe sdtl;

	public NewDataframe(org.c2metadata.sdtl.pojo.command.NewDataframe sdtl){
		this.sdtl = sdtl;
	}

	@Override
	public TransformBase getOriginalCommand() {
		return sdtl;
	}

	@Override
	public ValidationResult validate() {
		return new ValidationResult();
	}
}

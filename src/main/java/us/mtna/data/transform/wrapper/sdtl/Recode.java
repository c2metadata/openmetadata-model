/**
<Overview>
 
Copyright 2017 Metadata Technology North America Inc.
 
This software is the result of collaborative efforts from all participants of the C2Metadata project (http://www.c2metadata.org)
 
C2Metadata is supported by the Data Infrastructure Building Blocks (DIBBs) program of the National Science Foundation through grant NSF ACI-1640575.
 
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 
**/
package us.mtna.data.transform.wrapper.sdtl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.c2metadata.sdtl.pojo.RecodeRule;
import org.c2metadata.sdtl.pojo.RecodeVariable;
import org.c2metadata.sdtl.pojo.command.TransformBase;
import org.c2metadata.sdtl.pojo.expression.ExpressionBase;
import org.c2metadata.sdtl.pojo.expression.MissingValueConstantExpression;
import org.c2metadata.sdtl.pojo.expression.NumberRangeExpression;
import org.c2metadata.sdtl.pojo.expression.NumericConstantExpression;
import org.c2metadata.sdtl.pojo.expression.StringConstantExpression;
import org.c2metadata.sdtl.pojo.expression.StringRangeExpression;

import us.mtna.data.transform.command.CreatesVariables;
import us.mtna.data.transform.command.SelectsVariables;
import us.mtna.data.transform.command.UpdatesClassification;
import us.mtna.data.transform.command.object.ClassificationUpdate;
import us.mtna.data.transform.command.object.CodeDetail;
import us.mtna.data.transform.command.object.CodeRangeDetail;
import us.mtna.data.transform.command.object.NewVariable;
import us.mtna.data.transform.command.object.Range;

public class Recode implements SelectsVariables, UpdatesClassification, CreatesVariables {

	private final org.c2metadata.sdtl.pojo.command.Recode sdtl;
	private boolean requiresCopyOfClassification;
	private boolean copyFloatingCodes;

	public Recode(org.c2metadata.sdtl.pojo.command.Recode sdtl) {
		this.sdtl = sdtl;
		requiresCopyOfClassification = true;
		copyFloatingCodes = true;
	}

	@Override
	public ValidationResult validate() {
		ValidationResult result = new ValidationResult();
		if ((sdtl.getRecodedVariables() == null || sdtl.getRecodedVariables().length < 1)
				&& sdtl.getRecodedVariableRange() == null) {
			result.setValid(false);
			result.addMessages("No variables found to recode on Recode command ["
					+ sdtl.getSourceInformation().getOriginalSourceText() + "]");
		}
		return result;
	}

	@Override
	public TransformBase getOriginalCommand() {
		return sdtl;
	}

	@Override
	public boolean preserveOriginalVariable() {
		return true;
	}
	
	@Override
	public List<Range> getRanges() {
		List<Range> ranges = new ArrayList<>();

		if (sdtl.getRecodedVariableRange() != null) {
			Range range = new Range();
			if (sdtl.getRecodedVariableRange().getFirst() != null) {
				range.setStart(sdtl.getRecodedVariableRange().getFirst());
			}
			if (sdtl.getRecodedVariableRange().getLast() != null) {
				range.setEnd(sdtl.getRecodedVariableRange().getLast());
			}
			ranges.add(range);
		}
		return ranges;
	}

	@Override
	public Set<String> getVariables() {
		Set<String> vars = new HashSet<>();
		// for (RecodeRule rule : sdtl.getRules()) {
		// vars.add(rule.getTo());
		// }
		for (RecodeVariable var : sdtl.getRecodedVariables()) {
			vars.add(var.getSource());
		}
		return vars;
	}

	/**
	 * Recodes don't update codes - this method will instead delete all current
	 * codes to be recoded and add the new values back in their place
	 */
	// @Override
	// public ClassificationUpdate getUpdate() {
	// ClassificationUpdate update = new ClassificationUpdate();
	// List<CodeDetail> newCodes = new ArrayList<>();
	// List<CodeRangeDetail> newCodeRanges = new ArrayList<>();
	// List<Range> removedCodeRanges = new ArrayList<>();
	// List<String> removedCodes = new ArrayList<>();
	// if (sdtl.getRules() != null) {
	// for (RecodeRule rule : sdtl.getRules()) {
	// CodeRangeDetail range = null;
	// if (rule.getFromValueRange() != null) {
	// for (ValueRange valueRange : rule.getFromValueRange()) {
	// range = new CodeRangeDetail();
	// range.setLabel(rule.getLabel());
	// Range codeRange = new Range();
	// codeRange.setEnd(valueRange.getLast());
	// codeRange.setStart(valueRange.getFirst());
	// range.setFromRange(codeRange);
	// range.setTargetValue(rule.getTo());
	// removedCodeRanges.add(codeRange);
	// }
	// newCodeRanges.add(range);
	// }
	// CodeDetail updateDetail;
	// if (rule.getFromValue() != null) {
	// for (String fromValue : rule.getFromValue()) {
	// updateDetail = new CodeDetail();
	// updateDetail.setLabel(rule.getLabel());
	// updateDetail.setNewValue(rule.getTo());
	// updateDetail.setFromValue(fromValue);
	// newCodes.add(updateDetail);
	// removedCodes.add(fromValue);
	// }
	// }
	// }
	// } else {
	// log.warn("No RecodeRules found when trying to execute command [" +
	// sdtl.getCommand().toString() + "]");
	// }
	//
	// update.setNewCodes(newCodes.toArray(new CodeDetail[newCodes.size()]));
	// update.setNewCodeRanges(newCodeRanges.toArray(new
	// CodeRangeDetail[newCodeRanges.size()]));
	// update.setRemovedCodeRanges(removedCodeRanges.toArray(new
	// Range[removedCodeRanges.size()]));
	// update.setRemovedCodes(removedCodes.toArray(new
	// String[removedCodes.size()]));
	// return update;
	// }

	@Override
	public ClassificationUpdate getUpdate() {
		ClassificationUpdate update = new ClassificationUpdate();
		List<CodeDetail> newCodes = new ArrayList<>();
		List<CodeRangeDetail> newCodeRanges = new ArrayList<>();
		List<Range> removedCodeRanges = new ArrayList<>();
		List<String> removedCodes = new ArrayList<>();

		if (sdtl.getRules() != null) {
			for (RecodeRule rule : sdtl.getRules()) {
				if (rule.getFromValue() != null) {

					Class<?> toClass = rule.getTo().getClass();

					// if the rule's from value is a range, populate a code
					// range detail for the updated code range.
					for (ExpressionBase fromExpression : rule.getFromValue()) {
						Class<?> fromClass = fromExpression.getClass();

						// other possibilities: STRING CONSTANT numeric constant
						// , numeric list, string list
						if (NumberRangeExpression.class.isAssignableFrom(fromClass)
								|| StringRangeExpression.class.isAssignableFrom(fromClass)) {
							CodeRangeDetail codeRangeDetail = new CodeRangeDetail();
							codeRangeDetail.setLabel(rule.getLabel());

							String fromValue = null;
							String toValue = null;
							if (StringRangeExpression.class.isAssignableFrom(fromExpression.getClass())) {
								StringRangeExpression sre = (StringRangeExpression) fromExpression;
								fromValue = sre.getRangeStart();
								toValue = sre.getRangeEnd();
							} else if (NumberRangeExpression.class.isAssignableFrom(fromExpression.getClass())) {
								NumberRangeExpression nre = (NumberRangeExpression) fromExpression;
								ExpressionBase startExpression = nre.getNumberRangeStart();
								ExpressionBase endExpression = nre.getNumberRangeEnd();
								fromValue = checkConstantExpressions(startExpression);
								toValue = checkConstantExpressions(endExpression);
							}

							Range fromRange = new Range();
							fromRange.setEnd(toValue);
							fromRange.setStart(fromValue);
							codeRangeDetail.setFromRange(fromRange);
							codeRangeDetail.setTargetValue(rule.getTo());
							removedCodeRanges.add(fromRange);
							newCodeRanges.add(codeRangeDetail);

							// check for lists

						} else if (MissingValueConstantExpression.class.isAssignableFrom(fromClass)) {
							// TODO
						}

						// and then add all the new codes in the toValue
						// expressions. (could be different classes than the
						// from values.)
						// can only be numeric constant, string constant, or
						// missing vlaue constant
						if (rule.getTo() != null) {
							if (StringConstantExpression.class.isAssignableFrom(toClass)
									|| NumericConstantExpression.class.isAssignableFrom(toClass)) {
								// TODO set the to value
							} else if (MissingValueConstantExpression.class.isAssignableFrom(toClass)) {
								// TODO do missing value constant stuff.
							} else {
								ValidationResult v = new ValidationResult();
								v.setValid(false);
								v.addMessages(
										"RecodeRule \"toValue\" [" + rule.getTo() + "]is not of the expected type");
								// TODO add to validation?
								// throw exception bc the docs say it can only
								// be one of these three.
							}
						}
					}
				}
			}
		}

		update.setNewCodes(newCodes.toArray(new CodeDetail[newCodes.size()]));
		update.setNewCodeRanges(newCodeRanges.toArray(new CodeRangeDetail[newCodeRanges.size()]));
		update.setRemovedCodeRanges(removedCodeRanges.toArray(new Range[removedCodeRanges.size()]));
		update.setRemovedCodes(removedCodes.toArray(new String[removedCodes.size()]));
		return update;
	}

	/**
	 * for every recode variable in the sdtl, make a NewVairable object. Set all
	 * of the information onto this NewVariable, except for if its a self
	 * recode, in which case the baseVarName should be left null to let the
	 * updater know not to create a new variable.
	 */
	@Override
	public NewVariable[] getNewVariables() {
		List<NewVariable> newVars = new ArrayList<>();
		for (RecodeVariable rv : sdtl.getRecodedVariables()) {
			NewVariable newVar = new NewVariable();
			String baseVarName;
			if (rv.getSource().equals(rv.getTarget())) {
				// don't set base variable name if source=target
				break;
			} else {
				baseVarName = rv.getSource();
			}
			newVar.setNewVariableName(rv.getTarget());
			newVar.setBasisVariableName(baseVarName);
			newVars.add(newVar);
		}
		return newVars.toArray(new NewVariable[newVars.size()]);
	}

	/**
	 * all recodes should be done in a new classification to preserve the
	 * original classification that may be used by other variables. Defaults to
	 * true. {@inheritDoc}
	 */
	@Override
	public boolean requiresCopyOfClassification() {
		return requiresCopyOfClassification;
	}

	public void setRequiresCopyOfClassification(boolean requiresCopyOfClassification) {
		this.requiresCopyOfClassification = requiresCopyOfClassification;
	}

	/**
	 * Carry over floating/unused codes. Defaults to true. {@inheritDoc}
	 */
	@Override
	public boolean copyFloatingCodes() {
		/*
		 * * if the special from value is "unhandled" and the specialToValue is
		 * "copy", carry over floating/unused codes. otherwise return false, as
		 * these should not be carried over by default.
		 */

		// for (RecodeRule rule : sdtl.getRules()) {
		// if ("unhandled".equals(rule.getSpecialFromValue()) &&
		// "copy".equals(rule.getSpecialToValue())) {
		// return true; }}
		// return false;
		return copyFloatingCodes;
	}

	/**
	 * Sets whether a classification's floating/unused codes should be carried
	 * over. Default to true.
	 * 
	 * @param copyFloatingCodes
	 */
	public void setCopyFloatingCodes(boolean copyFloatingCodes) {
		this.copyFloatingCodes = copyFloatingCodes;
	}

}

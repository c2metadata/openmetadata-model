package us.mtna.data.transform.wrapper.sdtl;

import org.c2metadata.sdtl.pojo.command.TransformBase;
import org.c2metadata.sdtl.pojo.expression.ExpressionBase;

import us.mtna.data.transform.command.ds.UpdatesCases;

public class KeepCases implements UpdatesCases {

	private final org.c2metadata.sdtl.pojo.command.KeepCases sdtl;

	public KeepCases(org.c2metadata.sdtl.pojo.command.KeepCases sdtl) {
		this.sdtl = sdtl;
	}

	@Override
	public TransformBase getOriginalCommand() {
		return sdtl;
	}

	@Override
	public ValidationResult validate() {
		return new ValidationResult();
	}

	@Override
	public ExpressionBase getCondition() {
		return sdtl.getCondition();
	}

}

package us.mtna.data.transform.wrapper.sdtl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.c2metadata.sdtl.pojo.command.TransformBase;

import us.mtna.data.transform.command.CreatesVariables;
import us.mtna.data.transform.command.DeletesVariable;
import us.mtna.data.transform.command.SelectsVariables;
import us.mtna.data.transform.command.object.NewVariable;
import us.mtna.data.transform.command.object.Range;

public class ReshapeLong implements CreatesVariables, SelectsVariables, DeletesVariable {
	// reorders dataset? reshapes dataset?
	private final org.c2metadata.sdtl.pojo.command.ReshapeLong sdtl;

	public ReshapeLong(org.c2metadata.sdtl.pojo.command.ReshapeLong sdtl) {
		this.sdtl = sdtl;
	}

	@Override
	public boolean preserveOriginalVariable() {
		return true;
	}
	
	@Override
	public TransformBase getOriginalCommand() {
		return sdtl;
	}

	@Override
	public ValidationResult validate() {
		return new ValidationResult();
	}

	@Override
	public Set<String> getDeletedVars() {
		//should getDeletedVars also have a range option? ReshapeLong has the option
		return getVariablesFromVariableReferenceBase(sdtl.getDropVariables());
	}

	@Override
	public List<Range> getRanges() {
		//idVariables? 
		return new ArrayList<>();
	}

	@Override
	public Set<String> getVariables() {
		//make items, case number variable, idVariables?, keepVariables
		return new HashSet<>();
	}

	@Override
	public NewVariable[] getNewVariables() {
		//case number variable , keep variables? 
		return new NewVariable[0];
	}

	@Override
	public List<Range> getDeletedVariableRanges() {
		return getRangesFromVariableReferenceBase(sdtl.getDropVariables());

	}

	@Override
	public Set<String> getKeepVars() {
		return new HashSet<>();
	}

	@Override
	public List<Range> getKeepVariableRanges() {
		return new ArrayList<>();
	}
}

package us.mtna.data.transform.wrapper.sdtl;

import org.c2metadata.sdtl.pojo.command.InformBase;

import us.mtna.data.transform.command.ds.NonTransform;

public class NoTransformOp implements NonTransform{

	private final org.c2metadata.sdtl.pojo.command.NoTransformOp sdtl;

	public NoTransformOp(org.c2metadata.sdtl.pojo.command.NoTransformOp sdtl){
		this.sdtl = sdtl;
	}

	@Override
	public InformBase getOriginalCommand() {
		return sdtl;
	}

	@Override
	public ValidationResult validate() {
		return new ValidationResult();
	}

}

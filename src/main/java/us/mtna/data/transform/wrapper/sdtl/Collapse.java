package us.mtna.data.transform.wrapper.sdtl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.c2metadata.sdtl.pojo.command.TransformBase;
import org.c2metadata.sdtl.pojo.expression.VariableReferenceBase;

import us.mtna.data.transform.command.CreatesVariables;
import us.mtna.data.transform.command.SdtlWrapper;
import us.mtna.data.transform.command.ds.CollapsesDataset;
import us.mtna.data.transform.command.object.NewVariable;

//loads dataset? merges dataset?
public class Collapse implements CollapsesDataset, CreatesVariables {

	private final org.c2metadata.sdtl.pojo.command.Collapse sdtl;

	@Override
	public ValidationResult validate() {
		ValidationResult result = new ValidationResult();
		if (sdtl.getAggregateVariables() == null || sdtl.getAggregateVariables().length < 1) {
			result.addMessages("No aggregation variabes found on command ["
					+ sdtl.getSourceInformation().getOriginalSourceText() + "]");
			result.setValid(false);
		}
		return result;
	}

	public Collapse(org.c2metadata.sdtl.pojo.command.Collapse sdtl) {
		this.sdtl = sdtl;
	}

	@Override
	public TransformBase getOriginalCommand() {
		return sdtl;
	}

	@Override
	public String getOutputDatasetName() {
		return sdtl.getOutputDatasetName();
	}
	
	@Override
	public boolean preserveOriginalVariable() {
		return false;
	}

	@Override
	public List<Compute> getAggregateVariables() {
		// passing these to the updater to parse since a lot of that logic is
		// there already.
		List<Compute> wrappedComputes = new ArrayList<>();
		for (org.c2metadata.sdtl.pojo.command.Compute base : sdtl.getAggregateVariables()) {
			SdtlWrapper wrapped = WrapperFactory.wrap(base);
			// make sure its not an unknown command
			if (Compute.class.isAssignableFrom(wrapped.getClass())) {
				Compute compute = (Compute) wrapped;
				wrappedComputes.add(compute);
			}
		}
		return wrappedComputes;
	}

	@Override
	public List<VariableReferenceBase> getGroupByVariables() {
		if (sdtl.getGroupByVariables() != null) {
			return new ArrayList<>(Arrays.asList(sdtl.getGroupByVariables()));
		} else {
			return new ArrayList<>();
		}
	}

	// the aggregated vars are the new ones
	@Override
	public NewVariable[] getNewVariables() {
		// parse the aggregated computes
		List<NewVariable> newVariables = new ArrayList<>();
		for (org.c2metadata.sdtl.pojo.command.Compute compute : sdtl.getAggregateVariables()) {
			Compute computeWrapper = (Compute) WrapperFactory.wrap(compute);
			newVariables.addAll(Arrays.asList(computeWrapper.getNewVariables()));
		}
		return newVariables.toArray(new NewVariable[0]);
	}

}

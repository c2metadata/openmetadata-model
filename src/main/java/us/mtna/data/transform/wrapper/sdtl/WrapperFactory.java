/**
<Overview>
 
Copyright 2017 Metadata Technology North America Inc.
 
This software is the result of collaborative efforts from all participants of the C2Metadata project (http://www.c2metadata.org)
 
C2Metadata is supported by the Data Infrastructure Building Blocks (DIBBs) program of the National Science Foundation through grant NSF ACI-1640575.
 
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 
**/
package us.mtna.data.transform.wrapper.sdtl;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import org.c2metadata.sdtl.pojo.command.CommandBase;
import org.slf4j.LoggerFactory;

import us.mtna.data.transform.command.SdtlWrapper;

/**
 * Creates a SdtlWrapper from a TransformBase command
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public class WrapperFactory {

	private static final String wrapperPackage = "us.mtna.data.transform.wrapper.sdtl.";
	private static org.slf4j.Logger log = LoggerFactory.getLogger(WrapperFactory.class);

	public static void main(String[] args) {
		org.c2metadata.sdtl.pojo.command.Recode cogsRecode = new org.c2metadata.sdtl.pojo.command.Recode();
		SdtlWrapper wrapper = wrap(cogsRecode);
		System.out.println(wrapper.getClass());
	}

	/**
	 * Creates a SdtlWrapper from a TransformBase command with the same name. If
	 * a matching wrapper class is not found, an {@link UnknownCommand} is
	 * returned.
	 * 
	 * @param command
	 * @return
	 */
	public static SdtlWrapper wrap(CommandBase command) {
		Class<? extends CommandBase> commandClass = command.getClass();
		String wrapperClassName = wrapperPackage + commandClass.getSimpleName();

		try {
			@SuppressWarnings("unchecked")
			Class<? extends SdtlWrapper> wrapperClass = (Class<? extends SdtlWrapper>) Class.forName(wrapperClassName);
			Constructor<? extends SdtlWrapper> constructor = wrapperClass.getConstructor(commandClass);
			log.trace("creating new instance of constructor [" + constructor.getName() + "] with command["
					+ commandClass + "]");
			return constructor.newInstance(command);
		} catch (ClassNotFoundException e) {
			log.debug("Class was not found when trying to create constructor for command [" + command.getCommand()
					+ "]. Creating an UnknownCommand instead.");
			return new UnknownCommand(command);
		} catch (NoSuchMethodException | SecurityException e) {
			throw new RuntimeException("constructor not found for [" + commandClass + "]", e);
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException
				| InvocationTargetException e) {
			throw new RuntimeException("error invoking constructor for [" + commandClass + "]", e);
		}
	}

	public static org.slf4j.Logger getLog() {
		return log;
	}

	public static void setLog(org.slf4j.Logger log) {
		WrapperFactory.log = log;
	}

}

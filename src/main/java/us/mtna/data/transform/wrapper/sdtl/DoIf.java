package us.mtna.data.transform.wrapper.sdtl;

import org.c2metadata.sdtl.pojo.command.TransformBase;

import us.mtna.data.transform.command.ds.Transform;

public class DoIf implements Transform {
	// interfaces to implement?

	private final org.c2metadata.sdtl.pojo.command.DoIf sdtl;

	public DoIf(org.c2metadata.sdtl.pojo.command.DoIf sdtl) {
		this.sdtl = sdtl;
	}

	@Override
	public TransformBase getOriginalCommand() {
		return sdtl;
	}

	@Override
	public ValidationResult validate() {
		ValidationResult result = new ValidationResult();
		// conditions are not technically required, but they kind of are to
		// have a command that makes sense.
		if (sdtl.getCondition() == null) {
			result.setValid(false);
			result.addMessages("No conditions found to evaluate on DoIf command ["
					+ sdtl.getSourceInformation().getOriginalSourceText() + "]");
		}
		if (sdtl.getThenCommands() == null || sdtl.getThenCommands().length < 1) {
			result.setValid(false);
			result.addMessages("No thenCommands found to evaluate on DoIf command ["
					+ sdtl.getSourceInformation().getOriginalSourceText() + "]");
		}

		return result;
	}

}

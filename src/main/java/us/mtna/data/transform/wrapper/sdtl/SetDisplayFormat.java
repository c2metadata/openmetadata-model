package us.mtna.data.transform.wrapper.sdtl;

import java.util.List;
import java.util.Set;

import org.c2metadata.sdtl.pojo.command.TransformBase;

import us.mtna.data.transform.command.SelectsVariables;
import us.mtna.data.transform.command.object.Range;

public class SetDisplayFormat implements SelectsVariables { // UpdatesVariable?

	private final org.c2metadata.sdtl.pojo.command.SetDisplayFormat sdtl;

	public SetDisplayFormat(org.c2metadata.sdtl.pojo.command.SetDisplayFormat sdtl) {
		this.sdtl = sdtl;
	}

	@Override
	public TransformBase getOriginalCommand() {
		return sdtl;
	}

	@Override
	public List<Range> getRanges() {
		return getRangesFromVariableReferenceBaseArray(sdtl.getVariables());
	}

	@Override
	public Set<String> getVariables() {
		return getVariablesFromVariableReferenceBaseArray(sdtl.getVariables());
	}

	@Override
	public ValidationResult validate() {
		ValidationResult result = new ValidationResult();
		boolean valid = true;
		if (sdtl.getVariables() == null) {
			String message = "SetDisplayFormat SDTL does not define any variables in command ["
					+ sdtl.getSourceInformation().getOriginalSourceText() + "]";
			valid = false;
			result.addMessages(message);

		}
		result.setValid(valid);
		return result;
	}

}

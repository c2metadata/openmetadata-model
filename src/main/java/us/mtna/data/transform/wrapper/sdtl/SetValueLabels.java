/**
<Overview>
 
Copyright 2017 Metadata Technology North America Inc.
 
This software is the result of collaborative efforts from all participants of the C2Metadata project (http://www.c2metadata.org)
 
C2Metadata is supported by the Data Infrastructure Building Blocks (DIBBs) program of the National Science Foundation through grant NSF ACI-1640575.
 
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 
**/
package us.mtna.data.transform.wrapper.sdtl;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.c2metadata.sdtl.pojo.ValueLabel;
import org.c2metadata.sdtl.pojo.command.TransformBase;

import us.mtna.data.transform.command.SelectsVariables;
import us.mtna.data.transform.command.UpdatesClassification;
import us.mtna.data.transform.command.object.ClassificationUpdate;
import us.mtna.data.transform.command.object.CodeDetail;
import us.mtna.data.transform.command.object.Range;

public class SetValueLabels implements SelectsVariables, UpdatesClassification {

	private final org.c2metadata.sdtl.pojo.command.SetValueLabels sdtl;
	private boolean requiresCopyOfClassification;
	private boolean copyFloatingCodes;

	public SetValueLabels(org.c2metadata.sdtl.pojo.command.SetValueLabels sdtl) {
		this.sdtl = sdtl;
		requiresCopyOfClassification = false;
		copyFloatingCodes = true;
	}

	@Override
	public ValidationResult validate() {
		ValidationResult result = new ValidationResult();
		if (sdtl.getLabels() == null || sdtl.getLabels().length < 1) {
			result.setValid(false);
			result.addMessages("SetValueLabels SDTL does not have any value labels to assign in command ["
					+ sdtl.getSourceInformation().getOriginalSourceText() + "]");
		}
		if (sdtl.getVariables() == null || sdtl.getVariables().length < 1) {
			result.setValid(false);
			result.addMessages("SetValueLabels SDTL does not define any variables in command ["
					+ sdtl.getSourceInformation().getOriginalSourceText() + "]");
		}
		return result;
	}

	@Override
	public TransformBase getOriginalCommand() {
		return sdtl;
	}

	@Override
	public List<Range> getRanges() {
		return getRangesFromVariableReferenceBaseArray(sdtl.getVariables());
	}

	@Override
	public Set<String> getVariables() {
		return getVariablesFromVariableReferenceBaseArray(sdtl.getVariables());
	}

	/**
	 * SetValueLabels SDTL should have at least one value label pair
	 */
	@Override
	public ClassificationUpdate getUpdate() {
		ClassificationUpdate update = new ClassificationUpdate();
		List<CodeDetail> updatesCodes = new ArrayList<>();
		if (sdtl.getLabels() != null) {
			for (ValueLabel pair : sdtl.getLabels()) {
				CodeDetail detail = new CodeDetail();
				detail.setLabel(pair.getLabel());
				detail.setNewValue(pair.getValue());
				// detail.setFromValue(pair.getValue());
				detail.setFromValue(pair.getValue());
				updatesCodes.add(detail);
			}
		}
		update.setUpdatesCodes(updatesCodes.toArray(new CodeDetail[updatesCodes.size()]));
		return update;
	}

	// you can modify the original classif with a value label change.
	/**
	 * defaults to false {@inheritDoc}
	 */
	@Override
	public boolean requiresCopyOfClassification() {
		return requiresCopyOfClassification;
	}

	public void setRequiresCopyOfClassification(boolean requiresCopyOfClassification) {
		this.requiresCopyOfClassification = requiresCopyOfClassification;
	}

	@Override
	public boolean copyFloatingCodes() {
		return copyFloatingCodes;
	}

	public void setCopyFloatingCodes(boolean copyFloatingCodes) {
		this.copyFloatingCodes = copyFloatingCodes;
	}
}

package us.mtna.data.transform.wrapper.sdtl;

import java.util.ArrayList;

/**
 * A wrapper to hold a validation boolean and a message about what is wrong or
 * missing from the sdtl command.
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public class ValidationResult {

	private boolean valid;
	private ArrayList<String> messages;

	public ValidationResult() {
		valid = true;
		this.messages = new ArrayList<>();
	}

	public ValidationResult(boolean isValid) {
		this.valid = isValid;
	}

	public boolean isValid() {
		return valid;
	}

	public void setValid(boolean valid) {
		this.valid = valid;
	}

	public ArrayList<String> getMessages() {
		return messages;
	}

	public void setMessages(ArrayList<String> messages) {
		this.messages.addAll(messages);
	}

	public void addMessages(String...messages) {
		for(String message : messages){
			this.messages.add(message);
		}
	}

	/**
	 * Converts messages to string.
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		if (valid) {
			sb.append("All transforms in this program appear to be valid.");
		} else {
			sb.append("Invalid transforms: ");
			int i = 0;
			for (String message : messages) {
				if (i > 0) {
					sb.append(",\n ");
				}
				sb.append(message);
				i++;
			}
		}
		return sb.toString();
	}

}

package us.mtna.data.transform.wrapper.sdtl;

import org.c2metadata.sdtl.pojo.command.InformBase;

import us.mtna.data.transform.command.ds.NonTransform;

public class Unsupported implements NonTransform {
	
	private final org.c2metadata.sdtl.pojo.command.Unsupported sdtl;

	@Override
	public ValidationResult validate() {
		return new ValidationResult();
	}
	
	public Unsupported(org.c2metadata.sdtl.pojo.command.Unsupported sdtl) {
		this.sdtl = sdtl;
	}

	@Override
	public InformBase getOriginalCommand() {
		return sdtl;
	}
}

/**
<Overview>
 
Copyright 2017 Metadata Technology North America Inc.
 
This software is the result of collaborative efforts from all participants of the C2Metadata project (http://www.c2metadata.org)
 
C2Metadata is supported by the Data Infrastructure Building Blocks (DIBBs) program of the National Science Foundation through grant NSF ACI-1640575.
 
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 
**/
package us.mtna.data.transform.wrapper.sdtl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.c2metadata.sdtl.pojo.command.TransformBase;

import us.mtna.data.transform.command.DeletesVariable;
import us.mtna.data.transform.command.SelectsVariables;
import us.mtna.data.transform.command.object.Range;

public class KeepVariables implements SelectsVariables, DeletesVariable {

	private org.c2metadata.sdtl.pojo.command.KeepVariables sdtl;

	public KeepVariables(org.c2metadata.sdtl.pojo.command.KeepVariables sdtl) {
		this.sdtl = sdtl;
	}

	@Override
	public ValidationResult validate() {
		ValidationResult result = new ValidationResult();
		return result;
	}

	@Override
	public TransformBase getOriginalCommand() {
		return sdtl;
	}

	@Override
	public List<Range> getRanges() {
		return getRangesFromVariableReferenceBaseArray(sdtl.getVariables());
	}

	/**
	 * Keep SDTL should have either a variable range expression or string array
	 * of variable names to be kept
	 */
	@Override
	public Set<String> getVariables() {
		return getVariablesFromVariableReferenceBaseArray(sdtl.getVariables());
	}

	@Override
	public Set<String> getDeletedVars() {
		return new HashSet<>();
	}

	@Override
	public List<Range> getDeletedVariableRanges() {
		return new ArrayList<>();
	}

	@Override
	public Set<String> getKeepVars() {
		return getVariablesFromVariableReferenceBaseArray(sdtl.getVariables());
	}

	@Override
	public List<Range> getKeepVariableRanges() {
		return getRangesFromVariableReferenceBaseArray(sdtl.getVariables());
	}

}

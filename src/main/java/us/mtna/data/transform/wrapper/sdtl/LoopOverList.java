package us.mtna.data.transform.wrapper.sdtl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.c2metadata.sdtl.pojo.IterationDescription;
import org.c2metadata.sdtl.pojo.command.TransformBase;

import us.mtna.data.transform.command.SelectsVariables;
import us.mtna.data.transform.command.object.Range;

/**
 * This is a temporary placeholder, this and LoopOverValues will eventually be
 * replaced with a single Loop Command
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public class LoopOverList implements SelectsVariables {

	private final org.c2metadata.sdtl.pojo.command.LoopOverList sdtl;

	public LoopOverList(org.c2metadata.sdtl.pojo.command.LoopOverList sdtl) {
		this.sdtl = sdtl;
	}

	@Override
	public ValidationResult validate() {
		ValidationResult result = new ValidationResult();
		if (sdtl.getIterators() == null || sdtl.getIterators().length < 1) {
			result.addMessages("No iterators found on the LoopOverList command ["
					+ sdtl.getSourceInformation().getOriginalSourceText() + "] ");
			result.setValid(false);
		} else {
			// check the iterator description
			int i = 0;
			for (IterationDescription iterator : sdtl.getIterators()) {
				if (iterator.getIteratorSymbolName() == null) {
					result.addMessages("The iteratorDescription at index " + i
							+ " is missing an IteratorSymbolName in the LoopOverList command ["
							+ sdtl.getSourceInformation().getOriginalSourceText() + "] ");
					result.setValid(false);
				}
				if (iterator.getIteratorValues() == null || iterator.getIteratorValues().length < 1) {
					result.addMessages("The iteratorDescription at index " + i
							+ " is missing the iteratorValues in the LoopOverList command ["
							+ sdtl.getSourceInformation().getOriginalSourceText() + "] ");
					result.setValid(false);
				}
				i++;
			}
		}
		if (sdtl.getCommands() == null || sdtl.getCommands().length < 1) {
			result.addMessages("No commands found on the LoopOverList command ["
					+ sdtl.getSourceInformation().getOriginalSourceText() + "] ");
			result.setValid(false);
		}

		return result;
	}

	@Override
	public TransformBase getOriginalCommand() {
		return sdtl;
	}

	@Override
	public List<Range> getRanges() {
		List<Range> ranges = new ArrayList<>();
		for (TransformBase base : sdtl.getCommands()) {
			ranges.addAll(getRangesFromTransformBase(base));
			// need to check for nested expressions?
		}
		return ranges;
	}

	// need to parse the nested commands and get the affected variables.
	@Override
	public Set<String> getVariables() {
		HashSet<String> vars = new HashSet<>();
		// get these from commands (nested)
		for (TransformBase base : sdtl.getCommands()) {
			vars.addAll(parseTransformBase(base));
			// need to check for nested expressions?
		}
		return vars;
	}

	// @Override
	// public NewVariable[] getNewVariables() {
	// List<NewVariable> newVars = new ArrayList<>();
	// //ExpressionBase expr = sdtl.getExpression();
	// TransformBase[] expr = sdtl.getCommands();
	//
	// if (sdtl.getVariables() != null) {
	// for (VariableReferenceBase base : sdtl.getVariables()) {
	// NewVariable newVar = new NewVariable();
	//
	// if (VariableSymbolExpression.class.isAssignableFrom(base.getClass())) {
	// VariableSymbolExpression symbol = (VariableSymbolExpression) base;
	// if (symbol.getVariableName() != null) {
	// newVar.setNewVariableName(symbol.getVariableName());
	// // checkForNestedExpressions(expr, newVar);
	//
	// } else {
	// continue;
	// }
	// newVars.add(newVar);
	// } else if
	// (VariableRangeExpression.class.isAssignableFrom(base.getClass())) {
	// VariableRangeExpression rangeExpr = (VariableRangeExpression) base;
	// Range range = new Range();
	// if (rangeExpr.getFirst() != null) {
	// range.setStart(rangeExpr.getFirst());
	// } else if (rangeExpr.getLast() != null) {
	// range.setEnd(rangeExpr.getLast());
	// }
	// newVar.setSourceVariableRange(range);
	// newVars.add(newVar);
	// }
	// }
	// } else {
	// log.error("Compute command [" + sdtl.getCommand().toString() + "] does
	// not list variables ");
	// }
	// return newVars.toArray(new NewVariable[newVars.size()]);
	// }

}

package us.mtna.data.transform.wrapper.sdtl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.c2metadata.sdtl.pojo.SortCriterion;
import org.c2metadata.sdtl.pojo.command.TransformBase;

import us.mtna.data.transform.command.SelectsVariables;
import us.mtna.data.transform.command.ds.ReordersDataset;
import us.mtna.data.transform.command.object.Range;

/**
 * Not yet supported by the spss parser as of 1/24/19
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public class SortCases implements SelectsVariables, ReordersDataset {
	// dont really think this would implement reorders dataset because you're
	// just reordering the cases, not the variables.
	// also this needs review.
	private final org.c2metadata.sdtl.pojo.command.SortCases sdtl;

	public SortCases(org.c2metadata.sdtl.pojo.command.SortCases sdtl) {
		this.sdtl = sdtl;
	}

	@Override
	public TransformBase getOriginalCommand() {
		return sdtl;
	}

	@Override
	public ValidationResult validate() {
		ValidationResult result = new ValidationResult();
		if (sdtl.getSortCriteria() == null || sdtl.getSortCriteria().length < 1) {
			result.setValid(false);
			result.addMessages("No SortCriteria found on the SortCases command ["
					+ sdtl.getSourceInformation().getOriginalSourceText() + "]");

		} else {
			int i = 0;
			for (SortCriterion crit : sdtl.getSortCriteria()) {
				// validate the sort criterion
				if (crit.getVariable() == null) {
					result.setValid(false);
					result.addMessages("No variable found on the SortCriterion at index [ " + i + " ] on command ["
							+ sdtl.getSourceInformation().getOriginalSourceText() + "]");
				}
				if (crit.getSortDirection() == null) {
					result.setValid(false);
					result.addMessages("No sort direction found on the SortCriterion at index [ " + i
							+ " ] on command [" + sdtl.getSourceInformation().getOriginalSourceText() + "]");
				}
				i++;
			}
		}
		return result;
	}

	@Override
	public List<Range> getRanges() {
		List<Range> vars = new ArrayList<>();
		for (SortCriterion crit : sdtl.getSortCriteria()) {
			vars.addAll(getRangesFromVariableReferenceBase(crit.getVariable()));
		}
		return vars;
	}

	@Override
	public Set<String> getVariables() {
		Set<String> vars = new LinkedHashSet<>();
		for (SortCriterion crit : sdtl.getSortCriteria()) {
			vars.addAll(getVariablesFromVariableReferenceBase(crit.getVariable()));
		}
		return vars;
	}

	@Override
	public Set<String> getVariableOrder() {
		// sort does not define a new variable order
		return new HashSet<>();
	}

	@Override
	public Set<String> getSortingVariables() {
		Set<String> vars = new HashSet<>();
		for (SortCriterion criterion : sdtl.getSortCriteria()) {
			vars.add(getVariablesFromVariableReferenceBase(criterion.getVariable()).iterator().next());
		}
		return vars;
	}

}

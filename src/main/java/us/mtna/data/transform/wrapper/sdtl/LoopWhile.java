package us.mtna.data.transform.wrapper.sdtl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.c2metadata.sdtl.pojo.command.TransformBase;

import us.mtna.data.transform.command.SelectsVariables;
import us.mtna.data.transform.command.object.Range;

public class LoopWhile implements SelectsVariables {

	private final org.c2metadata.sdtl.pojo.command.LoopWhile sdtl;

	public LoopWhile(org.c2metadata.sdtl.pojo.command.LoopWhile sdtl) {
		this.sdtl = sdtl;
	}

	@Override
	public TransformBase getOriginalCommand() {
		return sdtl;
	}

	@Override
	public ValidationResult validate() {
		ValidationResult result = new ValidationResult();
		if (sdtl.getCommands() == null || sdtl.getCommands().length < 1) {
			result.addMessages("No iterator commands found on command [" + sdtl.getSourceInformation().getOriginalSourceText() + "]");
			result.setValid(false);
		}
		return result;
	}

	@Override
	public List<Range> getRanges() {
		List<Range> ranges = new ArrayList<>();
		for (TransformBase base : sdtl.getCommands()) {
			ranges.addAll(getRangesFromTransformBase(base));
			//need to check for nested expressions? 
		}
		return ranges;
	}

	@Override
	public Set<String> getVariables() {
		HashSet<String> vars = new HashSet<>();
		// get these from commands (nested)
		for (TransformBase base : sdtl.getCommands()) {
			vars.addAll(parseTransformBase(base));
			//need to check for nested expressions? 
		}
		return vars;
	}

}

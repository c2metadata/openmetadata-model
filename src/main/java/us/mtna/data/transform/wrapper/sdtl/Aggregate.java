package us.mtna.data.transform.wrapper.sdtl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.c2metadata.sdtl.pojo.command.TransformBase;

import us.mtna.data.transform.command.CreatesVariables;
import us.mtna.data.transform.command.SdtlWrapper;
import us.mtna.data.transform.command.SelectsVariables;
import us.mtna.data.transform.command.object.NewVariable;
import us.mtna.data.transform.command.object.Range;

public class Aggregate implements CreatesVariables, SelectsVariables {

	private final org.c2metadata.sdtl.pojo.command.Aggregate sdtl;

	public Aggregate(org.c2metadata.sdtl.pojo.command.Aggregate sdtl) {
		this.sdtl = sdtl;
	}

	@Override
	public TransformBase getOriginalCommand() {
		return sdtl;
	}
	
	@Override
	public boolean preserveOriginalVariable() {
		return false;
	}

	// group by vars should be noted at the dataset level
	@Override
	public ValidationResult validate() {
		ValidationResult result = new ValidationResult();
		if (sdtl.getAggregateVariables() == null || sdtl.getAggregateVariables().length < 1) {
			result.setValid(false);
			result.addMessages("No aggregate variables found on command ["
					+ sdtl.getSourceInformation().getOriginalSourceText() + "]");
		}
		// wrap and validate all the compute commands in the aggregate vars
		for (TransformBase base : sdtl.getAggregateVariables()) {
			if (org.c2metadata.sdtl.pojo.command.Compute.class.isAssignableFrom(base.getClass())) {
				SdtlWrapper wrapped = WrapperFactory.wrap(base);
				// make sure its not an unknown command
				if (Compute.class.isAssignableFrom(wrapped.getClass())) {
					Compute compute = (Compute) wrapped;
					ValidationResult computeResult = compute.validate();
					result.addMessages(
							computeResult.getMessages().toArray(new String[computeResult.getMessages().size()]));
					if (!computeResult.isValid()) {
						result.setValid(false);
					}
				}
			}
		}
		return result;
	}

	@Override
	public NewVariable[] getNewVariables() {
		// this would come from the aggregated variables. so parse out the
		// computes?
		List<NewVariable> newVars = new ArrayList<>();
		for (TransformBase base : sdtl.getAggregateVariables()) {
			// it should always be a compute so don't test other transforms
			if (org.c2metadata.sdtl.pojo.command.Compute.class.isAssignableFrom(base.getClass())) {
				SdtlWrapper wrapped = WrapperFactory.wrap(base);
				// make sure its not an unknown command
				if (Compute.class.isAssignableFrom(wrapped.getClass())) {
					Compute compute = (Compute) wrapped;
					newVars.addAll(Arrays.asList(compute.getNewVariables()));
				}
			}
		}
		return newVars.toArray(new NewVariable[0]);
	}

	@Override
	public List<Range> getRanges() {
		return new ArrayList<>();
	}

	@Override
	public Set<String> getVariables() {
		// TODO add in weight variable and group by variables?
		Set<String> newVars = new HashSet<>();
		for (NewVariable nv : getNewVariables()) {
			// not sure which one to use
			newVars.add(nv.getNewVariableName());
			nv.getBasisVariableName();
		}
		return newVars;
	}

}

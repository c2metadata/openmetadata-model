package us.mtna.data.transform.wrapper.sdtl;

import java.util.List;
import java.util.Set;

import org.c2metadata.sdtl.pojo.command.TransformBase;

import us.mtna.data.transform.command.SelectsVariables;
import us.mtna.data.transform.command.UpdatesVariables;
import us.mtna.data.transform.command.object.Range;
import us.mtna.data.transform.command.object.VariableNamePair;
import us.mtna.pojo.DataType;

public class SetDataType implements SelectsVariables, UpdatesVariables {

	private final org.c2metadata.sdtl.pojo.command.SetDataType sdtl;

	public SetDataType(org.c2metadata.sdtl.pojo.command.SetDataType sdtl) {
		this.sdtl = sdtl;
	}

	@Override
	public TransformBase getOriginalCommand() {
		return sdtl;
	}

	@Override
	public ValidationResult validate() {
		ValidationResult result = new ValidationResult();
		boolean valid = true;
		if (sdtl.getDataType() == null) {
			String message = "SetDataType SDTL is missing a data type string in command ["
					+ sdtl.getSourceInformation().getOriginalSourceText() + "]";
			valid = false;
			result.addMessages(message);
		}
		result.setValid(valid);
		return result;
	}

	@Override
	public List<Range> getRanges() {
		return getRangesFromVariableReferenceBaseArray(sdtl.getVariables());
	}

	@Override
	public Set<String> getVariables() {
		return getVariablesFromVariableReferenceBaseArray(sdtl.getVariables());
	}

	@Override
	public VariableNamePair[] getUpdatedVariables() {

		return new VariableNamePair[0];
	}

	public DataType parseDataType(String dt) {
		switch (dt.toUpperCase().trim()) {

		case "NUMERIC":
			return DataType.NUMBER;
		case "BOOLEAN":
			return DataType.BOOLEAN;
		case "DATE-TIME":
		case "DATETIME":
			return DataType.DATE;
		case "FACTOR":
			return DataType.FACTOR;
		case "TEXT":
		default:
			return DataType.STRING;
		}
	}

	@Override
	public DataType getDataType() {
		return parseDataType(sdtl.getDataType());
	}


}

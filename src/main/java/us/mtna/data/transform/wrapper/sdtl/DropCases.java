package us.mtna.data.transform.wrapper.sdtl;

import org.c2metadata.sdtl.pojo.command.TransformBase;
import org.c2metadata.sdtl.pojo.expression.ExpressionBase;

import us.mtna.data.transform.command.ds.UpdatesCases;

//TODOO unsure what interfaces to implement
public class DropCases implements UpdatesCases{

	private final org.c2metadata.sdtl.pojo.command.DropCases sdtl;

	public DropCases(org.c2metadata.sdtl.pojo.command.DropCases sdtl){
		this.sdtl = sdtl;
	}
	
	@Override
	public TransformBase getOriginalCommand() {
		return sdtl;
	}

	@Override
	public ValidationResult validate() {
		ValidationResult result = new ValidationResult();
		boolean valid = true;
		if (sdtl.getCondition() == null) {
			String message = "DropCases SDTL is missing a condition in command ["
					+ sdtl.getSourceInformation().getOriginalSourceText() + "]";
			valid = false;
			result.addMessages(message);
		}
		result.setValid(valid);
		return result;
	}

	@Override
	public ExpressionBase getCondition() {
		return sdtl.getCondition();
	}

}

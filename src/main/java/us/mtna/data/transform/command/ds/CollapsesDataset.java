package us.mtna.data.transform.command.ds;

import java.util.List;

import org.c2metadata.sdtl.pojo.expression.VariableReferenceBase;

import us.mtna.data.transform.wrapper.sdtl.Compute;

/**
 * An collapse command summarizes data using aggregation functions applied to
 * data that may be grouped by one or more variables. The resulting summary data
 * is represented in a new dataset.
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public interface CollapsesDataset extends Transform {

	String getOutputDatasetName();

	List<Compute> getAggregateVariables();

	List<VariableReferenceBase> getGroupByVariables();

}

package us.mtna.data.transform.command.ds;

public interface UpdatesDataset extends Transform{

	String getUpdatedProperty();
	
	String getUpdatedValue();
	
}

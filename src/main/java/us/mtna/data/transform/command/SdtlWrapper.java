/**
<Overview>
 
Copyright 2017 Metadata Technology North America Inc.
 
This software is the result of collaborative efforts from all participants of the C2Metadata project (http://www.c2metadata.org)
 
C2Metadata is supported by the Data Infrastructure Building Blocks (DIBBs) program of the National Science Foundation through grant NSF ACI-1640575.
 
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 
**/
package us.mtna.data.transform.command;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.c2metadata.sdtl.pojo.CompositeVariableName;
import org.c2metadata.sdtl.pojo.FunctionArgument;
import org.c2metadata.sdtl.pojo.command.CommandBase;
import org.c2metadata.sdtl.pojo.command.Compute;
import org.c2metadata.sdtl.pojo.command.TransformBase;
import org.c2metadata.sdtl.pojo.expression.AllVariablesExpression;
import org.c2metadata.sdtl.pojo.expression.ExpressionBase;
import org.c2metadata.sdtl.pojo.expression.FunctionCallExpression;
import org.c2metadata.sdtl.pojo.expression.GroupedExpression;
import org.c2metadata.sdtl.pojo.expression.MissingValueConstantExpression;
import org.c2metadata.sdtl.pojo.expression.NumericConstantExpression;
import org.c2metadata.sdtl.pojo.expression.NumericMaximumValueExpression;
import org.c2metadata.sdtl.pojo.expression.NumericMinimumValueExpression;
import org.c2metadata.sdtl.pojo.expression.StringConstantExpression;
import org.c2metadata.sdtl.pojo.expression.VariableListExpression;
import org.c2metadata.sdtl.pojo.expression.VariableRangeExpression;
import org.c2metadata.sdtl.pojo.expression.VariableReferenceBase;
import org.c2metadata.sdtl.pojo.expression.VariableSymbolExpression;

import us.mtna.data.transform.command.object.MinMax;
import us.mtna.data.transform.command.object.NewVariable;
import us.mtna.data.transform.command.object.Range;
import us.mtna.data.transform.wrapper.sdtl.ValidationResult;
import us.mtna.data.transform.wrapper.sdtl.WrapperFactory;

/**
 * This is the base interface that all transform command interfaces should
 * implement to carry the original TransformBase(from pojo-sdtl) through to the
 * output.
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public interface SdtlWrapper {

	CommandBase getOriginalCommand();

	ValidationResult validate();

	default Set<String> parseExpression(ExpressionBase base) {
		Set<String> value = new HashSet<>();
		if (NumericConstantExpression.class.isAssignableFrom(base.getClass())) {
			NumericConstantExpression expr = (NumericConstantExpression) base;
			value.add(expr.getValue());
		} else if (StringConstantExpression.class.isAssignableFrom(base.getClass())) {
			StringConstantExpression expr = (StringConstantExpression) base;
			value.add(expr.getValue());
		} else if (MissingValueConstantExpression.class.isAssignableFrom(base.getClass())) {
			MissingValueConstantExpression expr = (MissingValueConstantExpression) base;
			value.add(expr.getValue());
		} else if (VariableSymbolExpression.class.isAssignableFrom(base.getClass())) {
			VariableSymbolExpression expr = (VariableSymbolExpression) base;
			value.add(expr.getVariableName());
		} else if (VariableListExpression.class.isAssignableFrom(base.getClass())) {
			VariableListExpression expr = (VariableListExpression) base;
			for (ExpressionBase inner : expr.getVariables()) {
				value.addAll(parseExpression(inner));
			}
		}

		else if (CompositeVariableName.class.isAssignableFrom(base.getClass())) {
			CompositeVariableName expr = (CompositeVariableName) base;
			// is this right?
			value.add(expr.getStub());
		} else if (FunctionCallExpression.class.isAssignableFrom(base.getClass())) {
			FunctionCallExpression expr = (FunctionCallExpression) base;
			for (FunctionArgument arg : expr.getArguments()) {
				value.addAll(parseExpression(arg.getArgumentValue()));
			}
		}
		// Note range expressions not checked here since returning a set of vars
		// wouldn't be helpful.
		// grouped expression, unsupported? variable reference base?
		return value;
	}

	default NewVariable parseSourceVariables(ExpressionBase base, NewVariable value) {
		if (NumericConstantExpression.class.isAssignableFrom(base.getClass())) {
			NumericConstantExpression expr = (NumericConstantExpression) base;
			value.addSourceVariables(expr.getValue());
		} else if (StringConstantExpression.class.isAssignableFrom(base.getClass())) {
			StringConstantExpression expr = (StringConstantExpression) base;
			value.addSourceVariables(expr.getValue());
			value.setBasisVariableName(expr.getValue());
		} else if (MissingValueConstantExpression.class.isAssignableFrom(base.getClass())) {
			MissingValueConstantExpression expr = (MissingValueConstantExpression) base;
			value.addSourceVariables(expr.getValue());
		} else if (VariableSymbolExpression.class.isAssignableFrom(base.getClass())) {
			VariableSymbolExpression expr = (VariableSymbolExpression) base;
			value.addSourceVariables(expr.getVariableName());
			value.setBasisVariableName(expr.getVariableName());

		} else if (VariableListExpression.class.isAssignableFrom(base.getClass())) {
			VariableListExpression expr = (VariableListExpression) base;
			for (ExpressionBase inner : expr.getVariables()) {
				parseSourceVariables(inner, value);
			}
		} else if (CompositeVariableName.class.isAssignableFrom(base.getClass())) {
			CompositeVariableName expr = (CompositeVariableName) base;
			// is this right?
			value.addSourceVariables(expr.getStub());
		} else if (FunctionCallExpression.class.isAssignableFrom(base.getClass())) {
			FunctionCallExpression expr = (FunctionCallExpression) base;
			for (FunctionArgument arg : expr.getArguments()) {
				parseSourceVariables(arg.getArgumentValue(), value);
			}
		} else if (VariableRangeExpression.class.isAssignableFrom(base.getClass())) {
			VariableRangeExpression expr = (VariableRangeExpression) base;
			Range range = new Range();
			if (expr.getFirst() != null) {
				range.setStart(expr.getFirst());
			} 
			if (expr.getLast() != null) {
				range.setEnd(expr.getLast());
			}
			value.setSourceVariableRange(range);
		}

		return value;
	}

	default String checkConstantExpressions(ExpressionBase expression) {
		String value = null;
		if (NumericConstantExpression.class.isAssignableFrom(expression.getClass())) {
			NumericConstantExpression nce = (NumericConstantExpression) expression;
			value = nce.getValue();
		} else if (StringConstantExpression.class.isAssignableFrom(expression.getClass())) {
			StringConstantExpression sce = (StringConstantExpression) expression;
			value = sce.getValue();
		} else if (NumericMaximumValueExpression.class.isAssignableFrom(expression.getClass())) {
			value = MinMax.MAXIMUM.getValue();
		} else if (NumericMinimumValueExpression.class.isAssignableFrom(expression.getClass())) {
			value = MinMax.MINIMUM.getValue();
		}
		return value;
	}

	/**
	 * Checks the type of the expression coming in and casts it to the
	 * appropriate one. Then adds its source variables to the NewVariable if any
	 * exist, and checks for nested expressions where appropriate. Any
	 * Expressions with constants will be added to the new variable as possible
	 * codes
	 * 
	 * @param expression
	 * @param newVariable
	 */
	default void checkForNestedExpressions(ExpressionBase expression, NewVariable newVariable) {
		if (VariableSymbolExpression.class.isAssignableFrom(expression.getClass())) {
			VariableSymbolExpression expr = (VariableSymbolExpression) expression;
			newVariable.addSourceVariables(expr.getVariableName());
			newVariable.setBasisVariableName(expr.getVariableName());
		} else if (VariableRangeExpression.class.isAssignableFrom(expression.getClass())) {
			VariableRangeExpression expr = (VariableRangeExpression) expression;
			newVariable.addSourceVariables(expr.getFirst(), expr.getLast());
			Range range = new Range();
			range.setStart(expr.getFirst());
			range.setEnd(expr.getLast());
			newVariable.setSourceVariableRange(range);
		} else if (VariableListExpression.class.isAssignableFrom(expression.getClass())) {
			VariableListExpression expr = (VariableListExpression) expression;
			// for every reference base, check if it is a range or a symbol, and
			// take the necessary steps to parse it and add it to the list.
			for (VariableReferenceBase base : expr.getVariables()) {
				// if (base.getRange() != null
				// && (base.getRange().getFirst() != null &&
				// base.getRange().getLast() != null)) {
				// VariableRangeExpression baseExpr = (VariableRangeExpression)
				// expression;
				// newVariable.addSourceVariables(baseExpr.getFirst(),
				// baseExpr.getLast());
				// Range range = new Range();
				// range.setStart(baseExpr.getFirst());
				// range.setEnd(baseExpr.getLast());
				// newVariable.setSourceVariableRange(range);
				// }
				// if (base.getVariable() != null &&
				// base.getVariable().getVariableName() != null) {
				// VariableSymbolExpression symbolExpr =
				// (VariableSymbolExpression) expression;
				// newVariable.addSourceVariables(symbolExpr.getVariableName());
				// newVariable.setBasisVariableName(symbolExpr.getVariableName());
				// }
			}
		} else if (FunctionCallExpression.class.isAssignableFrom(expression.getClass())) {
			FunctionCallExpression expr = (FunctionCallExpression) expression;
			for (FunctionArgument eb : expr.getArguments()) {
				checkForNestedExpressions(eb.getArgumentValue(), newVariable);
			}
		} else if (GroupedExpression.class.isAssignableFrom(expression.getClass())) {
			GroupedExpression expr = (GroupedExpression) expression;
			checkForNestedExpressions(expr.getExpression(), newVariable);
		} else if (NumericConstantExpression.class.isAssignableFrom(expression.getClass())) {
			NumericConstantExpression expr = (NumericConstantExpression) expression;
			newVariable.setDataType(expr.getNumericType());
			newVariable.addPossibleCodes(String.valueOf(expr.getValue()));
		} else if (MissingValueConstantExpression.class.isAssignableFrom(expression.getClass())) {
			MissingValueConstantExpression expr = (MissingValueConstantExpression) expression;
			newVariable.setDataType(expr.getTypeName());
			newVariable.addPossibleCodes(String.valueOf(expr.getValue()));
		} else if (StringConstantExpression.class.isAssignableFrom(expression.getClass())) {
			StringConstantExpression expr = (StringConstantExpression) expression;
			newVariable.setDataType(expr.getType());
			newVariable.addPossibleCodes(expr.getValue());
		}
		// } else if
		// (UnknownExpression.class.isAssignableFrom(expression.getClass())) {
		// UnknownExpression expr = (UnknownExpression) expression;
		// } else if
		// (UnsupportedExpression.class.isAssignableFrom(expression.getClass()))
		// {
		// UnsupportedExpression expr = (UnsupportedExpression) expression;
		// }
	}

	default Set<String> parseTransformBase(TransformBase base) {
		Set<String> vars = new HashSet<>();
		if (Compute.class.isAssignableFrom(base.getClass())) {
			Compute compute = (Compute) base;
			SdtlWrapper wrapper = WrapperFactory.wrap(compute);
			us.mtna.data.transform.wrapper.sdtl.Compute computeWrapper = (us.mtna.data.transform.wrapper.sdtl.Compute) wrapper;
			vars.addAll(computeWrapper.getVariables());
		}

		return vars;
	}

	default List<Range> getRangesFromTransformBase(TransformBase base) {
		List<Range> ranges = new ArrayList<>();
		if (Compute.class.isAssignableFrom(base.getClass())) {
			Compute compute = (Compute) base;
			SdtlWrapper wrapper = WrapperFactory.wrap(compute);
			us.mtna.data.transform.wrapper.sdtl.Compute computeWrapper = (us.mtna.data.transform.wrapper.sdtl.Compute) wrapper;
			ranges.addAll(computeWrapper.getRanges());
		}
		// TODO add support for other transforms
		return ranges;
	}

	default Set<String> getVariablesFromVariableReferenceBase(VariableReferenceBase base) {
		Set<String> vars = new HashSet<>();
		if (base != null) {
			if (VariableSymbolExpression.class.isAssignableFrom(base.getClass())) {
				VariableSymbolExpression se = (VariableSymbolExpression) base;
				if (se.getVariableName() != null) {
					String name = se.getVariableName();
					vars.add(name);
				}
			}
		}
		return vars;
	}

	// testing casting to more than just the variable symbol expression:
	default Set<String> getVariablesFromVariableReferenceBaseTEST(VariableReferenceBase base) {
		Set<String> vars = new HashSet<>();
		if (VariableSymbolExpression.class.isAssignableFrom(base.getClass())) {
			VariableSymbolExpression se = (VariableSymbolExpression) base;
			if (se.getVariableName() != null) {
				String name = se.getVariableName();
				vars.add(name);
			}
		} else if (VariableListExpression.class.isAssignableFrom(base.getClass())) {
			VariableListExpression se = (VariableListExpression) base;
			for (VariableReferenceBase inner : se.getVariables()) {
				vars.addAll(getVariablesFromVariableReferenceBase(inner));
			}
		} else if (AllVariablesExpression.class.isAssignableFrom(base.getClass())) {
			AllVariablesExpression se = (AllVariablesExpression) base;
			// add all variables in the dataset - may have to do this in the
			// Updater?
		}
		// could also be a composite variable name expression or a variable
		// range expression, but these two dont return sets of vars.
		return vars;
	}

	default Set<String> getVariablesFromVariableReferenceBaseArray(VariableReferenceBase... array) {
		Set<String> vars = new HashSet<>();
		if (array != null && array.length > 0) {
			for (VariableReferenceBase var : array) {
				vars.addAll(getVariablesFromVariableReferenceBase(var));
			}
		}
		return vars;
	}

	default List<Range> getRangesFromVariableReferenceBase(VariableReferenceBase base) {
		List<Range> ranges = new ArrayList<>();
		if (base != null) {
			if (VariableRangeExpression.class.isAssignableFrom(base.getClass())) {
				VariableRangeExpression se = (VariableRangeExpression) base;
				Range range = new Range();
				if (se.getFirst() != null) {
					range.setStart(se.getFirst());
				}
				if (se.getLast() != null) {
					range.setEnd(se.getLast());
				}
				ranges.add(range);
			}
		}
		return ranges;
	}

	default List<Range> getRangesFromVariableReferenceBaseArray(VariableReferenceBase[] array) {
		List<Range> ranges = new ArrayList<>();
		if (array != null) {
			for (VariableReferenceBase base : array) {
				ranges.addAll(getRangesFromVariableReferenceBase(base));
			}
		}
		return ranges;
	}

	// below is the check for nested expression in compute that may be more
	// thorough. compare it against the one above.
	/**
	 * Checks the type of the expression coming in and casts it to the
	 * appropriate one. Then adds its source variables to the NewVariable if any
	 * exist, and checks for nested expressions where appropriate. Any
	 * Expressions with constants will be added to the new variable as possible
	 * codes
	 * 
	 * @param expression
	 * @param newVariable
	 */
	// private void checkForNestedExpressions(ExpressionBase expression,
	// NewVariable newVariable) {
	// if
	// (VariableSymbolExpression.class.isAssignableFrom(expression.getClass()))
	// {
	// VariableSymbolExpression expr = (VariableSymbolExpression) expression;
	// newVariable.addSourceVariables(expr.getVariableName());
	// newVariable.setBasisVariableName(expr.getVariableName());
	// } else if
	// (VariableRangeExpression.class.isAssignableFrom(expression.getClass())) {
	// VariableRangeExpression expr = (VariableRangeExpression) expression;
	// newVariable.addSourceVariables(expr.getFirst(), expr.getLast());
	// Range range = new Range();
	// range.setStart(expr.getFirst());
	// range.setEnd(expr.getLast());
	// newVariable.setSourceVariableRange(range);
	// } else if
	// (VariableListExpression.class.isAssignableFrom(expression.getClass())) {
	// VariableListExpression expr = (VariableListExpression) expression;
	// // for every reference base, check if it is a range or a symbol, and
	// // take the necessary steps to parse it and add it to the list.
	// for (VariableReferenceBase base : expr.getVariables()) {
	// if (base.getRange() != null
	// && (base.getRange().getFirst() != null && base.getRange().getLast() !=
	// null)) {
	// VariableRangeExpression baseExpr = (VariableRangeExpression) expression;
	// newVariable.addSourceVariables(baseExpr.getFirst(), baseExpr.getLast());
	// Range range = new Range();
	// range.setStart(baseExpr.getFirst());
	// range.setEnd(baseExpr.getLast());
	// newVariable.setSourceVariableRange(range);
	// }
	// if (base.getVariable() != null && base.getVariable().getVariableName() !=
	// null) {
	// VariableSymbolExpression symbolExpr = (VariableSymbolExpression)
	// expression;
	// newVariable.addSourceVariables(symbolExpr.getVariableName());
	// newVariable.setBasisVariableName(symbolExpr.getVariableName());
	// }
	// }
	// } else if
	// (FunctionCallExpression.class.isAssignableFrom(expression.getClass())) {
	// FunctionCallExpression expr = (FunctionCallExpression) expression;
	// for (FunctionArgument eb : expr.getArguments()) {
	// // TODO idk if this is right.
	// checkForNestedExpressions(eb.getArgumentValue(), newVariable);
	// }
	// } else if
	// (GroupedExpression.class.isAssignableFrom(expression.getClass())) {
	// GroupedExpression expr = (GroupedExpression) expression;
	// checkForNestedExpressions(expr.getExpression(), newVariable);
	// } else if
	// (NumericConstantExpression.class.isAssignableFrom(expression.getClass()))
	// {
	// NumericConstantExpression expr = (NumericConstantExpression) expression;
	// newVariable.setDataType(expr.getNumericType());
	// newVariable.addPossibleCodes(String.valueOf(expr.getValue()));
	// } else if
	// (MissingValueConstantExpression.class.isAssignableFrom(expression.getClass()))
	// {
	// MissingValueConstantExpression expr = (MissingValueConstantExpression)
	// expression;
	// newVariable.setDataType(expr.getTypeName());
	// newVariable.addPossibleCodes(String.valueOf(expr.getValue()));
	// } else if
	// (StringConstantExpression.class.isAssignableFrom(expression.getClass()))
	// {
	// StringConstantExpression expr = (StringConstantExpression) expression;
	// newVariable.setDataType(expr.getType());
	// newVariable.addPossibleCodes(expr.getValue());
	// } else if
	// (UnknownExpression.class.isAssignableFrom(expression.getClass())) {
	// UnknownExpression expr = (UnknownExpression) expression;
	// } else if
	// (UnsupportedExpression.class.isAssignableFrom(expression.getClass())) {
	// UnsupportedExpression expr = (UnsupportedExpression) expression;
	// }
	// }
}

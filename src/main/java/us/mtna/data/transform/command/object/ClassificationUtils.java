/**
<Overview>
 
Copyright 2017 Metadata Technology North America Inc.
 
This software is the result of collaborative efforts from all participants of the C2Metadata project (http://www.c2metadata.org)
 
C2Metadata is supported by the Data Infrastructure Building Blocks (DIBBs) program of the National Science Foundation through grant NSF ACI-1640575.
 
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 
**/
package us.mtna.data.transform.command.object;

import java.util.ArrayList;
import java.util.List;

import us.mtna.pojo.Classification;
import us.mtna.pojo.Code;

/**
 * for holding utility methods involving classifications that are too specific
 * to go on the classification object itself.
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public class ClassificationUtils {

	public ClassificationUtils() {

	}

	/**
	 * for help in carrying over unused codes in updateclassification
	 * 
	 * @param classif
	 *            the classification in scope.
	 * @param usedCodeValues
	 *            codes that have been updated/deleted/added in the method so
	 *            far
	 * @return a list of codes that have not been used
	 */
	public static List<Code> getUntouchedCodes(Classification classif, List<String> usedCodeValues) {
		List<Code> unused = new ArrayList<>();
		List<Code> codes = classif.getCodeList();
		List<Code> usedCodes = classif.getCodesByValues(usedCodeValues);
		
		for (Code c : codes) {
			if(!usedCodes.contains(c)){
				unused.add(c);
			}
		}
		return unused;
	}

}

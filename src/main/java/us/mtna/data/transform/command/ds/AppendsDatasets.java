package us.mtna.data.transform.command.ds;

public interface AppendsDatasets extends Transform{
	/**
	 * Names of files to be appended
	 * @return 
	 */
	String[] getFileNames();

	String getAppendFlagVariable();

}

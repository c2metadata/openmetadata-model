/**
<Overview>
 
Copyright 2017 Metadata Technology North America Inc.
 
This software is the result of collaborative efforts from all participants of the C2Metadata project (http://www.c2metadata.org)
 
C2Metadata is supported by the Data Infrastructure Building Blocks (DIBBs) program of the National Science Foundation through grant NSF ACI-1640575.
 
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 
**/
package us.mtna.data.transform.command;

import us.mtna.data.transform.command.ds.Transform;
import us.mtna.data.transform.command.object.NewVariable;

public interface CreatesVariables extends Transform {

	NewVariable[] getNewVariables();

	/**
	 * This method denotes whether a copy of the original variable should remain
	 * in the dataset, if a new variable is created from an original variable
	 * (like in compute, recode), when we want to have an intermediate/"orphan"
	 * variable remaining to show the provenance.
	 * 
	 * This property is not determined by the sdtl, it must be set manually in
	 * each implementing class.
	 * 
	 * @return true if the original variable should be preserved in the dataset
	 *         before making changes
	 */
	boolean preserveOriginalVariable();
}

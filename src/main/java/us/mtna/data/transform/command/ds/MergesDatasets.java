package us.mtna.data.transform.command.ds;

import java.util.List;
import java.util.Set;

import us.mtna.data.transform.command.SdtlWrapper;
import us.mtna.data.transform.command.object.UpdaterMergeFileDescription;

public interface MergesDatasets extends SdtlWrapper{

	Set<String> getMergeByVariables();

	Set<String> getKeepVariables();

	List<UpdaterMergeFileDescription> getMergeFileDscr();
	
	String getFirstVariable();
	
	String getLastVariable();

}

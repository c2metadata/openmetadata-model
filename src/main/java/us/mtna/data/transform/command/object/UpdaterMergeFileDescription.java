package us.mtna.data.transform.command.object;

import java.util.ArrayList;
import java.util.List;

import org.c2metadata.sdtl.pojo.expression.VariableReferenceBase;

/**
 * All the properties of a merge file description updated for use by the dataset
 * updater in the join datasets interface.
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public class UpdaterMergeFileDescription {
	/**
	 * "Name of the file being merged. May be ""Active file" 1..1
	 */
	private String fileName;
	/**
	 * Describes the type of merge performed. Enumerations: Sequential,
	 * OneToOne, ManyToOne, OneToMany, ManyToMany, OneToOne, Duplicates,
	 * Unmatched 1..1
	 */
	private MergeType mergeType;
	/**
	 * Creates a new variable indicating whether the row came from this file or
	 * a different input file 0..1
	 */
	private String mergeFlagVariable;
	/**
	 * Variables to be renamed, 0..n
	 */
	private List<VariableNamePair> renameVariables;
	/**
	 * Outcome when the same variables exist in more than one file. ""Master""
	 * means the value is taken from this file. ""Merge"" means the value is
	 * taken from the other file. ""Update"" means that value is taken from the
	 * other file when the value in this file is missing. Enumerations: Master,
	 * Merge, Update. 0..1
	 * 
	 */
	private UpdateType update;
	/**
	 * Generate new row when not matched to other files 0..1
	 */
	private boolean newRow;

	private VariableReferenceBase[] keepVariables;
	private VariableReferenceBase[] dropVariables;

	public enum MergeType {
		SEQUENTIAL, ONE_TO_ONE, MANY_TO_ONE, ONE_TO_MANY, CARTESIAN, UNMATCHED, SAS_MATCH_MERGE
	}

	public enum UpdateType {
		MASTER, IGNORE, FILL_NEW, UPDATE_MISSING, REPLACE
	}

	public UpdaterMergeFileDescription() {
		this.renameVariables = new ArrayList<>();
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public MergeType getMergeType() {
		return mergeType;
	}

	public void setMergeType(MergeType mergeType) {
		this.mergeType = mergeType;
	}

	public String getMergeFlagVariable() {
		return mergeFlagVariable;
	}

	public void setMergeFlagVariable(String mergeFlagVariable) {
		this.mergeFlagVariable = mergeFlagVariable;
	}

	public UpdateType getUpdate() {
		return update;
	}

	public void setUpdate(UpdateType update) {
		this.update = update;
	}

	public boolean isNewRow() {
		return newRow;
	}

	public void setNewRow(boolean newRow) {
		this.newRow = newRow;
	}

	public List<VariableNamePair> getRenameVariables() {
		return renameVariables;
	}

	public void setRenameVariables(List<VariableNamePair> renameVariables) {
		this.renameVariables = renameVariables;
	}

	public VariableReferenceBase[] getKeepVariables() {
		return keepVariables;
	}

	public void setKeepVariables(VariableReferenceBase[] keepVariables) {
		this.keepVariables = keepVariables;
	}

	public VariableReferenceBase[] getDropVariables() {
		return dropVariables;
	}

	public void setDropVariables(VariableReferenceBase[] dropVariables) {
		this.dropVariables = dropVariables;
	}

}

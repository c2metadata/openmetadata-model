package us.mtna.data.transform.command.ds;

import org.c2metadata.sdtl.pojo.command.TransformBase;

import us.mtna.data.transform.command.SdtlWrapper;

public interface Transform extends SdtlWrapper {
	@Override
	TransformBase getOriginalCommand();
}

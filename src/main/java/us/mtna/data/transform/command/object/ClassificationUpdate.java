/**
<Overview>
 
Copyright 2017 Metadata Technology North America Inc.
 
This software is the result of collaborative efforts from all participants of the C2Metadata project (http://www.c2metadata.org)
 
C2Metadata is supported by the Data Infrastructure Building Blocks (DIBBs) program of the National Science Foundation through grant NSF ACI-1640575.
 
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 
**/
package us.mtna.data.transform.command.object;

public class ClassificationUpdate {

	private CodeDetail[] newCodes;
	private CodeRangeDetail[] newCodeRanges;
	private CodeDetail[] updatesCodes;
	private CodeRangeDetail[] updatesCodeRange;
	private String[] removedCodes;
	private Range[] removedCodeRanges;

	public String[] getRemovedCodes() {
		return removedCodes;
	}

	public CodeDetail[] getNewCodes() {
		return newCodes;
	}

	public CodeDetail[] getUpdatesCodes() {
		return updatesCodes;
	}

	public Range[] getRemovedCodeRanges() {
		return removedCodeRanges;
	}

	public void setRemovedCodes(String[] removedCodes) {
		this.removedCodes = removedCodes;
	}

	public void setNewCodes(CodeDetail[] newCodes) {
		this.newCodes = newCodes;
	}

	public void setUpdatesCodes(CodeDetail[] updatesCodes) {
		this.updatesCodes = updatesCodes;
	}

	public void setRemovedCodeRanges(Range[] removedCodeRanges) {
		this.removedCodeRanges = removedCodeRanges;
	}

	public CodeRangeDetail[] getUpdatesCodeRange() {
		return updatesCodeRange;
	}

	public void setUpdatesCodeRange(CodeRangeDetail[] updatesCodeRange) {
		this.updatesCodeRange = updatesCodeRange;
	}

	public CodeRangeDetail[] getNewCodeRanges() {
		return newCodeRanges;
	}

	public void setNewCodeRanges(CodeRangeDetail[] newCodeRanges) {
		this.newCodeRanges = newCodeRanges;
	}
	
	
}

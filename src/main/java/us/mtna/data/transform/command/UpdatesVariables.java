/**
<Overview>
 
Copyright 2017 Metadata Technology North America Inc.
 
This software is the result of collaborative efforts from all participants of the C2Metadata project (http://www.c2metadata.org)
 
C2Metadata is supported by the Data Infrastructure Building Blocks (DIBBs) program of the National Science Foundation through grant NSF ACI-1640575.
 
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 
**/
package us.mtna.data.transform.command;

import us.mtna.data.transform.command.object.VariableNamePair;
import us.mtna.pojo.DataType;

/**
 * Any commands that updates or creates* variable(s) (Rename, Recode,
 * SetVariableLabel, etc.).
 * 
 * *Note that at the command level, without the dataset available, it is
 * impossible to tell if a variable already exists to be updated. If a variable
 * listed here does not exist, a variable of that name may be created.
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public interface UpdatesVariables {

	/**
	 * Gets pairs of variables as the source and target values. If no variable
	 * name pairs are found, move on to checking for other variable property
	 * changes (data type)
	 * 
	 * @return
	 */
	VariableNamePair[] getUpdatedVariables();

	/**
	 * Get the data type that the selected variables should be changed to. If
	 * null, that means no data type change needs to occur.
	 * 
	 * @return
	 */
	DataType getDataType();
}

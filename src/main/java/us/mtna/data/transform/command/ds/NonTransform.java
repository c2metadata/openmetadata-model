package us.mtna.data.transform.command.ds;

import org.c2metadata.sdtl.pojo.command.InformBase;

import us.mtna.data.transform.command.SdtlWrapper;

public interface NonTransform extends SdtlWrapper {
	@Override
	InformBase getOriginalCommand();

}

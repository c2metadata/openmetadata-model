/**
<Overview>
 
Copyright 2017 Metadata Technology North America Inc.
 
This software is the result of collaborative efforts from all participants of the C2Metadata project (http://www.c2metadata.org)
 
C2Metadata is supported by the Data Infrastructure Building Blocks (DIBBs) program of the National Science Foundation through grant NSF ACI-1640575.
 
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 
**/
package us.mtna.data.transform.command.object;

import java.util.ArrayList;
import java.util.List;

/**
 * Object to hold information regarding creation of new variables.
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public class NewVariable {
	/**
	 * name of the new variable that is to be created.
	 */
	private String newVariableName;
	/**
	 * The name of the source variable the new variable(s) will be created from.
	 * May be null if you're creating a new blank variable
	 */
	private String basisVariableName;

	private List<String> sourceVariables;
	
	private Range sourceVarRange;

	private String dataType;

	private List<String> possibleCodes;

	public NewVariable() {
		this.sourceVariables = new ArrayList<>();
		this.possibleCodes = new ArrayList<>();
	}

	public String getNewVariableName() {
		return newVariableName;
	}

	public void setNewVariableName(String newVariableName) {
		this.newVariableName = newVariableName;
	}

	public String getBasisVariableName() {
		return basisVariableName;
	}

	public void setBasisVariableName(String basisVariableName) {
		this.basisVariableName = basisVariableName;
	}

	public void addSourceVariables(String... vars) {
		for (String s : vars) {
			this.sourceVariables.add(s);
		}
	}

	public List<String> getSourceVariables() {
		return sourceVariables;
	}

	public void setSourceVariables(List<String> sourceVariables) {
		this.sourceVariables = sourceVariables;
	}

	public String getDataType() {
		return dataType;
	}

	public void setDataType(String dataType) {
		this.dataType = dataType;
	}

	public List<String> getPossibleCodes() {
		return possibleCodes;
	}

	public void setPossibleCodes(List<String> possibleCodes) {
		this.possibleCodes = possibleCodes;
	}

	public void addPossibleCodes(String... codes) {
		for (String code : codes) {
			this.possibleCodes.add(code);
		}
	}
	
	public Range getSourceVariableRange() {
		return sourceVarRange;
	}
	
	public void setSourceVariableRange(Range varRange) {
		this.sourceVarRange = varRange;
	}

}

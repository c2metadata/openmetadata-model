/**
<Overview>
 
Copyright 2017 Metadata Technology North America Inc.
 
This software is the result of collaborative efforts from all participants of the C2Metadata project (http://www.c2metadata.org)
 
C2Metadata is supported by the Data Infrastructure Building Blocks (DIBBs) program of the National Science Foundation through grant NSF ACI-1640575.
 
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 
**/
package us.mtna.data.transform.command;

import java.util.List;
import java.util.Set;

import us.mtna.data.transform.command.ds.Transform;
import us.mtna.data.transform.command.object.Range;

/**
 * This interface should be implemented on any command that applies changes to
 * variables. All variables are referenced by name. It can hold either a set of
 * variables, or a list of ranges of variables, to be expanded in the
 * Dataset Updater once the dataset in scope is present.
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public interface SelectsVariables extends Transform {

	/**
	 * Gets the first and last variable names in a range of variables to be
	 * parsed in the updater. Wrappers should have either the range or list of
	 * variables filled out
	 * 
	 * @return a list of ranges of variable names to be operated on
	 */
	List<Range> getRanges();

	/**
	 * Gets a set of variable names. If this is null, first/last should be
	 * filled out.
	 * 
	 * @return a set of variables that will have commands applied to them.
	 */
	Set<String> getVariables();

	/**
	 * Checking that either a list or range of variables is present. If both are
	 * empty, there are no variables to select/operate on and the SDTL is most
	 * likely invalid. Implementations with other necessary properties should
	 * override, call this method, and check that additional necessary
	 * properties are present.
	 * 
	 * @return isValid boolean: true if valid (either var list or range (or both) is
	 *         present), false if invalid (both list and range are empty).
	 */
//	default ValidationResult validation(){
//		ValidationResult validation = new ValidationResult();
//		if(getVariables().isEmpty() && getRanges().isEmpty()){
//			validation.addMessage("No variables found on command [" + getOriginalCommand().getCommand() + "]");
//			validation.setValid(false);
//		}
//		return validation;
//	}
	
}

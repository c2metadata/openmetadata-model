package us.mtna.data.transform.command.object;

public enum MinMax {
	
	MINIMUM("Minimum"), MAXIMUM("Maximum");

	private final String value;

	private MinMax(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

}

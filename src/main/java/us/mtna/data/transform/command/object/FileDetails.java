/**
<Overview>
 
Copyright 2017 Metadata Technology North America Inc.
 
This software is the result of collaborative efforts from all participants of the C2Metadata project (http://www.c2metadata.org)
 
C2Metadata is supported by the Data Infrastructure Building Blocks (DIBBs) program of the National Science Foundation through grant NSF ACI-1640575.
 
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 
**/
package us.mtna.data.transform.command.object;

import java.time.ZonedDateTime;
import java.util.UUID;

/**
 * FileDetails holds file and document-level information. Some of this info
 * (filePath, fileName) can only be inferred if we have access to the upload
 * process, so they may be blank if not.
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public class FileDetails {
	/**
	 * title of the file - this is found in the metadata
	 */
	private String documentTitle;
	/**
	 * id of the file - this is found in the metadata
	 */
	private String fileId;
	/**
	 * path of the uploaded file, if available.
	 */
	private String filePath;
	/**
	 * name of the file that was uploaded (without extension)
	 */
	private String fileName;
	/**
	 * extension of the file that was uploaded (most likely "xml")
	 */
	private String extension;
	/**
	 * a generated uuid
	 */
	private String uri;
	/**
	 * time the file was uploaded (or first encountered in the reader if not
	 * uploaded)
	 */
	private ZonedDateTime uploadTime;

	public FileDetails() {
		this.uri = UUID.randomUUID().toString();
	}

	public String getDocumentTitle() {
		return documentTitle;
	}

	public void setDocumentTitle(String documentTitle) {
		this.documentTitle = documentTitle;
	}

	public String getFileId() {
		return fileId;
	}

	public void setFileId(String fileId) {
		this.fileId = fileId;
	}

	public ZonedDateTime getUploadTime() {
		return uploadTime;
	}

	public void setUploadTime(ZonedDateTime string) {
		this.uploadTime = string;
	}

	public FileDetails(String fileName) {
		this.fileName = fileName;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getExtension() {
		return extension;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	@Override
	public String toString() {
		return "File Path: [" + getFilePath() + "] " + "File Name: [" + getFileName() + "] " + "Extension: ["
				+ getExtension() + "] " + "URI: [" + getUri() + "]";
	}
}

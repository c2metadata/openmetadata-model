/**
<Overview>
 
Copyright 2017 Metadata Technology North America Inc.
 
This software is the result of collaborative efforts from all participants of the C2Metadata project (http://www.c2metadata.org)
 
C2Metadata is supported by the Data Infrastructure Building Blocks (DIBBs) program of the National Science Foundation through grant NSF ACI-1640575.
 
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 
**/
package us.mtna.data.transform.command;

import java.util.List;
import java.util.Set;

import us.mtna.data.transform.command.ds.Transform;
import us.mtna.data.transform.command.object.Range;

/**
 * DeletesVariable takes into account both straightforward deletes, and also
 * keeps. Keep commands delete all variables that are not listed in the list to
 * keep.
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public interface DeletesVariable extends Transform {

	Set<String> getDeletedVars();

	List<Range> getDeletedVariableRanges();

	Set<String> getKeepVars();

	List<Range> getKeepVariableRanges();
}

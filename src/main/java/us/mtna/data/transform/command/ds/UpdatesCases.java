package us.mtna.data.transform.command.ds;

import org.c2metadata.sdtl.pojo.expression.ExpressionBase;

public interface UpdatesCases extends Transform{

	ExpressionBase getCondition();
}

/**
<Overview>
 
Copyright 2017 Metadata Technology North America Inc.
 
This software is the result of collaborative efforts from all participants of the C2Metadata project (http://www.c2metadata.org)
 
C2Metadata is supported by the Data Infrastructure Building Blocks (DIBBs) program of the National Science Foundation through grant NSF ACI-1640575.
 
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 
**/
package us.mtna.data.transform.command;

import us.mtna.data.transform.command.object.ClassificationUpdate;

/**
 * To be implemented by any command that updates or creates* a classification
 * (Recode, SetMissingValues, SetValueLabels, etc.). The ClassificationUpdate
 * object holds the information needed to perform the commands. The other
 * booleans included in this interface are optional configurations and not
 * necessary to run the ds updater.
 * 
 * *At the command level, it is impossible to tell if a classification exists.
 * If the classification does not exist, one will be created with the
 * information provided.
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public interface UpdatesClassification extends SdtlWrapper {
	/**
	 * @return {@link ClassificationUpdate} object holding details of the
	 *         updated classification
	 */
	ClassificationUpdate getUpdate();

	/**
	 * Determines if a new Classification should be created so as to not affect
	 * the original one (recode), or if the original classification should be
	 * modified (as in a SetValueLabels)
	 * 
	 * @return {@code true} if a new Classification should be created,
	 *         {@code false} otherwise.
	 */
	boolean requiresCopyOfClassification();

	/**
	 * The recode command may optionally specify what to do with floating codes
	 * (that are not being recoded). In SPSS, for example, this uses the 'else'
	 * option: <code>recode v2 (1=2)(else=copy) into rec_v2.1</code>
	 * 
	 * @return {@code true} if floating codes should be carried over to the new
	 *         classification, {@code false} otherwise.
	 */
	boolean copyFloatingCodes();
}

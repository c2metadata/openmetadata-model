package us.mtna.transform.json;

import com.fasterxml.jackson.databind.module.SimpleModule;

import us.mtna.pojo.Classification;
import us.mtna.pojo.Code;
import us.mtna.pojo.ResourceImpl;
import us.mtna.pojo.SummaryStatistics;

public class DatasetModule extends SimpleModule {

	private static final long serialVersionUID = 1L;

	public DatasetModule() {
		super();
	
	}
	
	 @Override
	  public void setupModule(SetupContext context)
	  {
	    context.setMixInAnnotations(Classification.class, ClassificationMixin.class);
	    context.setMixInAnnotations(ResourceImpl.class, ResourceImplMixin.class);
	    context.setMixInAnnotations(Code.class, CodeMixin.class);
	    context.setMixInAnnotations(SummaryStatistics.class, SummaryStatsMixin.class);

	    
	  }
}

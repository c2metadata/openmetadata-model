/**
<Overview>
 
Copyright 2017 Metadata Technology North America Inc.
 
This software is the result of collaborative efforts from all participants of the C2Metadata project (http://www.c2metadata.org)
 
C2Metadata is supported by the Data Infrastructure Building Blocks (DIBBs) program of the National Science Foundation through grant NSF ACI-1640575.
 
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 
**/
package us.mtna.transform.json;

import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnore;

import us.mtna.pojo.Attribute;

public abstract class SummaryStatsMixin {

	@JsonIgnore
	public abstract Attribute<Set> getWeightedVariableNamesAttribute();

	@JsonIgnore
	public abstract Attribute<Double> getMeanAttribute();

	@JsonIgnore
	public abstract Attribute<Double> getMedianAttribute();

	@JsonIgnore
	public abstract Attribute<Double> getModeAttribute();

	@JsonIgnore
	public abstract Attribute<Double> getMinAttribute();

	@JsonIgnore
	public abstract Attribute<Double> getMaxAttribute();

	@JsonIgnore
	public abstract Attribute<Double> getStdDevAttribute();

	@JsonIgnore
	public abstract Attribute<Double> getOtherAttribute();

	@JsonIgnore
	public abstract Attribute<Double> getValidAttribute();

	@JsonIgnore
	public abstract Attribute<Double> getInvalidAttribute();

	@JsonIgnore
	public abstract Attribute<Double> getWeightedMeanAttribute();

	@JsonIgnore
	public abstract Attribute<Double> getWeightedMedianAttribute();

	@JsonIgnore
	public abstract Attribute<Double> getWeightedModeAttribute();

	@JsonIgnore
	public abstract Attribute<Double> getWeightedMinAttribute();

	@JsonIgnore
	public abstract Attribute<Double> getWeightedMaxAttribute();

	@JsonIgnore
	public abstract Attribute<Double> getWeightedStdDevAttribute();

	@JsonIgnore
	public abstract Attribute<Double> getWeightedOtherAttribute();

	@JsonIgnore
	public abstract Attribute<Double> getWeightedValidAttribute();

	@JsonIgnore
	public abstract Attribute<Double> getWeightedInvalidAttribute();
}

/**
<Overview>
 
Copyright 2017 Metadata Technology North America Inc.
 
This software is the result of collaborative efforts from all participants of the C2Metadata project (http://www.c2metadata.org)
 
C2Metadata is supported by the Data Infrastructure Building Blocks (DIBBs) program of the National Science Foundation through grant NSF ACI-1640575.
 
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 
**/
package us.mtna.transform.cogs.json;

import org.c2metadata.sdtl.pojo.command.CommandBase;
import org.c2metadata.sdtl.pojo.command.TransformBase;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import us.mtna.pojo.Transform;
import us.mtna.pojo.TransformCommand;

/**
 * Maps a {@link TransformBase} POJO to a {@link Transform} /
 * {@link TransformCommand} POJO to be used on a variable
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public class TransformMapper {

	public static final String SDTL_POJO = "sdtl-pojo";
	public static final String SDTL_PSEUDOCODE = "sdtl-pseudocode";
	private static PseudocodeService pseudocodeService;
	private static ObjectMapper mapper;
	private static org.slf4j.Logger log = LoggerFactory.getLogger(TransformMapper.class);

	// TODO look into reusing object mapper across all components

	static {
		mapper = new ObjectMapper();
		mapper.findAndRegisterModules();
		mapper.setSerializationInclusion(Include.NON_NULL);
		mapper.setSerializationInclusion(Include.NON_EMPTY);
		mapper.enable(SerializationFeature.INDENT_OUTPUT);
	}

	private static TransformCommand baseToCommand(CommandBase base, String syntax) {
		TransformCommand command = new TransformCommand();
		if (base.getSourceInformation() != null) {
			command.setCommand(base.getSourceInformation().getOriginalSourceText());
		} else if (base.getCommand() != null) {
			command.setCommand(base.getCommand());
		}

		if (syntax != null) {
			command.setSyntax(syntax);
		}

		return command;
	}

	/**
	 * convert relevant info from transform base to command - description and
	 * alt commands not transferred until added to json. source ids are not
	 * currently in the json and will only be added as we parse the commands.
	 * may be added to transform base later though.
	 * 
	 * @param base
	 * @return transform with relevant properties
	 */
	public static Transform mapTransformBase(CommandBase base, String syntax) {
		Transform transform = new Transform();
		transform.setOriginalCommand(baseToCommand(base, syntax));
		try {
			transform.addAlternativeCommands(new TransformCommand(SDTL_POJO, mapper.writeValueAsString(base)));
		} catch (JsonProcessingException e) {
			log.warn("Error transforming TransformBase to JSON", e);
		}
		if (pseudocodeService != null) {
			try {
				transform.addAlternativeCommands(new TransformCommand(SDTL_PSEUDOCODE,
						pseudocodeService.generate(mapper.writeValueAsString(base))));

			} catch (JsonProcessingException | RuntimeException e) {
				log.warn("Error transforming TransformBase to SDTL pseudocode", e);
			}
		} else {
			log.warn("PseudocodeService is null when trying to add pseudoce to transform.");
		}
		return transform;
	}

	// pass in the pseudocode command
	public static Transform mapTransformBase(CommandBase base, String syntax, String pseudocode) {
		Transform transform = new Transform();
		transform.setOriginalCommand(baseToCommand(base, syntax));
		try {
			transform.addAlternativeCommands(new TransformCommand(SDTL_POJO, mapper.writeValueAsString(base)));
		} catch (JsonProcessingException e) {
			log.warn("Error transforming TransformBase to JSON", e);
		}
		try {
			transform.addAlternativeCommands(new TransformCommand(SDTL_PSEUDOCODE, pseudocode));

		} catch (RuntimeException e) {
			log.warn("Error transforming TransformBase to SDTL pseudocode", e);
		}

		return transform;
	}

	/**
	 * Used to set the pseudocode service for generating pseudocode from the
	 * SDTL.
	 * 
	 * @param service
	 *            PseudocodeService implementation
	 */
	public static void setPseudocodeService(PseudocodeService service) {
		pseudocodeService = service;
	}
}

package us.mtna.transform.cogs.json;

/**
 * Generates pseudocode from an sdtl command
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public interface PseudocodeService {
	/**
	 * Generate pseudocode from a SDTL json string
	 * 
	 * @param sdtlCommand
	 *            json string
	 * @return pseudocode
	 */
	String generate(String sdtlCommand);
	
	/**
	 * Generate pseudocode from a SDTL TransformBase pojo
	 * 
	 * @param command
	 *            TransformBase pojo
	 * @return pseudocode
	 */
	//String generate(TransformBase command);
}

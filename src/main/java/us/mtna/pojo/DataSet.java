/**
<Overview>
 
Copyright 2017 Metadata Technology North America Inc.
 
This software is the result of collaborative efforts from all participants of the C2Metadata project (http://www.c2metadata.org)
 
C2Metadata is supported by the Data Infrastructure Building Blocks (DIBBs) program of the National Science Foundation through grant NSF ACI-1640575.
 
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 
**/
package us.mtna.pojo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import us.mtna.data.transform.command.object.FileDetails;
import us.mtna.updater.Citation;

/**
 * A data set is an organized collection of data. Here, dataset is made up of a
 * list of {@link #records} and adds {@link #metadata} as an attribute to
 * {@link #getAttributes()}.
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public class DataSet extends ResourceImpl {

	private Metadata metadata;
	private ArrayList<FileTransform> transforms;
	private FileDetails fileDetails;
	private int recordCount;
	private String scriptName;
	// author info- should this be a list?
	// should it be inside fileDetails?
	private Citation<?> citation;
	//should this be at the resource level? 
	private String datasetId;

	public DataSet() {
		transforms = new ArrayList<>();
		metadata = new Metadata();
		fileDetails = new FileDetails();
	}

	public FileDetails getFile() {
		return fileDetails;
	}

	public void setFileDetails(FileDetails file) {
		this.fileDetails = file;
	}

	public Metadata getMetadata() {
		return metadata;
	}

	public void setMetadata(Metadata metadata) {
		this.metadata = metadata;
	}

	public String getDatasetId() {
		return datasetId;
	}

	public void setDatasetId(String key) {
		this.datasetId = key;
	}

	public int getRecordCount() {
		return recordCount;
	}

	public void setRecordCount(int recordCount) {
		this.recordCount = recordCount;
	}

	public ArrayList<FileTransform> getCommands() {
		return transforms;
	}

	public void setCommands(ArrayList<FileTransform> commands) {
		this.transforms.clear();
		this.transforms.addAll(commands);
	}

	public void addCommands(FileTransform... commands) {
		for (FileTransform transform : commands) {
			this.transforms.add(transform);
		}
	}

	public Citation<?> getCitation() {
		return citation;
	}

	public void setCitation(Citation<?> citation) {
		this.citation = citation;
	}

	/**
	 * Get the name that the dataset is referred to as in the associated script
	 * 
	 * @return
	 */
	public String getScriptName() {
		return scriptName;
	}

	/**
	 * Set the name that the dataset is referred to as in the associated script
	 * 
	 */
	public void setScriptName(String scriptName) {
		this.scriptName = scriptName;
	}

	/**
	 * Adds metadata to the list of attributes and returns the list of
	 * attributes
	 * 
	 * @return list of attributes
	 */
	public List<Attribute<?>> getAttributes() {
		List<Attribute<?>> attributes = super.getAttributes();
		Attribute<Metadata> metadataAttribute = new Attribute<>(Metadata.class);
		if (metadata != null) {
			metadataAttribute.setName("Metadata");
			metadataAttribute.setValue(metadata);
			attributes.add(metadataAttribute);
		}
		return attributes;
	}

	/**
	 * Convenience method for getting variables in a specified range (inclusive
	 * by default) using either a start position if present, or their natural
	 * order in the ds.
	 * 
	 * @param from
	 *            - start value of range (inclusive)
	 * @param to
	 *            - value ending the range (inclusive)
	 * @return ordered list of variables in the range.
	 */
	public List<Variable> getVariablesInRange(String from, String to) {
		Map<String, Variable> originalMap = metadata.getVarNameMap();
		List<Variable> vars = new ArrayList<>();

		Map<String, Variable> varMap = new HashMap<>();
		for (Entry<String, Variable> entry : originalMap.entrySet()) {
			varMap.put(entry.getKey().toUpperCase(), entry.getValue());
		}

		// find out if I can sort on start position or if I have to do it
		// by natural order and width. if even one doesn't have a start position
		// you can't use this
		boolean useStartPosition = true;
		for (Variable variable : varMap.values()) {
			if (variable.getStartPosition() == null) {
				useStartPosition = false;
				break;
			}
		}

		// sort if start position. if useStartPosition = false, they will
		// already be sorted by natural order
		List<Variable> sortedVars = new ArrayList<>(metadata.getVariables());
		if (useStartPosition) {
			Collections.sort(sortedVars, Variable.StartPositionComparator);
		}

		Variable mapFirst = varMap.get(from.toUpperCase());
		Variable mapLast = varMap.get(to.toUpperCase());
		int toIndex = sortedVars.indexOf(mapLast);
		int fromIndex = sortedVars.indexOf(mapFirst);

		// use indices to get the vars in the range (+1 on end index since it is
		// exclusive)
		List<Variable> subList = sortedVars.subList(fromIndex, toIndex + 1);

		for (Variable variable : subList) {
			vars.add(variable);
		}
		return vars;
	}

}

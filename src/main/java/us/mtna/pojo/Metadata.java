/**
<Overview>
 
Copyright 2017 Metadata Technology North America Inc.
 
This software is the result of collaborative efforts from all participants of the C2Metadata project (http://www.c2metadata.org)
 
C2Metadata is supported by the Data Infrastructure Building Blocks (DIBBs) program of the National Science Foundation through grant NSF ACI-1640575.
 
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 
**/
package us.mtna.pojo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Metadata holds a list of {@link #classifications} & {@link #variables}.
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public class Metadata {

	private Map<String, Classification> classifs;
	private List<Variable> variables;
	/**
	 * these final/intermediate/"orphan" variables are variables that are being
	 * preserved in their original form during the updater process so that when
	 * changes are made to them, there is a way to trace back the provenance.
	 * They will be added back into the dataset at the end of the update
	 * process.
	 **/
	private List<Variable> finalVariables;

	/**
	 * when creating a test {@link DataSet.java}, add list of variables and
	 * classifications to metadata object, then add metadata to dataSet. These
	 * variables should have their source ids pointing to the original file
	 * dscr, while the variables derived from these would reference these.
	 */
	public Metadata() {
		variables = new ArrayList<>();
		classifs = new LinkedHashMap<>();
		finalVariables = new ArrayList<>();
	}

	public Map<String, Classification> getClassifs() {
		return classifs;
	}

	public void setClassifs(Map<String, Classification> classifs) {
		this.classifs.clear();
		this.classifs.putAll(classifs);
	}

	public List<Variable> getVariables() {
		return variables;
	}

	public void setVariables(List<Variable> variables) {
		this.variables = variables;
	}

	/**
	 * add one or more variables to the dataset metadata's list of variables.
	 * 
	 * @param vars
	 */
	public void addVariables(Variable... vars) {
		for (Variable v : vars) {
			this.variables.add(v);

		}
	}

	public Classification lookupClassificationById(String id) {
		return classifs.get(id);
	}

	/**
	 * Populate a map with classification's id and classification object
	 * 
	 * @param classificationAttributes
	 * @return a map of classifications mapped to their ids
	 */
	public Map<String, Classification> populateClassificationMap(List<Classification> classificationAttributes) {
		Map<String, Classification> classificationAttributeMap = new HashMap<>();
		for (Classification classification : classificationAttributes) {
			classificationAttributeMap.put(classification.getId(), classification);
		}
		return classificationAttributeMap;
	}

	/**
	 * populate a map with variable names and objects. Every time you get this,
	 * it will use the latest list of variables, so no need to reindex as long
	 * as you are not holding this map as a local variable
	 * 
	 * @return a map of variables mapped to their names.
	 */
	public Map<String, Variable> getVarNameMap() {
		Map<String, Variable> variableNameMap = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);
		for (Variable v : variables) {
			if (v.getName() == null) {
				// log.error("Cannot add variable to varNameMap because name is
				// null");
			}
			variableNameMap.put(v.getName(), v);
		}
		return variableNameMap;
	}

	public List<Variable> getFinalVariables() {
		return finalVariables;
	}

	public void setFinalVariables(List<Variable> finalVariables) {
		this.finalVariables = finalVariables;
	}
}

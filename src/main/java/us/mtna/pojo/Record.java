/**
<Overview>
 
Copyright 2017 Metadata Technology North America Inc.
 
This software is the result of collaborative efforts from all participants of the C2Metadata project (http://www.c2metadata.org)
 
C2Metadata is supported by the Data Infrastructure Building Blocks (DIBBs) program of the National Science Foundation through grant NSF ACI-1640575.
 
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 
**/
package us.mtna.pojo;

import java.util.ArrayList;
import java.util.List;

/**
 * A record contains all of the data points about a specific unit of interest.
 * A record object contains a list of {@link Datum}
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */

public class Record {

	private List<Datum> datumList;
	private String name;
	private Integer id;
	private DataType dataType;
	
	/**
	 * populate a record with datum. 
	 */
	public Record(){
		datumList = new ArrayList<>();
	}


	public List<Datum> getDatumList() {
		return datumList;
	}
	
	public void setDatumList(List<Datum> datum){
		this.datumList.clear();
		this.datumList.addAll(datum);
	}
	
	public void addDatumToList(Datum...datum){
		for(Datum d : datumList){
			datumList.add(d);
		}
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public DataType getDataType() {
		return dataType;
	}

	public void setDataType(DataType dataType) {
		this.dataType = dataType;
	}
	
}

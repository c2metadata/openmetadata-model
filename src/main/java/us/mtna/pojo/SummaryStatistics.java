/**
<Overview>
 
Copyright 2017 Metadata Technology North America Inc.
 
This software is the result of collaborative efforts from all participants of the C2Metadata project (http://www.c2metadata.org)
 
C2Metadata is supported by the Data Infrastructure Building Blocks (DIBBs) program of the National Science Foundation through grant NSF ACI-1640575.
 
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 
**/
package us.mtna.pojo;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * SummaryStatistics contains multiple measures of a dataset.
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public class SummaryStatistics extends ResourceImpl {
	public static final String WEIGHT_VARS = "SummaryStats:WeightVariables";
	
	protected Double mean;
	protected Double median;
	protected Double mode;
	protected Double min;
	protected Double max;
	protected Double stdDev;
	protected Double other;
	protected Double valid;
	protected Double invalid;

	protected Double weightedMean;
	protected Double weightedMedian;
	protected Double weightedMode;
	protected Double weightedMin;
	protected Double weightedMax;
	protected Double weightedStdDev;
	protected Double weightedOther;
	protected Double weightedValid;
	protected Double weightedInvalid;

	private boolean isWeighted;
	
	protected Set<String> weightVariableNames;

	public SummaryStatistics() {
		weightVariableNames = new HashSet<>();
	}

	public boolean isWeighted() {
		return isWeighted;
	}

	public void setWeighted(boolean isWeighted) {
		this.isWeighted = isWeighted;
	}

	public Set<String> getWeightedVariableNames() {
		return weightVariableNames;
	}

	public void setWeightedVariableNames(Set<String> weightedVariableNames) {
		this.weightVariableNames = weightedVariableNames;
	}

	public void addWeightedVariableNames(String... variableNames) {
		for (String name : variableNames) {
			this.weightVariableNames.add(name);
		}
	}
	//this is used in compareWeightedVarRefs in the DatasetComparisonManager
	public Attribute<Set> getWeightedVariableNamesAttribute() {
		if (weightVariableNames != null) {
			Attribute<Set> weightedVariableNamesAttribute = new Attribute<>(Set.class);
			weightedVariableNamesAttribute.setName(WEIGHT_VARS);
			weightedVariableNamesAttribute.setValue(weightVariableNames);
			return weightedVariableNamesAttribute;
		}
		return null;
	}

	public Double getMean() {
		return mean;
	}

	public Attribute<Double> getMeanAttribute() {
		if (mean != null) {
			Attribute<Double> meanAttribute = new Attribute<>(Double.class);
			meanAttribute.setName("Mean");
			meanAttribute.setValue(mean);
			return meanAttribute;
		}
		return null;
	}

	public void setMean(Double mean) {
		this.mean = mean;
	}

	public Double getMedian() {
		return median;
	}

	public Attribute<Double> getMedianAttribute() {
		if (median != null) {
			Attribute<Double> medianAttribute = new Attribute<>(Double.class);
			medianAttribute.setName("Median");
			medianAttribute.setValue(median);
			return medianAttribute;
		}
		return null;
	}

	public void setMedian(Double median) {
		this.median = median;
	}

	public Double getMode() {
		return mode;
	}

	public Attribute<Double> getModeAttribute() {
		if (mode != null) {
			Attribute<Double> modeAttribute = new Attribute<>(Double.class);
			modeAttribute.setName("Mode");
			modeAttribute.setValue(mode);
			return modeAttribute;
		}
		return null;
	}

	public void setMode(Double mode) {
		this.mode = mode;
	}

	public Double getMin() {
		return min;
	}

	public Attribute<Double> getMinAttribute() {
		if (min != null) {
			Attribute<Double> minAttribute = new Attribute<>(Double.class);
			minAttribute.setName("Min");
			minAttribute.setValue(min);
			return minAttribute;
		}
		return null;
	}

	public void setMin(Double minimumValue) {
		this.min = minimumValue;
	}

	public Double getMax() {
		return max;
	}

	public Attribute<Double> getMaxAttribute() {
		if (max != null) {
			Attribute<Double> maxAttribute = new Attribute<>(Double.class);
			maxAttribute.setName("Max");
			maxAttribute.setValue(max);
			return maxAttribute;
		}
		return null;
	}

	public void setMax(Double max) {
		this.max = max;
	}

	public Double getStdDev() {
		return stdDev;
	}

	public Attribute<Double> getStdDevAttribute() {
		if (stdDev != null) {
			Attribute<Double> stdDevAttribute = new Attribute<>(Double.class);
			stdDevAttribute.setName("StdDev");
			stdDevAttribute.setValue(stdDev);
			return stdDevAttribute;
		}
		return null;
	}

	public void setStdDev(Double stdDev) {
		this.stdDev = stdDev;
	}

	public Double getOther() {
		return other;
	}

	public Attribute<Double> getOtherAttribute() {
		if (other != null) {
			Attribute<Double> otherAttribute = new Attribute<>(Double.class);
			otherAttribute.setName("Other");
			otherAttribute.setValue(other);
			return otherAttribute;
		}
		return null;
	}

	public void setOther(Double other) {
		this.other = other;
	}

	public Double getValid() {
		return valid;
	}

	public Attribute<Double> getValidAttribute() {
		if (valid != null) {
			Attribute<Double> validAttribute = new Attribute<>(Double.class);
			validAttribute.setName("Valid");
			validAttribute.setValue(valid);
			return validAttribute;
		}
		return null;
	}

	public void setValid(Double valid) {
		this.valid = valid;
	}

	public Double getInvalid() {
		return invalid;
	}

	public Attribute<Double> getInvalidAttribute() {
		if (invalid != null) {
			Attribute<Double> invalidAttribute = new Attribute<>(Double.class);
			invalidAttribute.setName("Invalid");
			invalidAttribute.setValue(invalid);
			return invalidAttribute;
		}
		return null;
	}

	public void setInvalid(Double invalid) {
		this.invalid = invalid;
	}

	public Double getWeightedMean() {
		return weightedMean;
	}

	public void setWeightedMean(Double weightedMean) {
		this.weightedMean = weightedMean;
	}

	public Attribute<Double> getWeightedMeanAttribute() {
		if (weightedMean != null) {
			Attribute<Double> weightedMeanAttribute = new Attribute<>(Double.class);
			weightedMeanAttribute.setName("WeightedMean");
			weightedMeanAttribute.setValue(weightedMean);
			return weightedMeanAttribute;
		}
		return null;
	}

	public Double getWeightedMedian() {
		return weightedMedian;
	}

	public void setWeightedMedian(Double weightedMedian) {
		this.weightedMedian = weightedMedian;
	}

	public Attribute<Double> getWeightedMedianAttribute() {
		if (weightedMedian != null) {
			Attribute<Double> weightedMedianAttribute = new Attribute<>(Double.class);
			weightedMedianAttribute.setName("WeightedMedian");
			weightedMedianAttribute.setValue(weightedMedian);
			return weightedMedianAttribute;
		}
		return null;
	}

	public Double getWeightedMode() {
		return weightedMode;
	}

	public void setWeightedMode(Double weightedMode) {
		this.weightedMode = weightedMode;
	}

	public Attribute<Double> getWeightedModeAttribute() {
		if (weightedMode != null) {
			Attribute<Double> weightedModeAttribute = new Attribute<>(Double.class);
			weightedModeAttribute.setName("WeightedMode");
			weightedModeAttribute.setValue(weightedMode);
			return weightedModeAttribute;
		}
		return null;
	}

	public Double getWeightedMin() {
		return weightedMin;
	}

	public void setWeightedMin(Double weightedMin) {
		this.weightedMin = weightedMin;
	}

	public Attribute<Double> getWeightedMinAttribute() {
		if (weightedMin != null) {
			Attribute<Double> weightedMinAttribute = new Attribute<>(Double.class);
			weightedMinAttribute.setName("WeightedMin");
			weightedMinAttribute.setValue(weightedMin);
			return weightedMinAttribute;
		}
		return null;
	}

	public Double getWeightedMax() {
		return weightedMax;
	}

	public void setWeightedMax(Double weightedMax) {
		this.weightedMax = weightedMax;
	}

	public Attribute<Double> getWeightedMaxAttribute() {
		if (weightedMax != null) {
			Attribute<Double> weightedMaxAttribute = new Attribute<>(Double.class);
			weightedMaxAttribute.setName("WeightedMax");
			weightedMaxAttribute.setValue(weightedMax);
			return weightedMaxAttribute;
		}
		return null;
	}

	public Double getWeightedStdDev() {
		return weightedStdDev;
	}

	public void setWeightedStdDev(Double weightedStdDev) {
		this.weightedStdDev = weightedStdDev;
	}

	public Attribute<Double> getWeightedStdDevAttribute() {
		if (weightedStdDev != null) {
			Attribute<Double> weightedStdDevAttribute = new Attribute<>(Double.class);
			weightedStdDevAttribute.setName("WeightedStdDev");
			weightedStdDevAttribute.setValue(weightedStdDev);
			return weightedStdDevAttribute;
		}
		return null;
	}

	public Double getWeightedOther() {
		return weightedOther;
	}

	public void setWeightedOther(Double weightedOther) {
		this.weightedOther = weightedOther;
	}

	public Attribute<Double> getWeightedOtherAttribute() {
		if (weightedOther != null) {
			Attribute<Double> weightedOtherAttribute = new Attribute<>(Double.class);
			weightedOtherAttribute.setName("WeightedOther");
			weightedOtherAttribute.setValue(weightedOther);
			return weightedOtherAttribute;
		}
		return null;
	}

	public Double getWeightedValid() {
		return weightedValid;
	}

	public void setWeightedValid(Double weightedValid) {
		this.weightedValid = weightedValid;
	}

	public Attribute<Double> getWeightedValidAttribute() {
		if (weightedValid != null) {
			Attribute<Double> weightedValidAttribute = new Attribute<>(Double.class);
			weightedValidAttribute.setName("WeightedValid");
			weightedValidAttribute.setValue(weightedValid);
			return weightedValidAttribute;
		}
		return null;
	}

	public Double getWeightedInvalid() {
		return weightedInvalid;
	}

	public void setWeightedInvalid(Double weightedInvalid) {
		this.weightedInvalid = weightedInvalid;
	}

	public Attribute<Double> getWeightedInvalidAttribute() {
		if (weightedInvalid != null) {
			Attribute<Double> weightedInvalidAttribute = new Attribute<>(Double.class);
			weightedInvalidAttribute.setName("WeightedInvalid");
			weightedInvalidAttribute.setValue(weightedInvalid);
			return weightedInvalidAttribute;
		}
		return null;
	}

	public List<Attribute<?>> getAttributes() {
		List<Attribute<?>> attributes = super.getAttributes();
		if (mean != null) {
			Attribute<Double> meanAttribute = new Attribute<>(Double.class);
			meanAttribute.setName("Mean");
			meanAttribute.setValue(mean);
			attributes.add(meanAttribute);
		}
		if (median != null) {
			Attribute<Double> medianAttribute = new Attribute<>(Double.class);
			medianAttribute.setName("Median");
			medianAttribute.setValue(median);
			attributes.add(medianAttribute);
		}
		if (mode != null) {
			Attribute<Double> modeAttribute = new Attribute<>(Double.class);
			modeAttribute.setName("Mode");
			modeAttribute.setValue(mode);
			attributes.add(modeAttribute);
		}
		if (min != null) {
			Attribute<Double> minAttribute = new Attribute<>(Double.class);
			minAttribute.setName("Min");
			minAttribute.setValue(min);
			attributes.add(minAttribute);
		}
		if (max != null) {
			Attribute<Double> maxAttribute = new Attribute<>(Double.class);
			maxAttribute.setName("Max");
			maxAttribute.setValue(max);
			attributes.add(maxAttribute);
		}
		if (stdDev != null) {
			Attribute<Double> stdDevAttribute = new Attribute<>(Double.class);
			stdDevAttribute.setName("StandardDeviation");
			stdDevAttribute.setValue(stdDev);
			attributes.add(stdDevAttribute);
		}
		if (other != null) {
			Attribute<Double> otherAttribute = new Attribute<>(Double.class);
			otherAttribute.setName("Other");
			otherAttribute.setValue(other);
			attributes.add(otherAttribute);
		}
		if (valid != null) {
			Attribute<Double> validAttribute = new Attribute<>(Double.class);
			validAttribute.setName("Valid");
			validAttribute.setValue(valid);
			attributes.add(validAttribute);
		}
		if (invalid != null) {
			Attribute<Double> invalidAttribute = new Attribute<>(Double.class);
			invalidAttribute.setName("Invalid");
			invalidAttribute.setValue(invalid);
			attributes.add(invalidAttribute);
		}
		return attributes;
	}

}

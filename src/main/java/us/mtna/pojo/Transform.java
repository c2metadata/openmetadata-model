/**
<Overview>
 
Copyright 2017 Metadata Technology North America Inc.
 
This software is the result of collaborative efforts from all participants of the C2Metadata project (http://www.c2metadata.org)
 
C2Metadata is supported by the Data Infrastructure Building Blocks (DIBBs) program of the National Science Foundation through grant NSF ACI-1640575.
 
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 
**/
package us.mtna.pojo;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * A transform describes a action that takes place on a derived variable in
 * order to create it. A general description is provided along with the command
 * in the original syntax in which it was written. Additionally, alternative
 * syntaxes of the command that are derived from the original command can be
 * provided.
 * 
 * @author Jack Gager (j.gager@mtna.us)
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Transform {

	private String description;
	private LinkedHashSet<String> sourceIds;
	private TransformCommand originalCommand;
	private final ArrayList<TransformCommand> alternativeCommands;

	public Transform() {
		sourceIds = new LinkedHashSet<>();
		alternativeCommands = new ArrayList<>();
	}

	/**
	 * Gets a textual description of the transform. This describes the process
	 * 
	 * @return
	 */
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public LinkedHashSet<String> getSourceIds() {
		return sourceIds;
	}

	public void setSourceIds(LinkedHashSet<String> sourceIds) {
		this.sourceIds.clear();
		this.sourceIds.addAll(sourceIds);
	}

	public void addSourceIds(String... sourceIds) {
		for (String id : sourceIds) {
			this.sourceIds.add(id);
		}
	}

	public TransformCommand getOriginalCommand() {
		return originalCommand;
	}

	public void setOriginalCommand(TransformCommand originalCommand) {
		this.originalCommand = originalCommand;
	}

	public TransformCommand[] getAlternativeCommands() {
		return alternativeCommands.toArray(new TransformCommand[0]);
	}

	public void addAlternativeCommands(TransformCommand... alternativeCommands) {
		for (TransformCommand alternativeCommand : alternativeCommands) {
			this.alternativeCommands.add(alternativeCommand);
		}
	}

	public void setAlternativeCommands(List<TransformCommand> alternativeCommands) {
		this.alternativeCommands.addAll(alternativeCommands);
	}

	public TransformCommand[] getAllCommands() {
		int counter = 0;
		TransformCommand[] commands = new TransformCommand[(originalCommand != null ? 1 : 0)
				+ alternativeCommands.size()];
		if (originalCommand != null) {
			commands[counter++] = originalCommand;
		}
		for (TransformCommand alternativeCommand : alternativeCommands) {
			commands[counter++] = alternativeCommand;
		}
		return commands;

	}

}

/**
<Overview>
 
Copyright 2017 Metadata Technology North America Inc.
 
This software is the result of collaborative efforts from all participants of the C2Metadata project (http://www.c2metadata.org)
 
C2Metadata is supported by the Data Infrastructure Building Blocks (DIBBs) program of the National Science Foundation through grant NSF ACI-1640575.
 
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 
**/
package us.mtna.pojo;

import java.util.Comparator;

/**
 * Codes contain a {@link #value} and {@link #label} and are used in
 * {@linkplain Classification}s
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public class Code extends ResourceImpl {

	private String value;
	private String label;
	private boolean missing;

	public Code() {
		this.missing = false;
	}

	/**
	 * get a code's value as an attribute to use in a comparison result
	 * 
	 * @return code value as string attribute
	 */
	public Attribute<String> getCodeValueAttribute() {
		Attribute<String> codeValue = new Attribute<>(String.class);
		codeValue.setName("codeValue");
		if (value != null) {
			codeValue.setValue(value);
		}
		return codeValue;
	}

	/**
	 * get a code's label as an attribute to use in a comparison result
	 * 
	 * @return code label as string attribute
	 */
	public Attribute<String> getCodeLabelAttribute() {
		Attribute<String> codeLabel = new Attribute<>(String.class);
		codeLabel.setName("codeLabel");
		if (label != null) {
			codeLabel.setValue(label);
		}
		return codeLabel;
	}

	/**
	 * get code value as a string
	 */
	public String getCodeValue() {
		return value;
	}

	public void setCodeValue(String codeValue) {
		this.value = codeValue;
	}

	/**
	 * get a code's label as a string
	 */
	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public boolean isMissing() {
		return missing;
	}

	/**
	 * set as a missing value. true = missing.
	 * 
	 */
	public void setMissing(boolean missing) {
		this.missing = missing;
	}

	public static final Comparator<Code> NumericCodeValueComparator = new Comparator<Code>() {
		//constructor inside this, the compare override would just delegate to different methods of a simple code coparator
		@Override
		public int compare(Code c1, Code c2) {
			try { //a try catch for each 
				Double value1 = Double.valueOf(c1.getCodeValue());
				Double value2 = Double.valueOf(c2.getCodeValue());
				return value1.compareTo(value2);
			} catch (NumberFormatException e) {
				throw new RuntimeException(e);
			}
		}
	};

	public static final Comparator<Code> AlphabeticalCodeValueComparator = new Comparator<Code>() {

		@Override
		public int compare(Code c1, Code c2) {
			String value1 = c1.getCodeValue();
			String value2 = c2.getCodeValue();
			return value1.compareTo(value2);
		}
	};
}

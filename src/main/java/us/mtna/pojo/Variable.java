/**
<Overview>
 
Copyright 2017 Metadata Technology North America Inc.
 
This software is the result of collaborative efforts from all participants of the C2Metadata project (http://www.c2metadata.org)
 
C2Metadata is supported by the Data Infrastructure Building Blocks (DIBBs) program of the National Science Foundation through grant NSF ACI-1640575.
 
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 
**/
package us.mtna.pojo;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

/**
 * A variable defines the characteristic that is to be measured. Variables
 * contain {@link Attribute}s. Here, variable adds {@link DataType},
 * {@link SummaryStatistics}, and {@link Classification} to
 * {@link #getAttributes() }
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public class Variable extends ResourceImpl {
	public static final String SUMMARY_STATS = "summaryStats";

	private DataType dataType;
	private SummaryStatistics summaryStatistics;
	private String classificationId;
	private String questionText;
	private Integer textDisplayWidth;
	private Long originalStart;
	private Long startPosition;
	private Long originalEnd;
	private Long endPosition;
	private String textStorageWidth;
	private String label;
	private List<Transform> transforms;
	private String originalSourceId;
	private boolean modified;
	private boolean deleted;
	private boolean referenced;
	//added for eml: 
	private String description; //attribute definition
	private Measure measure;

	public enum Measure{
		NOMINAL, ORDINAL, INTERVAL, RATIO, PERCENT, DATE, OTHER;
	}
	
	public Variable() {
		transforms = new ArrayList<>();
	}

	public Variable(String name, String id) {
		this(name, id, null);
	}

	public Variable(String name, String id, DataType dataType) {
		this();
		this.name = name;
		this.uuid = id;
		this.dataType = dataType;
	}

	public DataType getDataType() {
		return dataType;
	}

	public void setDataType(DataType dataType) {
		this.dataType = dataType;
	}

	public SummaryStatistics getSummaryStatistics() {
		return summaryStatistics;
	}

	public void setSummaryStatistics(SummaryStatistics summaryStatistics) {
		this.summaryStatistics = summaryStatistics;
	}

	public String getClassificationId() {
		return classificationId;
	}

	public void setClassification(Classification classification) {
		this.classificationId = classification.getId();
	}

	public String getQuestionText() {
		return questionText;
	}

	public void setQuestionText(String questionText) {
		this.questionText = questionText;
	}

	public Integer getTextDisplayWidth() {
		return textDisplayWidth;
	}

	public void setTextDisplayWidth(Integer textDisplayWidth) {
		this.textDisplayWidth = textDisplayWidth;
	}

	public Long getOriginalStart() {
		return originalStart;
	}

	public void setOriginalStart(Long originalStart) {
		this.originalStart = originalStart;
	}

	public Long getStartPosition() {
		return startPosition;
	}

	public void setStartPosition(Long startPosition) {
		this.startPosition = startPosition;
	}

	public Long getOriginalEnd() {
		return originalEnd;
	}

	public void setOriginalEnd(Long originalEnd) {
		this.originalEnd = originalEnd;
	}

	public Long getEndPosition() {
		return endPosition;
	}

	public void setEndPosition(Long endPosition) {
		this.endPosition = endPosition;
	}

	public String getTextStorageWidth() {
		return textStorageWidth;
	}

	public void setTextStorageWidth(String textStorageWidth) {
		this.textStorageWidth = textStorageWidth;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public void setClassificationId(String classificationId) {
		this.classificationId = classificationId;
	}

	public String getOriginalSourceId() {
		return originalSourceId;
	}

	public void setOriginalSourceId(String originalSourceId) {
		this.originalSourceId = originalSourceId;
	}

	public boolean isModified() {
		return modified;
	}

	public void setModified(boolean modified) {
		this.modified = modified;
	}

	public Transform[] getTransforms() {
		return transforms.toArray(new Transform[0]);
	}

	public void setTransforms(Collection<Transform> transforms) {
		this.transforms.clear();
		this.transforms.addAll(transforms);
	}

	public void addTransforms(Transform... transforms) {
		for (Transform transform : transforms) {
			this.transforms.add(transform);
		}
	}

	public void removeAllTransforms() {
		this.transforms.clear();
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public boolean isReferenced() {
		return referenced;
	}

	public void setReferenced(boolean referenced) {
		this.referenced = referenced;
	}

	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public Measure getMeasure() {
		return measure;
	}
	
	public void setMeasure(Measure measure) {
		this.measure = measure;
	}
	
	/**
	 * Get the list of attributes that will be used in comparing source and
	 * target variables. If variables differ on these properties, the name and
	 * value of the attribute will be listed in the output.
	 * 
	 * This method adds DataType, SummaryStatistics, and Deleted to the list
	 * defined in ResourceImpl.
	 */
	public List<Attribute<?>> getAttributes() {
		List<Attribute<?>> attributes = super.getAttributes();

		if (dataType != null) {
			Attribute<DataType> dataTypeAttribute = new Attribute<>(DataType.class);
			dataTypeAttribute.setName("DataType");
			dataTypeAttribute.setValue(dataType);
			attributes.add(dataTypeAttribute);
		}
		if (summaryStatistics != null) {
			Attribute<SummaryStatistics> summaryStatsAttribute = new Attribute<>(SummaryStatistics.class);
			summaryStatsAttribute.setName(SUMMARY_STATS);
			summaryStatsAttribute.setValue(summaryStatistics);
			attributes.add(summaryStatsAttribute);
		}
		if (questionText != null) {
			Attribute<String> questionTextAtt = new Attribute<>(String.class);
			questionTextAtt.setName("QuestionText");
			questionTextAtt.setValue(questionText);
			attributes.add(questionTextAtt);
		}

		// testing to catch vars that are still in the ds but marked as deleted.
		Attribute<Boolean> deletedAttribute = new Attribute<>(Boolean.class);
		deletedAttribute.setName("Deleted");
		deletedAttribute.setValue(deleted);
		attributes.add(deletedAttribute);

		return attributes;
	}

	public static final Comparator<Variable> StartPositionComparator = new Comparator<Variable>() {

		@Override
		public int compare(Variable v1, Variable v2) {
			Long startPos1 = v1.getStartPosition();
			Long startPos2 = v2.getStartPosition();
			return startPos1.compareTo(startPos2);
		}

	};

}

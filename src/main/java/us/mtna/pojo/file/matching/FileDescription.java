package us.mtna.pojo.file.matching;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FileDescription {
	//TODO add on an input stream for easier matching here? 
	/**
	 * the file name used in the script
	 */
	@JsonProperty("input_file_name")
	private String inputFileName;
	/**
	 * name of the ddi xml file
	 */
	@JsonProperty("DDI_XML_file")
	private String ddiXmlFile;
	/**
	 * file name used in the ddi file when more than one data file is described
	 */
	@JsonProperty("file_name_DDI")
	private String fileNameDdi;
	/**
	 * variable list extracted from the ddi file.
	 */
	//TODO I think this should be a list of strings but its this way in the example
	private String variables;

	public FileDescription() {
	}

	public String getInputFileName() {
		return inputFileName;
	}

	public void setInputFileName(String inputFileName) {
		this.inputFileName = inputFileName;
	}

	public String getDdiXmlFile() {
		return ddiXmlFile;
	}

	public void setDdiXmlFile(String ddiXmlFile) {
		this.ddiXmlFile = ddiXmlFile;
	}

	public String getFileNameDdi() {
		return fileNameDdi;
	}

	public void setFileNameDdi(String fileNameDdi) {
		this.fileNameDdi = fileNameDdi;
	}

	public String getVariables() {
		return variables;
	}

	public void setVariables(String variables) {
		this.variables = variables;
	}

	

}

package us.mtna.pojo.file.matching;
/**
 * Outermost object in the file matching JSON passed to the Updater by the File Upload UI. 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public class FileMatcher {
	
	private Parameters parameters;
	
	public Parameters getParameters() {
		return parameters;
	}
	
	public void setParameters(Parameters parameters) {
		this.parameters = parameters;
	}
}

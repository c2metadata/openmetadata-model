package us.mtna.pojo.file.matching;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Parameters {

	private String stata;
	@JsonProperty("data_file_descriptions")
	private List<FileDescription> fileDescriptions;

	public String getStata() {
		return stata;
	}

	public void setStata(String stata) {
		this.stata = stata;
	}

	public List<FileDescription> getFileDescriptions() {
		return fileDescriptions;
	}

	public void setFileDescriptions(List<FileDescription> fileDescriptions) {
		this.fileDescriptions = fileDescriptions;
	}

}

/**
<Overview>
 
Copyright 2017 Metadata Technology North America Inc.
 
This software is the result of collaborative efforts from all participants of the C2Metadata project (http://www.c2metadata.org)
 
C2Metadata is supported by the Data Infrastructure Building Blocks (DIBBs) program of the National Science Foundation through grant NSF ACI-1640575.
 
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 
**/
package us.mtna.pojo;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * An abstract class that implements {@link Resource.java} and adds
 * {@link #name} and {@link #uuid} to {@link #getAttributes()} since they are
 * applicable to all attributes
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
@JsonIgnoreProperties(ignoreUnknown=true)
public abstract class ResourceImpl implements Resource {

	protected String name;
	protected String uuid;

	public ResourceImpl() {
		this(null);
	}

	/**
	 * if resource is given a name only, provide a random UUID as id
	 * 
	 * @param name
	 */
	public ResourceImpl(String name) {
		this.name = name;
		uuid = UUID.randomUUID().toString();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getId() {
		return uuid;
	}

	public Attribute<String> getIdAttribute() {
		if (uuid != null) {
			Attribute<String> idAttribute = new Attribute<>(String.class);
			idAttribute.setName("ID");
			idAttribute.setValue(uuid);
			return idAttribute;
		}
		return null;
	}
	
	public Attribute<String> getNameAttr() {
		if (name != null) {
			Attribute<String> nameAttribute = new Attribute<>(String.class);
			nameAttribute.setName("Name");
			nameAttribute.setValue(name);
			return nameAttribute;
		}
		return null;
	}

	public void setId(String id) {
		this.uuid = id;
	}

	public List<Attribute<?>> getAttributes() {
		List<Attribute<?>> attributes = new ArrayList<>();

		return attributes;
	}
}

/**
<Overview>
 
Copyright 2017 Metadata Technology North America Inc.
 
This software is the result of collaborative efforts from all participants of the C2Metadata project (http://www.c2metadata.org)
 
C2Metadata is supported by the Data Infrastructure Building Blocks (DIBBs) program of the National Science Foundation through grant NSF ACI-1640575.
 
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 
**/
package us.mtna.pojo;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Classifications are comprised of codes. Lists of codes can be added all at
 * once with {@link #setCodeList(List)}, or add one list to the existing list of
 * codes with {@link #addCodesToCodeList(List)}.
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public class Classification extends ResourceImpl {

	public static final String CODE_COUNT = "codeCount";
	public static final String CODE_LIST = "codeList";
	public static final String CODE = "code";

	private LinkedHashMap<String, Code> codeList;

	public Classification() {
		codeList = new LinkedHashMap<>();
	}

	/**
	 * get all codes from a classification in their original form
	 * 
	 * @return
	 */
	public List<Code> getCodeList() {
		return new ArrayList<>(codeList.values());
	}

	/**
	 * get the classification's code list as an attribute
	 * 
	 * @return
	 */
	public Attribute<List<Code>> getCodeListAttr() {
		if (codeList != null) {
			Attribute<List<Code>> codeListAttribute = new Attribute<>();
			codeListAttribute.setName(CODE_LIST);
			codeListAttribute.setValue(getCodeList());
			return codeListAttribute;
		}
		return null;
	}

	/**
	 * clears the current codelist and adds back the list provided
	 * 
	 * @param codeList
	 */
	public void setCodeList(List<Code> codeList) {
		this.codeList.clear();
		addCodesToCodeList(codeList);
	}

	public void addCodesToCodeList(List<Code> codeList) {
		for (Code c : codeList) {
			this.codeList.put(c.getCodeValue().trim(), c);
		}
	}

	public List<Attribute<Code>> getCodes() {
		List<Attribute<Code>> codes = new ArrayList<>();
		if (codeList != null) {
			for (Code code : codeList.values()) {
				Attribute<Code> codeAttribute = new Attribute<>(Code.class);
				codeAttribute.setName(CODE);
				codeAttribute.setValue(code);
				codes.add(codeAttribute);
			}
		}
		return codes;
	}

	/**
	 * pass in a code label and get the corresponding code attribute for use in
	 * populating ComparsionResults that require Attributes.
	 * 
	 * @param label
	 * @return
	 */
	public Attribute<Code> getCodeForLabel(String label) {
		if (codeList != null) {
			for (Code code : codeList.values()) {
				if (code.getLabel().equals(label)) {
					Attribute<Code> codeAttribute = new Attribute<>(Code.class);
					codeAttribute.setName(CODE);
					codeAttribute.setValue(code);
					return codeAttribute;
				}
			}
		}
		return null;
	}

	public Attribute<Integer> getCodeCount() {
		Attribute<Integer> codeCount = new Attribute<>(Integer.class);
		codeCount.setName(CODE_COUNT);
		if (codeList != null) {
			codeCount.setValue(codeList.size());
		} else {
			codeCount.setValue(0);
		}
		return codeCount;
	}

	public List<Attribute<?>> getAttributes() {
		List<Attribute<?>> attributes = super.getAttributes();
		if (codeList != null) {
			for (Code code : codeList.values()) {
				Attribute<Code> codeAttribute = new Attribute<>(Code.class);
				codeAttribute.setName(CODE);
				codeAttribute.setValue(code);
				attributes.add(codeAttribute);
			}
		}

		return attributes;
	}

	/**
	 * Look up a code by its code value
	 * 
	 * @param codeValue
	 * @return Code object with the given code value, from the code list
	 */
	public Code lookupCode(String codeValue) {
		return codeList.get(codeValue);
	}

	public List<Code> getCodesByValues(List<String> codeValues) {
		List<Code> codes = new ArrayList<>();
		for (String value : codeValues) {
			codes.add(lookupCode(value));
		}
		return codes;
	}

	/**
	 * Removes the code from the code list
	 * 
	 * @param codeValue
	 */
	public void removeCode(String codeValue) {
		this.codeList.remove(codeValue);
	}

	/**
	 * Adds the given code to the code list
	 * 
	 * @param code
	 */
	public void addNewCode(Code code) {
		this.codeList.put(code.getCodeValue().trim(), code);
	}

	/**
	 * Selects a range of codes from the codelist, inclusive of start and end
	 * values
	 * 
	 * @param startValue
	 * @param endValue
	 * @return a list of codes
	 */
//	public List<Code> selectCodeRange(String startValue, String endValue) {
//		ArrayList<Code> codes = new ArrayList<>();
//		boolean inRange = false;
//		for (Code code : codeList.values()) {
//			if (startValue == null && !inRange) {
//				inRange = true;
//			} else if (startValue != null && !inRange) {
//				inRange = startValue.equals(code.getCodeValue().trim());
//			}
//
//			if (inRange) {
//				codes.add(code);
//
//				if (endValue != null) {
//					inRange = !endValue.equals(code.getCodeValue().trim());
//				}
//			}
//		}
//		return codes;
//	}

	public List<Code> selectCodeRange(String startValue, String endValue) {
		ArrayList<Code> codes = new ArrayList<>();
		boolean inRange = false;
		Integer start = null;
		Integer end = null;
		if (startValue.matches("-?\\d+")) {
			// startValue is an int
			start = Integer.parseInt(startValue);
		}
		if (endValue.matches("-?\\d+")) {
			// end value is an int
			end = Integer.parseInt(endValue);
		}

		// method for parsing int ranges:
		for (Code code : codeList.values()) {

			if (start != null && end != null) {
				Integer codeVal = null;
				if (code.getCodeValue().trim().matches("-?\\d+")) {
					// end value is an int
					codeVal = Integer.parseInt(code.getCodeValue().trim());
				}

				if (codeVal <= end && codeVal >= start) {
					codes.add(code);
				}
			} else {
				//parse them as strings
				if (startValue == null && !inRange) {
					inRange = true;
				} else if (startValue != null && !inRange) {
					inRange = startValue.equals(code.getCodeValue().trim());
				}

				if (inRange) {
					codes.add(code);

					if (endValue != null) {
						inRange = !endValue.equals(code.getCodeValue().trim());
					}
				}
			}
		}

		return codes;
	}

}

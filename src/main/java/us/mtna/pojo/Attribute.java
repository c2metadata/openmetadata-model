/**
<Overview>
 
Copyright 2017 Metadata Technology North America Inc.
 
This software is the result of collaborative efforts from all participants of the C2Metadata project (http://www.c2metadata.org)
 
C2Metadata is supported by the Data Infrastructure Building Blocks (DIBBs) program of the National Science Foundation through grant NSF ACI-1640575.
 
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 
**/
package us.mtna.pojo;

/**
 * Attributes hold characteristics of variables. Contains {@link #clazz} of type
 * Class<O> so variables can be compared by their class regardless of type.
 * Attribute also contains {@link #value} of type Object, and a {@link #name}
 * string.
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public class Attribute<O extends Object> {

	private Class<O> clazz;
	private O value;
	private String name;
	private boolean isReference;

	public Attribute() {
		this(null);
	}

	public Attribute(Class<O> clazz) {
		this(clazz, null, null, false);
	}

	public Attribute(Class<O> clazz, String name, O value, boolean isReference) {
		this.clazz = clazz;
		this.name = name;
		this.value = value;
		this.isReference = isReference;

	}

	public Class<O> getClazz() {
		return clazz;
	}

	public void setClazz(Class<O> clazz) {
		this.clazz = clazz;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public O getValue() {
		return value;
	}

	public void setValue(O value) {
		this.value = value;
	}

	/**
	 * Denotes if an attribute is a reference to another variable, like a
	 * variable's classification id that can be mapped back to a specific
	 * {@link Classification} object.
	 * 
	 * @return
	 */
	public boolean isReference() {
		return isReference;
	}

	public void setReference(boolean isReference) {
		this.isReference = isReference;
	}

	@Override
	public String toString() {
		return this.value + "";
	}

}

package us.mtna.pojo;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * A FileTransform describes a file-level transformation, noting which variables
 * were dropped, kept, and added, and if cases were added or dropped. Follows
 * the schema found in src/test/resources.
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public class FileTransform {

	private Set<String> droppedVariables;
	private Set<String> keptVariables;
	private Set<String> addedVariables;
	private boolean addCases;
	private boolean dropCases;
	private Transform transform;
	private String sourceDataset;

	public FileTransform(){
		this.droppedVariables = new HashSet<>();
		this.keptVariables = new HashSet<>();
		this.addedVariables = new HashSet<>();
	}
	
	public FileTransform(Transform transform) {
		this.droppedVariables = new HashSet<>();
		this.keptVariables = new HashSet<>();
		this.addedVariables = new HashSet<>();
		this.transform = transform;
	}

	public Transform getTransform() {
		return transform;
	}

	public void setTransform(Transform transform) {
		this.transform = transform;
	}

	public Set<String> getDroppedVariables() {
		return droppedVariables;
	}

	public void setDroppedVariables(Set<String> droppedVariables) {
		this.droppedVariables = droppedVariables;
	}

	public Set<String> getKeptVariables() {
		return keptVariables;
	}

	public void setKeptVariables(Set<String> keptVariables) {
		this.keptVariables = keptVariables;
	}

	public Set<String> getAddedVariables() {
		return addedVariables;
	}

	public void setAddedVariables(Set<String> addedVariables) {
		this.addedVariables = addedVariables;
	}

	public boolean isAddCases() {
		return addCases;
	}

	public void setAddCases(boolean addCases) {
		this.addCases = addCases;
	}

	public boolean isDropCases() {
		return dropCases;
	}

	public void setDropCases(boolean dropCases) {
		this.dropCases = dropCases;
	}

	public boolean hasDerivedVars() {
		return (!addedVariables.isEmpty() || !droppedVariables.isEmpty() || !keptVariables.isEmpty());
	}
	
	/**
	 * Id of the dataset this derived dataset is sourced from 
	 * @return
	 */
	public String getSourceDataset() {
		return sourceDataset;
	}
	
	public void setSourceDataset(String sourceDataset) {
		this.sourceDataset = sourceDataset;
	}

}
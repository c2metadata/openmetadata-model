/**
<Overview>
 
Copyright 2017 Metadata Technology North America Inc.
 
This software is the result of collaborative efforts from all participants of the C2Metadata project (http://www.c2metadata.org)
 
C2Metadata is supported by the Data Infrastructure Building Blocks (DIBBs) program of the National Science Foundation through grant NSF ACI-1640575.
 
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 
**/

package us.mtna.config;

/**
 * Classification-level comparison configuration used in
 * {@linkplain ComparisonConfig}. Users can specify whether or not to run a
 * classification comparison at all, and whether to include comparisons on
 * classification code counts and labels.
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public class ClassificationComparisonConfig {

	private boolean compared;
	private boolean codeCount;
	private boolean label;

	public ClassificationComparisonConfig() {
		codeCount = true;
		label = true;
		compared = true;
	}

	/**
	 * check whether classifications are set to be compared
	 * @return
	 */
	public boolean isCompared() {
		return compared;
	}

	/**
	 * toggle classification comparison
	 * 
	 * @param compared
	 */
	public void setCompared(boolean compared) {
		this.compared = compared;
	}

	/**
	 * check whether code counts are set to be compared
	 * 
	 * @return boolean
	 */
	public boolean isCodeCount() {
		return codeCount;
	}

	/**
	 * toggle classification code count comparison
	 * 
	 * @param codeCount
	 */
	public void setCodeCount(boolean codeCount) {
		this.codeCount = codeCount;
	}

	/**
	 * check if a classification's labels are set to be compared
	 * 
	 * @return boolean
	 */
	public boolean isLabel() {
		return label;
	}

	/**
	 * toggle classification label comparison
	 * 
	 * @param label
	 */
	public void setLabel(boolean label) {
		this.label = label;
	}

}

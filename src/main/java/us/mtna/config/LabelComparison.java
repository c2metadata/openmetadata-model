/**
<Overview>
 
Copyright 2017 Metadata Technology North America Inc.
 
This software is the result of collaborative efforts from all participants of the C2Metadata project (http://www.c2metadata.org)
 
C2Metadata is supported by the Data Infrastructure Building Blocks (DIBBs) program of the National Science Foundation through grant NSF ACI-1640575.
 
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 
**/
package us.mtna.config;

/**
 * comparison configuration used in {@linkplain VarialeComparisonConfig} to
 * toggle different aspects of variable label comparisons. Users can turn on or
 * off the whole comparison, or choose to include specific results for case and
 * space mismatch, as well as close match (2 or more types of mismatches). If
 * specific label comparisons occur but are switched off, they will be reported
 * as a more general ResultType.MISMATCH.
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public class LabelComparison {
	private boolean compared;
	private boolean caseMismatch;
	private boolean spaceMismatch;
	private boolean specialCharMismatch;
	private boolean closeMatch;

	public LabelComparison() {
		compared = true;
		caseMismatch = true;
		spaceMismatch = true;
		specialCharMismatch = true;
		closeMatch = true;
	}

	/**
	 * check whether special character mismatch result types are included in
	 * label comparison.
	 * 
	 * @return
	 */
	public boolean isSpecialCharMismatch() {
		return specialCharMismatch;
	}

	/**
	 * include special character mismatch result types
	 * 
	 * @param characterMismatch
	 */
	public void setSpecialCharMismatch(boolean specialCharMismatch) {
		this.specialCharMismatch = specialCharMismatch;
	}

	/**
	 * check whether case mismatch result types are included in label comparison
	 * 
	 * @return
	 */
	public boolean isCaseMismatch() {
		return caseMismatch;
	}

	/**
	 * include case mismatch result types
	 * 
	 * @param spaceMismatch
	 */
	public void setCaseMismatch(boolean caseMismatch) {
		this.caseMismatch = caseMismatch;
	}

	/**
	 * check whether space mismatch result types are included in label
	 * comparison
	 * 
	 * @return
	 */
	public boolean isSpaceMismatch() {
		return spaceMismatch;
	}

	/**
	 * include space mismatch result types
	 * 
	 * @param spaceMismatch
	 */
	public void setSpaceMismatch(boolean spaceMismatch) {
		this.spaceMismatch = spaceMismatch;
	}

	/**
	 * check whether close match result types are included in label comparison
	 * 
	 * @return
	 */
	public boolean isCloseMatch() {
		return closeMatch;
	}

	/**
	 * include close match result types
	 * 
	 * @param spaceMismatch
	 */
	public void setCloseMatch(boolean closeMatch) {
		this.closeMatch = closeMatch;
	}

	/**
	 * check whether include specific label comparisons
	 * 
	 * @return
	 */
	public boolean isCompared() {
		return compared;
	}

	/**
	 * compare variable labels using all comparison types
	 * 
	 * @param caseMismatch
	 */
	public void setCompared(boolean compared) {
		this.compared = compared;
	}

}

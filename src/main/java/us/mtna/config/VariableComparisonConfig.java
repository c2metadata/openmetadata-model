/**
<Overview>
 
Copyright 2017 Metadata Technology North America Inc.
 
This software is the result of collaborative efforts from all participants of the C2Metadata project (http://www.c2metadata.org)
 
C2Metadata is supported by the Data Infrastructure Building Blocks (DIBBs) program of the National Science Foundation through grant NSF ACI-1640575.
 
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 
**/
package us.mtna.config;

import java.util.ArrayList;
import java.util.List;

/**
 * Variable-level comparison configuration used in {@linkplain ComparisonConfig}. Variable comparison will always run,
 * but users can specify if and which comparisons are performed for labels and
 * summary statistics, and pass in pre-matched variable pairs if desired 
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public class VariableComparisonConfig {

	private SummaryStatisticsComparison sumStatsComparison;
	private LabelComparison labelComparison;
	private List<VariablePair> variablePairs;
	

	public VariableComparisonConfig() {
		sumStatsComparison = new SummaryStatisticsComparison();
		labelComparison = new LabelComparison();
		variablePairs = new ArrayList<>();
	}

	public SummaryStatisticsComparison getSumStatsComparison() {
		return sumStatsComparison;
	}

	public void setSumStatsComparison(SummaryStatisticsComparison sumStatsComparison) {
		this.sumStatsComparison = sumStatsComparison;
	}

	public LabelComparison getLabelComparison() {
		return labelComparison;
	}

	public void setLabelComparison(LabelComparison labelComparison) {
		this.labelComparison = labelComparison;
	}

	public List<VariablePair> getVariablePairs() {
		return variablePairs;
	}

	public void setVariablePairs(List<VariablePair> variablePairs) {
		this.variablePairs.clear();
		this.variablePairs.addAll(variablePairs);
	}
	
	public void addVariablePairs(VariablePair...variablePairs){
		for(VariablePair pair : variablePairs){
			this.variablePairs.add(pair);
		}
	}

	
	
}

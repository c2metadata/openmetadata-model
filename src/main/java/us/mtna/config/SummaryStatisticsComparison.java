/**
<Overview>
 
Copyright 2017 Metadata Technology North America Inc.
 
This software is the result of collaborative efforts from all participants of the C2Metadata project (http://www.c2metadata.org)
 
C2Metadata is supported by the Data Infrastructure Building Blocks (DIBBs) program of the National Science Foundation through grant NSF ACI-1640575.
 
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 
**/
package us.mtna.config;

/**
 * 
 * SummaryStatistics-level comparison configuration used in
 * {@linkplain VarialeComparisonConfig}. Users can turn on or off the whole
 * comparison, or choose specific measures to compare
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public class SummaryStatisticsComparison {
	private boolean compared;
	private boolean min;
	private boolean max;
	private boolean mean;
	private boolean mode;
	private boolean median;
	private boolean valid;
	private boolean invalid;
	private boolean stdDev;
	private boolean other;
	private boolean weighted;
	private boolean unweighted;

	public SummaryStatisticsComparison() {
		compared = true;
		min = true;
		max = true;
		mean = true;
		mode = true;
		median = true;
		valid = true;
		invalid = true;
		stdDev = true;
		other = true;
		weighted = true;
		unweighted = true;
	}

	/**
	 * check whether a variable's summary statistics are set to be compared
	 * @return
	 */
	public boolean isCompared() {
		return compared;
	}

	/**
	 * compare variables' summaryStatistics values
	 * @param compared
	 */
	public void setCompared(boolean compared) {
		this.compared = compared;
	}

	/**
	 * check whether minimum values are set to be compared
	 * @return
	 */
	public boolean isMin() {
		return min;
	}
	/**
	 * compare variables' minimum values
	 * @param compared
	 */
	public void setMin(boolean min) {
		this.min = min;
	}
	/**
	 * check whether maximum values are set to be compared
	 * @return
	 */
	public boolean isMax() {
		return max;
	}
	/**
	 * compare variables' maximum values
	 * @param compared
	 */
	public void setMax(boolean max) {
		this.max = max;
	}
	/**
	 * check whether mean values are set to be compared
	 * @return
	 */
	public boolean isMean() {
		return mean;
	}
	/**
	 * compare variables' mean values
	 * @param compared
	 */
	public void setMean(boolean mean) {
		this.mean = mean;
	}
	/**
	 * check whether mode values are set to be compared
	 * @return
	 */
	public boolean isMode() {
		return mode;
	}
	/**
	 * compare variables' mode values
	 * @param compared
	 */
	public void setMode(boolean mode) {
		this.mode = mode;
	}
	/**
	 * check whether median values are set to be compared
	 * @return
	 */
	public boolean isMedian() {
		return median;
	}
	/**
	 * compare variables' median values
	 * @param compared
	 */
	public void setMedian(boolean median) {
		this.median = median;
	}
	/**
	 * check whether valid values are set to be compared
	 * @return
	 */
	public boolean isValid() {
		return valid;
	}
	/**
	 * compare variables' valid values
	 * @param compared
	 */
	public void setValid(boolean valid) {
		this.valid = valid;
	}
	/**
	 * check whether invalid values are set to be compared
	 * @return
	 */
	public boolean isInvalid() {
		return invalid;
	}
	/**
	 * compare variables' invalid values
	 * @param compared
	 */
	public void setInvalid(boolean invalid) {
		this.invalid = invalid;
	}
	/**
	 * check whether standard deviation values are set to be compared
	 * @return
	 */
	public boolean isStdDev() {
		return stdDev;
	}
	/**
	 * compare variables' standard deviation values
	 * @param compared
	 */
	public void setStdDev(boolean stdDev) {
		this.stdDev = stdDev;
	}
	/**
	 * check whether "other" values are set to be compared
	 * @return
	 */
	public boolean isOther() {
		return other;
	}
	/**
	 * compare variables' "other" values
	 * @param compared
	 */
	public void setOther(boolean other) {
		this.other = other;
	}
	/**
	 * check whether weighted values are set to be compared
	 * @return
	 */
	public boolean isWeighted() {
		return weighted;
	}
	/**
	 * compare variables' weighted values
	 * @param compared
	 */
	public void setWeighted(boolean weighted) {
		this.weighted = weighted;
	}

	/**
	 * check whether unweighted values are set to be compared
	 * @return
	 */
	public boolean isUnweighted() {
		return unweighted;
	}
	/**
	 * compare variables' unweighted values
	 * @param compared
	 */
	public void setUnweighted(boolean unweighted) {
		this.unweighted = unweighted;
	}

}

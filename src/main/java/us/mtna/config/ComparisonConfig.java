/**
<Overview>
 
Copyright 2017 Metadata Technology North America Inc.
 
This software is the result of collaborative efforts from all participants of the C2Metadata project (http://www.c2metadata.org)
 
C2Metadata is supported by the Data Infrastructure Building Blocks (DIBBs) program of the National Science Foundation through grant NSF ACI-1640575.
 
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 
**/
package us.mtna.config;

/**
 * A customizable configuration object for specifying which comparisons to
 * perform. A sample file to use for customizing and passing into the controller
 * can be found in src/main/resources/comparisonConfigurationSample.json
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public class ComparisonConfig {

	private VariableComparisonConfig variableConfig;
	private ClassificationComparisonConfig classificationConfig;

	public ComparisonConfig() {
		this.variableConfig = new VariableComparisonConfig();
		this.classificationConfig = new ClassificationComparisonConfig();
	}

	public VariableComparisonConfig getVariableComparison() {
		return variableConfig;
	}

	public void setVariableComparison(VariableComparisonConfig variableComparison) {
		this.variableConfig = variableComparison;
	}

	public ClassificationComparisonConfig getClassificationComparison() {
		return classificationConfig;
	}

	public void setClassificationComparison(ClassificationComparisonConfig classificationComparison) {
		this.classificationConfig = classificationComparison;
	}
}

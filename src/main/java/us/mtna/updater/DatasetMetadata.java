package us.mtna.updater;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Container for dataset-level metadata provided by the user. All of the fields
 * are optional - anything not filled out that is required by the relevant XML
 * schema will be given a default value.
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public class DatasetMetadata {

	/**
	 * what the file will be saved as 
	 */
	//file name in the ui
	private String documentTitle;
	private String defaultTitle;
	//this is file description in the ui
	private String documentId;
	private String defaultDocumentId;
	private List<Author> authors;
	private List<Author> defaultAuthors;

	// default values
	public static final String VERSION = "VERSION OF DATASET RESULTED FROM TRANSFORMATION (EX: V1)";
	public static final String NOTE = "Data from this study were used in the transformations described in this DDI Instance ";
	public static final String DEFAULT_TITLE = "updatedMetadata.xml";
	public static final String DEFAULT_ID = "generated_file";
	// used in : eml system
	public static final String DEFAULT_SYSTEM = "C2M Metadata Generator";

	public DatasetMetadata() {
		this.authors = new ArrayList<>();
		this.defaultTitle = DEFAULT_TITLE;
		this.defaultDocumentId = DEFAULT_ID;
	}

	@JsonIgnore
	public String getPopulatedTitle() {
		if (documentTitle == null) {
			return DEFAULT_TITLE;
		}
		return documentTitle;
	}

	@JsonIgnore
	public String getPopulatedId() {
		if (documentId == null) {
			return DEFAULT_ID;
		}
		return documentId;
	}

	public String getDocumentTitle() {
		return documentTitle;
	}

	public void setDocumentTitle(String documentTitle) {
		this.documentTitle = documentTitle;
	}

	public String getDocumentId() {
		return documentId;
	}

	public void setDocumentId(String documentId) {
		this.documentId = documentId;
	}

	public List<Author> getAuthors() {
		return authors;
	}

	public void setAuthors(List<Author> authors) {
		this.authors = authors;
	}

	public String getDefaultTitle() {
		return defaultTitle;
	}

	public void setDefaultTitle(String defaultTitle) {
		this.defaultTitle = defaultTitle;
	}

	public String getDefaultDocumentId() {
		return defaultDocumentId;
	}

	public void setDefaultDocumentId(String defaultDocumentId) {
		this.defaultDocumentId = defaultDocumentId;
	}

	public List<Author> getDefaultAuthors() {
		return defaultAuthors;
	}

	public void setDefaultAuthors(List<Author> defaultAuthors) {
		this.defaultAuthors = defaultAuthors;
	}

}

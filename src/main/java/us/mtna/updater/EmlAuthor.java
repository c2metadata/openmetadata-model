package us.mtna.updater;

import java.util.ArrayList;
import java.util.List;

/**
 * Provide information about a single author.
 * 
 * In EML, both organization and first/last name will be described in the
 * creator field if both are present.
 * 
 * If there is a case where only one of these values is allowed but all are
 * present, the authorLastName will take precedence.
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public class EmlAuthor implements Author {

	// use one or more of: org, individual, position
	// org
	private String organizationName;
	// individual
	private List<String> salutations;
	private List<String> givenNames;
	private String surName;
	// position
	private String position;
	// optional
	private String emailAddress;

	public EmlAuthor() {
		salutations = new ArrayList<>();
		givenNames = new ArrayList<>();
	}

	public void setAffiliation(String organizationName) {
		this.organizationName = organizationName;
	}

	public String getSurName() {
		return surName;
	}

	public void setSurName(String surName) {
		this.surName = surName;
	}

	public List<String> getSalutations() {
		return salutations;
	}

	public void addSalutations(String... salutations) {
		for (String s : salutations) {
			this.salutations.add(s);
		}
	}

	public void setSalutations(List<String> salutations) {
		this.salutations = salutations;
	}

	public List<String> getGivenNames() {
		return givenNames;
	}

	public void setGivenNames(List<String> givenNames) {
		this.givenNames = givenNames;
	}

	public void addGivenNames(String... givenNames) {
		for (String s : givenNames) {
			this.givenNames.add(s);
		}
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	@Override
	public String getAffiliation() {
		return organizationName;
	}

	@Override
	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

}

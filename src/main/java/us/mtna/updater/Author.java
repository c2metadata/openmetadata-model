package us.mtna.updater;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;

/**
 * Because author information is handled differently in different metadata
 * schemas, we created different implementations to better handle them. You will
 * see there is no name listed here - that is because EML separates them into
 * first and last names, while ddi just allows for the full name. This field
 * should be handled in the implementation
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = As.PROPERTY, property = "type")
@JsonSubTypes({ @Type(value = EmlAuthor.class, name = "EML"), @Type(value = DdiAuthor.class, name = "DDI") })
public interface Author {

	String getAffiliation();

	String getEmailAddress();
	
}

package us.mtna.updater.parser;

public class SasParserParams extends ParserParameters {
	private String sas;

	public String getSas() {
		return sas;
	}

	public void setSas(String sas) {
		this.sas = sas;
	}
}

package us.mtna.updater.parser;

public class RParserParams extends ParserParameters{
	private String r;

	public String getR() {
		return r;
	}

	public void setR(String r) {
		this.r = r;
	}
}

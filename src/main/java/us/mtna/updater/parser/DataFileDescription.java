package us.mtna.updater.parser;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DataFileDescription {

	private String inputFileName;
	private String ddiXmlFile;
	private String fileNameDdi;
	private List<String> variables;

	public DataFileDescription() {
		this.variables = new ArrayList<>();
	}

	@JsonProperty("input_file_name")
	public String getInputFileName() {
		return inputFileName;
	}

	@JsonProperty("input_file_name")
	public void setInputFileName(String inputFileName) {
		this.inputFileName = inputFileName;
	}

	@JsonProperty("DDI_XML_file")
	public String getDdiXmlFile() {
		return ddiXmlFile;
	}

	@JsonProperty("DDI_XML_file")
	public void setDdiXmlFile(String ddiXmlFile) {
		this.ddiXmlFile = ddiXmlFile;
	}

	@JsonProperty("file_name_DDI")
	public String getFileNameDdi() {
		return fileNameDdi;
	}

	@JsonProperty("file_name_DDI")
	public void setFileNameDdi(String fileNameDdi) {
		this.fileNameDdi = fileNameDdi;
	}

	public List<String> getVariables() {
		return variables;
	}

	public void setVariables(List<String> variables) {
		this.variables = variables;
	}

}

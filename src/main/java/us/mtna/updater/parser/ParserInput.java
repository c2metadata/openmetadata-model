package us.mtna.updater.parser;

public class ParserInput {

	private ParserParameters parameters;

	public ParserParameters getParameters() {
		return parameters;
	}

	public void setParameters(ParserParameters parameters) {
		this.parameters = parameters;
	}
}

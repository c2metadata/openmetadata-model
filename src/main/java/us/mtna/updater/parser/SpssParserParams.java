package us.mtna.updater.parser;

public class SpssParserParams extends ParserParameters{
	private String spss;

	public String getSpss() {
		return spss;
	}

	public void setSpss(String spss) {
		this.spss = spss;
	}
}

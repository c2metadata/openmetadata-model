package us.mtna.updater.parser;

public class StataParserParams extends ParserParameters{
	private String stata;
	
	public String getStata() {
		return stata;
	}
	
	public void setStata(String stata) {
		this.stata = stata;
	}
}

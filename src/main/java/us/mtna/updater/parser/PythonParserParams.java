package us.mtna.updater.parser;

public class PythonParserParams extends ParserParameters {
	
	private String python;
	public String getPython() {
		return python;
	}

	public void setPython(String python) {
		this.python = python;
	}
}

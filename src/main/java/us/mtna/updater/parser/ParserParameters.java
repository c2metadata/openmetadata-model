package us.mtna.updater.parser;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public abstract class ParserParameters {

	private List<DataFileDescription> dataFileDescriptions;

	public ParserParameters() {
		this.dataFileDescriptions = new ArrayList<>();
		this.dataFileDescriptions.add(new DataFileDescription());
	}

	@JsonProperty("data_file_descriptions")
	public List<DataFileDescription> getDataFileDescriptions() {
		return dataFileDescriptions;
	}

	@JsonProperty("data_file_descriptions")
	public void setDataFileDescriptions(List<DataFileDescription> dataFileDescriptions) {
		this.dataFileDescriptions = dataFileDescriptions;
	}
	
	public void addDataFileDescriptions(DataFileDescription...descriptions){
		for(DataFileDescription d : descriptions){
			this.dataFileDescriptions.add(d);
		}
	}

	
}

package us.mtna.updater;

import java.util.ArrayList;
import java.util.List;

public class DdiCitation implements Citation<DdiAuthor> {

	private List<DdiAuthor> authors;

	public DdiCitation() {
		this.authors = new ArrayList<>();
	}

	public List<DdiAuthor> getAuthors() {
		return authors;
	}

	public void setAuthors(List<DdiAuthor> authors) {
		this.authors = authors;
	}

}

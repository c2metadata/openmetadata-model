package us.mtna.updater;

import java.util.ArrayList;
import java.util.List;

public class EmlCitation implements Citation<EmlAuthor> {

	private List<EmlAuthor> authors;

	public EmlCitation() {
		this.authors = new ArrayList<>();
	}

	@Override
	public List<EmlAuthor> getAuthors() {
		return authors;
	}

	public void setAuthors(List<EmlAuthor> authors) {
		this.authors = authors;
	}

}

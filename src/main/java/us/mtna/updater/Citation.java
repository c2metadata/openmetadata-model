package us.mtna.updater;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;

/**
 * Because metadata schemas have differently structured citations, we created
 * different implementations to better handle them.
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = As.PROPERTY, property = "type")
@JsonSubTypes({ @Type(value = EmlCitation.class, name = "ddi"), @Type(value = DdiCitation.class, name = "eml") })
public interface Citation<A extends Author> {

	List<A> getAuthors();
}

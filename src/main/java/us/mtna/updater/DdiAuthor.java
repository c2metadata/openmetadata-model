package us.mtna.updater;

/**
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public class DdiAuthor implements Author {

	private String fullName;
	private String affiliation;
	private String emailAddress;

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	@Override
	public String getAffiliation() {
		return affiliation;
	}

	public void setAffiliation(String affiliation) {
		this.affiliation = affiliation;
	}

	@Override
	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

}

/**
<Overview>
 
Copyright 2017 Metadata Technology North America Inc.
 
This software is the result of collaborative efforts from all participants of the C2Metadata project (http://www.c2metadata.org)
 
C2Metadata is supported by the Data Infrastructure Building Blocks (DIBBs) program of the National Science Foundation through grant NSF ACI-1640575.
 
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 
**/
package us.mtna.reporting.simple;

import java.util.ArrayList;
import java.util.List;

/**
 * Lists of variables used in a {@linkplain VariableComparisonResult}. Most are
 * just lists of variable names, aside from updatedVariables, which is a list of
 * {@linkplain SimpleComparisonResult}s to give more information about variable
 * mismatches.
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public class DataSetComparisonVariables {

	private List<String> newVariables;
	private List<String> droppedVariables;
	private List<SimpleComparisonResult> updatedVariables;
	private List<String> matchedVariables;

	public DataSetComparisonVariables() {
		updatedVariables = new ArrayList<>();
		newVariables = new ArrayList<>();
		droppedVariables = new ArrayList<>();
		matchedVariables = new ArrayList<>();
	}

	/**
	 * get a list of variable names for variables that are only present in the
	 * source dataset
	 * 
	 * @return
	 */
	public List<String> getDroppedVariables() {
		return droppedVariables;
	}

	/**
	 * set a list of names of dropped variables (variables only present in the
	 * source dataset)
	 * 
	 * @param droppedVariables
	 */
	public void setDroppedVariables(List<String> droppedVariables) {
		this.droppedVariables.clear();
		this.droppedVariables.addAll(droppedVariables);
	}

	/**
	 * add variable names to the list of dropped variable names
	 * 
	 * @param variables
	 */
	public void addDroppedVariables(String... variables) {
		for (String variable : variables) {
			this.droppedVariables.add(variable);
		}
	}

	/**
	 * get a list of {@linkplain SimpleComparisonResult}s about
	 * updated(mismatched) variables.
	 * 
	 * @return
	 */
	public List<SimpleComparisonResult> getUpdatedVariables() {
		return updatedVariables;
	}

	/**
	 * set a list of {@linkplain SimpleComparisonResult}s about
	 * updated(mismatched) variables.
	 * 
	 * @param updatedVariables
	 */
	public void setUpdatedVariables(List<SimpleComparisonResult> updatedVariables) {
		this.updatedVariables.clear();
		this.updatedVariables.addAll(updatedVariables);
	}

	/**
	 * add a single SimpleComparisonResult to the list of results about updated
	 * (mismatched) variables.
	 * 
	 * @param results
	 */
	public void addUpdatedVariables(SimpleComparisonResult... results) {
		for (SimpleComparisonResult result : results) {
			this.updatedVariables.add(result);
		}
	}

	/**
	 * get a list of names of variables that were present in both datasets
	 * 
	 * @return
	 */
	public List<String> getMatchedVariables() {
		return matchedVariables;
	}

	/**
	 * set a list of variable names for variables that were present in both
	 * datasets
	 * 
	 * @param matchedVariables
	 */
	public void setMatchedVariables(List<String> matchedVariables) {
		this.matchedVariables.clear();
		this.matchedVariables.addAll(matchedVariables);
	}

	/**
	 * add variable names to the list of matched variable names
	 * 
	 * @param variables
	 */
	public void addMatchedVariables(String... variables) {
		for (String variable : variables) {
			this.matchedVariables.add(variable);
		}
	}

	/**
	 * get a list of names of variables that are only present in the target
	 * dataset
	 * 
	 * @return
	 */
	public List<String> getNewVariables() {
		return newVariables;
	}

	/**
	 * set a list of names of variables that are only present in the target
	 * dataset
	 * 
	 * @return
	 */
	public void setNewVariables(List<String> newVariables) {
		this.newVariables.clear();
		this.newVariables.addAll(newVariables);
	}

	/**
	 * add variable names to the list of new variable names
	 * 
	 * @param variables
	 */
	public void addNewVariables(String... variables) {
		for (String variable : variables) {
			this.newVariables.add(variable);
		}
	}

}

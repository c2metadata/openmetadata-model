/**
<Overview>
 
Copyright 2017 Metadata Technology North America Inc.
 
This software is the result of collaborative efforts from all participants of the C2Metadata project (http://www.c2metadata.org)
 
C2Metadata is supported by the Data Infrastructure Building Blocks (DIBBs) program of the National Science Foundation through grant NSF ACI-1640575.
 
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 
**/
package us.mtna.reporting.simple;

import java.util.ArrayList;
import java.util.List;

import us.mtna.reporting.ResultType;

/**
 * Base class for providing basic comparison details about two objects: the
 * source, target, match, and attributes. Extending classes don't have to fill
 * out every property.
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public class SimpleComparisonResult {

	private DataSetComparisonVariables variables;
	private String source;
	private String target;
	private Boolean match;
	private ResultType resultType;
	private ArrayList<AttributeComparisonResult<?>> attributes;

	public SimpleComparisonResult() {
		attributes = new ArrayList<>();
	}

	public DataSetComparisonVariables getVariables() {
		return variables;
	}

	public void setVariables(DataSetComparisonVariables variables) {
		this.variables = variables;
	}

	public ResultType getResultType() {
		return resultType;
	}

	public void setResultType(ResultType resultType) {
		this.resultType = resultType;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}

	public Boolean getMatch() {
		return match;
	}

	public void setMatch(Boolean match) {
		this.match = match;
	}

	public List<AttributeComparisonResult<?>> getAttributes() {
		return attributes;
	}

	public void setAttributes(List<AttributeComparisonResult<?>> attributes) {
		this.attributes.clear();
		this.attributes.addAll(attributes);
	}

	public void addAttributes(AttributeComparisonResult<?>... attributes) {
		for (AttributeComparisonResult<?> att : attributes) {
			this.attributes.add(att);
		}
	}
}

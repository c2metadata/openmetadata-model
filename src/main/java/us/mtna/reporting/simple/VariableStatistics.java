/**
<Overview>
 
Copyright 2017 Metadata Technology North America Inc.
 
This software is the result of collaborative efforts from all participants of the C2Metadata project (http://www.c2metadata.org)
 
C2Metadata is supported by the Data Infrastructure Building Blocks (DIBBs) program of the National Science Foundation through grant NSF ACI-1640575.
 
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 
**/
package us.mtna.reporting.simple;
/**
 * Summary of variable counts to be used in {@linkplain DatasetComparisonStatistics}.  
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public class VariableStatistics {
	
	private int newVariableCount;
	private int droppedVariableCount;
	private int updatedVariableCount;
	private int matchedVariableCount;

	public VariableStatistics() {
	}

	public int getNewVariableCount() {
		return newVariableCount;
	}

	public void setNewVariableCount(int newVariableCount) {
		this.newVariableCount = newVariableCount;
	}

	public int getDroppedVariableCount() {
		return droppedVariableCount;
	}

	public void setDroppedVariableCount(int droppedVariableCount) {
		this.droppedVariableCount = droppedVariableCount;
	}

	public int getUpdatedVariableCount() {
		return updatedVariableCount;
	}

	public void setUpdatedVariableCount(int updatedVariableCount) {
		this.updatedVariableCount = updatedVariableCount;
	}

	public int getMatchedVariableCount() {
		return matchedVariableCount;
	}

	public void setMatchedVariableCount(int matchedVariableCount) {
		this.matchedVariableCount = matchedVariableCount;
	}

}

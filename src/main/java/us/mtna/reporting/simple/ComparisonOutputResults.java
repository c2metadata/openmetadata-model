/**
<Overview>
 
Copyright 2017 Metadata Technology North America Inc.
 
This software is the result of collaborative efforts from all participants of the C2Metadata project (http://www.c2metadata.org)
 
C2Metadata is supported by the Data Infrastructure Building Blocks (DIBBs) program of the National Science Foundation through grant NSF ACI-1640575.
 
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 
**/
package us.mtna.reporting.simple;

import java.util.ArrayList;
import java.util.List;

/**
 * Used in {@linkplain ComparisonOutput} and holds {@linkplain DataSetComparisonResults}s,
 * {@linkplain #variableComaprisonResults}, and
 * {@linkplain ClassificationComparisonResult}s about two datasets being
 * compared
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public class ComparisonOutputResults {

	private DataSetComparisonResults dataSetComparisonResults;
	private VariableComparisonResult variableComparisonResults;
	private List<ClassificationComparisonResult> classificationComparisonResults;

	public ComparisonOutputResults() {
		classificationComparisonResults = new ArrayList<>();
	}

	public VariableComparisonResult getVariableComparisonResults() {
		return variableComparisonResults;
	}

	public void setVariableComparisonResults(VariableComparisonResult variableComparisonResults) {
		this.variableComparisonResults = variableComparisonResults;
	}

	public DataSetComparisonResults getDataSetComparisonResults() {
		return dataSetComparisonResults;
	}

	public void setDataSetComparisonResults(DataSetComparisonResults dataSetComparisonResult) {
		this.dataSetComparisonResults = dataSetComparisonResult;
	}

	public List<ClassificationComparisonResult> getClassificationComparisonResults() {
		return classificationComparisonResults;
	}

	public void setClassificationComparisonResults(
			List<ClassificationComparisonResult> classificationComparisonResults) {
		this.classificationComparisonResults = classificationComparisonResults;
	}

	public void addClassificationComparisonResults(ClassificationComparisonResult... classificationComparisonResults) {
		for (ClassificationComparisonResult comparisonResult : classificationComparisonResults) {
			this.classificationComparisonResults.add(comparisonResult);
		}
	}

}

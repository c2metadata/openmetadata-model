/**
<Overview>
 
Copyright 2017 Metadata Technology North America Inc.
 
This software is the result of collaborative efforts from all participants of the C2Metadata project (http://www.c2metadata.org)
 
C2Metadata is supported by the Data Infrastructure Building Blocks (DIBBs) program of the National Science Foundation through grant NSF ACI-1640575.
 
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 
**/
package us.mtna.reporting.simple;

import java.util.ArrayList;
import java.util.List;

import us.mtna.reporting.ResultType;

/**
 * At Attribute Comparison Result provides details on the comparison of two
 * objects' attributes
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 * @param <O>
 */
public class AttributeComparisonResult<O extends Object> {

	private String name;
	private O sourceValue;
	private O targetValue;
	private boolean match;
	private ResultType difference;
	private List<AttributeComparisonResult<?>> attributes;
	//testing: list for summary statistics measures to be attached to the 
	//sumstats attribute overall comparison result 

	public AttributeComparisonResult() {
		attributes = new ArrayList<>();
	}
	
	public AttributeComparisonResult(String name, O sourceValue, O targetValue, boolean match, ResultType difference) {
		this.name = name;
		this.sourceValue = sourceValue;
		this.targetValue = targetValue;
		this.match = match;
		this.difference = difference;
	}

	/**
	 * create an attribute comparison result that includes a list of attribute comparison results 
	 */
	public AttributeComparisonResult(String name, O sourceValue, O targetValue, boolean match, ResultType difference,
			List<AttributeComparisonResult<?>> attributes) {
		this.name = name;
		this.sourceValue = sourceValue;
		this.targetValue = targetValue;
		this.match = match;
		this.difference = difference;
		this.attributes = new ArrayList<>();
	}

	public List<AttributeComparisonResult<?>> getAttributes() {
		return attributes;
	}

	public void setAttributes(List<AttributeComparisonResult<?>> attributes) {
		this.attributes.clear();
		this.attributes.addAll(attributes);
	}
	
	public void addAttributes(AttributeComparisonResult<?>...attributes){
		for(AttributeComparisonResult<?> acr : attributes){
			this.attributes.add(acr);
		}
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public O getSourceValue() {
		return sourceValue;
	}

	public void setSourceValue(O sourceValue) {
		this.sourceValue = sourceValue;
	}

	public O getTargetValue() {
		return targetValue;
	}

	public void setTargetValue(O targetValue) {
		this.targetValue = targetValue;
	}

	public boolean isMatch() {
		return match;
	}

	public void setMatch(boolean match) {
		this.match = match;
	}

	public ResultType getDifference() {
		return difference;
	}

	public void setDifference(ResultType difference) {
		this.difference = difference;
	}

}

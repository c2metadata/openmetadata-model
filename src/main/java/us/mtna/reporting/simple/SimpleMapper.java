/**
<Overview>
 
Copyright 2017 Metadata Technology North America Inc.
 
This software is the result of collaborative efforts from all participants of the C2Metadata project (http://www.c2metadata.org)
 
C2Metadata is supported by the Data Infrastructure Building Blocks (DIBBs) program of the National Science Foundation through grant NSF ACI-1640575.
 
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 
**/
package us.mtna.reporting.simple;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import us.mtna.config.VariablePair;
import us.mtna.pojo.Attribute;
import us.mtna.pojo.Classification;
import us.mtna.pojo.Code;
import us.mtna.pojo.SummaryStatistics;
import us.mtna.pojo.Variable;
import us.mtna.reporting.ComparisonResult;
import us.mtna.reporting.Report;
import us.mtna.reporting.ResultType;

/**
 * Map information from the {@linkplain Report} to {@linkplain ComparisonOutput}
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public class SimpleMapper {

	/**
	 * populate the final output object that will be returned in the comoparison request
	 * @param report
	 * @return ComparisonOutput
	 */
	public ComparisonOutput populateComparisonOutput(Report report) {
		ComparisonOutput comparisonOutput = new ComparisonOutput();
		ComparisonOutputResults results = new ComparisonOutputResults();

		ArrayList<ClassificationComparisonResult> classifications = populateClassificationsList(report);
		ArrayList<SimpleComparisonResult> updatedVariablesList = populateUpdatedVariablesList(report);
		DataSetComparisonResults dataSet = populateDataSetResult(report);

		VariableComparisonResult variableComparisonResults = populateVariableComparisonResult(report,
				updatedVariablesList);
		results.setDataSetComparisonResults(dataSet);
		results.setVariableComparisonResults(variableComparisonResults);
		results.setClassificationComparisonResults(classifications);

		ComparisonRequest request = new ComparisonRequest();
		DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.FULL);
		ZonedDateTime time = ZonedDateTime.now();
		request.setTimestamp(time.format(dateTimeFormatter));
		comparisonOutput.setService("DDI2.5 Dataset Comparison");
		comparisonOutput.setRequest(request);
		comparisonOutput.setResults(results);
		comparisonOutput.setConfig(report.getConfiguration());
		return comparisonOutput;
	}

	private VariableComparisonResult populateVariableComparisonResult(Report report,
			ArrayList<SimpleComparisonResult> variableComparisonResults) {
		VariableComparisonResult vcr = new VariableComparisonResult();
		List<ComparisonResult> varComparisonResults = report.getVarComparisonResultList();
		DataSetComparisonVariables dataSetVars = setDataSetComparisonVariables(varComparisonResults,
				variableComparisonResults);
		vcr.setVariables(dataSetVars);
		return vcr;
	}

	private DataSetComparisonResults populateDataSetResult(Report report) {
		DataSetComparisonResults dataSetComparisonResult = new DataSetComparisonResults();

		List<ComparisonResult> variableComparisonResults = report.getVarComparisonResultList();
		List<ComparisonResult> classifComparisonResults = report.getClassComparisonResultList();

		AttributeComparisonResult<?> dataSetNames = getDataSetNames(report);
		AttributeComparisonResult<?> dataSetClassificationCount = getClassificationCountComparisonResult(report);
		AttributeComparisonResult<?> dataSetRecordCount = getRecordCountComparisonResult(report);
		AttributeComparisonResult<?> dataSetVariableCount = getVariableCountComparisonResult(variableComparisonResults);

		ArrayList<AttributeComparisonResult<?>> dataSetAttributeList = new ArrayList<>();
		DataSetComparisonStatistics dataSetStatistics = computeDataSetStatistics(variableComparisonResults,
				classifComparisonResults);

		dataSetAttributeList.add(dataSetNames);
		dataSetAttributeList.add(dataSetVariableCount);
		dataSetAttributeList.add(dataSetClassificationCount);
		dataSetAttributeList.add(dataSetRecordCount);

		//set identifying keys (codebookId.fileDscrId) 
		dataSetComparisonResult.setSource(report.getDs1().getId());
		dataSetComparisonResult.setTarget(report.getDs2().getId());
		
		dataSetComparisonResult.setAttributes(dataSetAttributeList);
		dataSetComparisonResult.setStatistics(dataSetStatistics);
		return dataSetComparisonResult;
	}

	/**
	 * create a comparison result about the dataset names
	 * 
	 * @param report
	 * @return comparison result about dataset names
	 */
	private AttributeComparisonResult<?> getDataSetNames(Report report) {
		String ds1Name = report.getDs1().getName();
		String ds2Name = report.getDs2().getName();
		boolean match = ds1Name.equals(ds2Name);
		return new AttributeComparisonResult<>("dataSetNames", ds1Name, ds2Name, match, null);
	}

	/**
	 * populate a list of classification comparison results by first harmonizing
	 * classification comparisons from the original report (keeping only
	 * mismatches), then for each remaining comparison result, add comparisons
	 * about code counts and labels.
	 * 
	 * @param report
	 * @return list of classificationComparisonResults 
	 */
	@SuppressWarnings("unchecked")
	private ArrayList<ClassificationComparisonResult> populateClassificationsList(Report report) {

		ArrayList<ClassificationComparisonResult> classifications = new ArrayList<>();
		List<ComparisonResult> reportClassificationResults = report.getClassComparisonResultList();

		// remove any duplicate classification comparison results by mapping the
		// combination of the source and target classification IDs to the
		// comparison result
		Map<String, ComparisonResult> indexedClassificationResults = new LinkedHashMap<>();
		for (ComparisonResult comparisonResult : reportClassificationResults) {
			StringBuilder sb = new StringBuilder();
			if (comparisonResult.getSourceAttribute() != null) {
				Attribute<?> sourceClassification = comparisonResult.getSourceAttribute();
				sb.append(sourceClassification.getValue().toString());
			}
			if (comparisonResult.getTargetAttribute() != null) {
				Attribute<?> targetClassification = comparisonResult.getTargetAttribute();
				sb.append(targetClassification.getValue().toString());
			}
			// leave out classification comparisons that match
			if (comparisonResult.getType() != ResultType.MATCH) {
				indexedClassificationResults.put(sb.toString(), comparisonResult);
			}
		}

		// iterate through all the indexed (and de-duplicated results) and add
		// them
		for (ComparisonResult comparisonResult : indexedClassificationResults.values()) {
			int numberOfSourceCodes = 0;
			int numberOfTargetCodes = 0;

			ClassificationComparisonResult classificationResult = populateClassificationResult(
					report.getSrcVarToClassifMap(), report.getTargetVarToClassifMap(), comparisonResult);

			//////// code results ////////
			LinkedHashMap<String, SimpleComparisonResult> codes = new LinkedHashMap<>();
			Attribute<Code> sourceAttribute;
			Attribute<Code> targetAttribute;
			SimpleComparisonResult codeResult;

			for (ComparisonResult child : comparisonResult.getChildResults()) {
				String name;
				if (child.getSourceAttribute() != null) {
					name = child.getSourceAttribute().getName();
				} else {
					name = child.getTargetAttribute().getName();
				}
				if (name.equals(Classification.CODE_COUNT)) {
					numberOfSourceCodes = (Integer) child.getSourceAttribute().getValue();
					numberOfTargetCodes = (Integer) child.getTargetAttribute().getValue();

				} else {
					codeResult = new SimpleComparisonResult();
					sourceAttribute = (Attribute<Code>) child.getSourceAttribute();
					targetAttribute = (Attribute<Code>) child.getTargetAttribute();
					String codeValue = sourceAttribute != null ? sourceAttribute.getValue().getCodeValue()
							: targetAttribute.getValue().getCodeValue();
					AttributeComparisonResult<String> labelResult = new AttributeComparisonResult<>();
					labelResult.setName("label");
					if (targetAttribute != null) {
						labelResult.setTargetValue(targetAttribute.getValue().getLabel());
					}
					if (sourceAttribute != null) {
						labelResult.setSourceValue(sourceAttribute.getValue().getLabel());
					}
					if (child.getType() != null) {
						boolean match = isMatch(child.getType());
						labelResult.setMatch(match);
						labelResult.setDifference(child.getType());
						codeResult.setResultType(child.getType());
					}
					codeResult.addAttributes(labelResult);
					// only add codeResult when the label doesn't match
					if (codeResult.getResultType() != null && codeResult.getResultType() != ResultType.MATCH) {
						codes.put(codeValue, codeResult);
					}
				}
			}

			/////// classification attributes ///////
			ArrayList<AttributeComparisonResult<?>> classificationAttributes = new ArrayList<>();
			AttributeComparisonResult<?> codeCountComparison = new AttributeComparisonResult<>("codeCount",
					numberOfSourceCodes, numberOfTargetCodes, compareCounts(numberOfSourceCodes, numberOfTargetCodes),
					getDifference(numberOfSourceCodes, numberOfTargetCodes));
			classificationAttributes.add(codeCountComparison);
			classificationResult.setAttributes(classificationAttributes);
			classificationResult.setCodes(codes);
			classifications.add(classificationResult);
		}
		return classifications;
	}

	/**
	 * for each comparison result in populateClassificationsList (above), return
	 * a ClassificaitonComparisonResult that contains a source & target
	 * classification ids, match value, result type (if not a match), and
	 * information about variables that use the classifications: pairs of
	 * matching variables that use the same classification, additional variables
	 * with no matches, and counts of variables in each dataset that use the
	 * classification.
	 * 
	 * @param sourceVariableClassificationMap
	 * @param targetVariableClassificationMap
	 * @param comparisonResult
	 * @return classificationComparisonResult
	 */
	private ClassificationComparisonResult populateClassificationResult(
			Map<String, List<Variable>> sourceVariableClassificationMap,
			Map<String, List<Variable>> targetVariableClassificationMap, ComparisonResult comparisonResult) {
		ClassificationComparisonResult classificationResult = new ClassificationComparisonResult();

		// variable names: <lowercase, original>
		Map<String, String> sourceVariables = new LinkedHashMap<>();
		Map<String, String> targetVariables = new LinkedHashMap<>();

		if (comparisonResult.getSourceAttribute() != null) {
			Attribute<?> sourceClassification = comparisonResult.getSourceAttribute();
			classificationResult.setSource(sourceClassification.getValue().toString());
			if (sourceVariableClassificationMap.get(comparisonResult.getSourceAttribute().getValue()) != null) {
				// get the list of vars with that classif ID and add them to
				// sourceVariables
				Attribute<?> sourceAttribute = comparisonResult.getSourceAttribute();
				for (Variable variable : sourceVariableClassificationMap.get(sourceAttribute.getValue().toString())) {
					sourceVariables.put(variable.getName().toLowerCase(), variable.getName());
				}
			}
		}
		if (comparisonResult.getTargetAttribute() != null) {
			Attribute<?> targetClassification = comparisonResult.getTargetAttribute();
			classificationResult.setTarget(targetClassification.getValue().toString());
			if (targetVariableClassificationMap.get(comparisonResult.getTargetAttribute().getValue()) != null) {
				Attribute<?> targetAttribute = comparisonResult.getTargetAttribute();
				for (Variable variable : targetVariableClassificationMap.get(targetAttribute.getValue().toString())) {
					targetVariables.put(variable.getName().toLowerCase(), variable.getName());
				}
			}
		}
		// for all vars contained in both lists, pair them by name and add them
		// to a list of 2
		List<VariablePair> variablePairList = new ArrayList<>();
		List<String> sourceRemovals = new ArrayList<>();
		List<String> targetRemovals = new ArrayList<>();
		for (Entry<String, String> entry : sourceVariables.entrySet()) {
			if (targetVariables.containsKey(entry.getKey())) {
				// make a new object for each pair
				VariablePair pair = new VariablePair();
				pair.setSource(entry.getValue());
				pair.setTarget(targetVariables.get(entry.getKey()));
				variablePairList.add(pair);
				sourceRemovals.add(entry.getValue());
				targetRemovals.add(targetVariables.get(entry.getKey()));
			}
		}

		List<String> additionalSourceVariables = new ArrayList<>();
		List<String> additionalTargetVariables = new ArrayList<>();
		additionalSourceVariables.addAll(sourceVariables.values());
		additionalTargetVariables.addAll(targetVariables.values());
		additionalSourceVariables.removeAll(sourceRemovals);
		additionalTargetVariables.removeAll(targetRemovals);

		classificationResult.setMatch(isMatch(comparisonResult.getType()));
		classificationResult.setResultType(comparisonResult.getType());
		classificationResult.setVariablePairs(variablePairList);
		classificationResult.setAdditionalSourceVariables(additionalSourceVariables);
		classificationResult.setAdditionalTargetVariables(additionalTargetVariables);
		classificationResult.setSourceVariableCount(targetVariables.size());
		classificationResult.setTargetVariableCount(sourceVariables.size());
		return classificationResult;
	}

	/**
	 * create a list of comparison results about variables that were contained
	 * in both datasets, but did not match.
	 * 
	 * @param report
	 * @return a list of comparison results that will be used as the
	 *         updatedVariables list in DataSetComparisonVariables
	 */
	private ArrayList<SimpleComparisonResult> populateUpdatedVariablesList(Report report) {
		ArrayList<SimpleComparisonResult> variableComparisonResults = new ArrayList<>();

		for (ComparisonResult comparisonResult : report.getVarComparisonResultList()) {

			Attribute<?> sourceAttribute = comparisonResult.getSourceAttribute();
			Attribute<?> targetAttribute = comparisonResult.getTargetAttribute();
			Boolean match = isMatch(comparisonResult.getType());

			SimpleComparisonResult variableComparison = new SimpleComparisonResult();
			if (sourceAttribute != null) {
				variableComparison.setSource(sourceAttribute.getValue().toString());
			}
			if (targetAttribute != null) {
				variableComparison.setTarget(targetAttribute.getValue().toString());
			}
			variableComparison.setMatch(match);

			List<ComparisonResult> childResults = comparisonResult.getChildResults();
			if (!childResults.isEmpty()) {
				List<AttributeComparisonResult<?>> variableAttributes = new ArrayList<>();

				variableAttributes = addComparisonToVariableAttributeComparisonList(childResults, variableAttributes);
				variableComparison.setAttributes(variableAttributes);

				// check if child results have child results
				// (sumstats measures)
				for (ComparisonResult cr : childResults) {
					List<ComparisonResult> grandchildResults = cr.getChildResults();
					if (Variable.SUMMARY_STATS.equals(cr.getSourceAttribute().getName())
							&& !grandchildResults.isEmpty()) {
						// attribute comparison results about measures
						List<AttributeComparisonResult<?>> varAtts = addComparisonToVariableAttributeComparisonList(
								grandchildResults, new ArrayList<>());

						for (AttributeComparisonResult<?> att : variableAttributes) {
							// if it is a ss measure comparison,
							// add it as an attribute to the
							// overall summary statistics attribute comparison.
							if (att.getName().equals(Variable.SUMMARY_STATS)) {
								att.setAttributes(varAtts);
							}
						}
					}
				}
			}

			if (!variableComparison.getMatch()) {
				variableComparison.setResultType(comparisonResult.getType());

				// only add variables that are a mismatch
				switch (variableComparison.getResultType()) {
				case MATCH:
				case NO_TARGET:
				case NO_SOURCE:
					break;
				default:
					variableComparisonResults.add(variableComparison);
				}
			}
		}
		return variableComparisonResults;
	}

	/**
	 * create and populate the DataSetComparisonVariables object with a list of
	 * variable names for the new and dropped variables, and comparison results
	 * for the updated (mismatched) variables
	 * 
	 * @param varComparisons
	 * @param variableComaprisonResults
	 * @return populated DataSetComparisonVariables object
	 */
	public DataSetComparisonVariables setDataSetComparisonVariables(List<ComparisonResult> varComparisons,
			List<SimpleComparisonResult> variableComaprisonResults) {
		List<String> addedList = new ArrayList<>();
		List<String> deletedList = new ArrayList<>();

		for (ComparisonResult cr : varComparisons) {
			ResultType type = cr.getType();
			if (type == ResultType.NO_SOURCE) {
				addedList.add(cr.getTargetAttribute().getValue().toString());
			}
			if (type == ResultType.NO_TARGET) {
				deletedList.add(cr.getSourceAttribute().getValue().toString());
			}
		}

		DataSetComparisonVariables variables = new DataSetComparisonVariables();
		variables.setNewVariables(addedList);
		variables.setDroppedVariables(deletedList);
		variables.setUpdatedVariables(variableComaprisonResults);
		return variables;
	}

	/**
	 * create and populate a DataSetComparisonStatistics object from the
	 * comparison results of the report
	 * 
	 * @param varComparisons
	 * @param classifComparisons
	 * @return populated DataSetComparisonStatistics
	 */
	private DataSetComparisonStatistics computeDataSetStatistics(List<ComparisonResult> varComparisons,
			List<ComparisonResult> classifComparisons) {
		VariableStatistics variableStats = computeVariableStats(varComparisons);
		ClassificationStatistics classifStats = computeClassificationStats(classifComparisons);
		DataSetComparisonStatistics dataSetStatistics = new DataSetComparisonStatistics();
		dataSetStatistics.setClassificationStatistics(classifStats);
		dataSetStatistics.setVariableStatistics(variableStats);
		return dataSetStatistics;
	}

	/**
	 * create and populate a {@linkplain ClassificationStatistics} object to be
	 * used in the {@linkplain DataSetComparisonStatistics} in the summary
	 * section of the output
	 * 
	 * @param classifComparisons
	 * @return
	 */
	private ClassificationStatistics computeClassificationStats(List<ComparisonResult> classifComparisons) {
		int droppedClassificationCount = 0;
		int matchedClassificationCount = 0;
		int newClassificationCount = 0;
		int updatedClassificationCount = 0;
		Set<String> sourceClassifications = new HashSet<>();
		Set<String> targetClassifications = new HashSet<>();
		Set<String> matchedClassifications = new HashSet<>();
		for (ComparisonResult comparisonResult : classifComparisons) {
			StringBuilder sb = new StringBuilder();
			ResultType type = comparisonResult.getType();

			if (comparisonResult.getSourceAttribute() == null && comparisonResult.getSourceAttribute() != null) {
				Attribute<?> sourceClassification = comparisonResult.getSourceAttribute();
				boolean add = sourceClassifications.add(sourceClassification.getValue().toString());
				if (add) {
					newClassificationCount++;
				}
			}
			if (comparisonResult.getSourceAttribute() == null && comparisonResult.getTargetAttribute() != null) {
				Attribute<?> targetClassification = comparisonResult.getTargetAttribute();
				boolean add = targetClassifications.add(targetClassification.getValue().toString());
				if (add) {
					droppedClassificationCount++;
				}
			}
			if (comparisonResult.getSourceAttribute() != null && comparisonResult.getTargetAttribute() != null) {
				if (comparisonResult.getType() == ResultType.MATCH) {
					// hash classifications in the same order and add them to a
					// set - count unique entries
					Attribute<?> targetClassification = comparisonResult.getTargetAttribute();
					Attribute<?> sourceClassification = comparisonResult.getSourceAttribute();
					sb.append(sourceClassification.getValue().toString());
					sb.append(targetClassification.getValue().toString());
					boolean add = matchedClassifications.add(sb.toString());
					if (add) {
						matchedClassificationCount++;
					}
				} else if (type == ResultType.MISMATCH || type == ResultType.CASE_MISMATCH
						|| type == ResultType.CLOSE_MATCH || type == ResultType.SPACE_MISMATCH) {
					// all mismatching comparison results
					updatedClassificationCount++;
				}
			}
		}

		ClassificationStatistics classifStats = new ClassificationStatistics();
		classifStats.setDroppedClassificationCount(droppedClassificationCount);
		classifStats.setMatchedClassificationCount(matchedClassificationCount);
		classifStats.setNewClassificationCount(newClassificationCount);
		classifStats.setUpdatedClassificationCount(updatedClassificationCount);
		return classifStats;
	}

	/**
	 * compute the number of each type of variable based on result types
	 * found in the comparison results in the original report.
	 * @param varComparisons
	 * @return a populated VariableStatistics oject
	 */
	private VariableStatistics computeVariableStats(List<ComparisonResult> varComparisons) {
		int matched = 0;
		int dropped = 0;
		int updated = 0;
		int added = 0;
		for (ComparisonResult cr : varComparisons) {
			ResultType type = cr.getType();
			if (type == ResultType.NO_SOURCE) {
				added++;
			}
			if (type == ResultType.NO_TARGET) {
				dropped++;
			}
			if (type == ResultType.MATCH) {
				matched++;
			}
			if (type == ResultType.MISMATCH || type == ResultType.CASE_MISMATCH || type == ResultType.CLOSE_MATCH
					|| type == ResultType.SPACE_MISMATCH) {
				updated++;
			}
		}
		VariableStatistics variableStats = new VariableStatistics();
		variableStats.setDroppedVariableCount(dropped); // ResultType.NOTARGETATT
		variableStats.setMatchedVariableCount(matched); // ResultType.MATCH
		variableStats.setNewVariableCount(added); // ResultType.NOSRCATT
		variableStats.setUpdatedVariableCount(updated); // ResultType.MISMATCH
		return variableStats;
	}

	/**
	 * get a count of harmonized classifications from the
	 * classificationComparisonResults
	 * 
	 * @param report
	 * @return an attribute comparisonResult about classification count
	 */
	private AttributeComparisonResult<?> getClassificationCountComparisonResult(Report report) {
		List<ComparisonResult> classificationComparisonResults = report.getClassComparisonResultList();
		int sourceClassificationCount = 0;
		int targetClassificationCount = 0;

		Set<String> sourceClassifications = new HashSet<>();
		Set<String> targetClassifications = new HashSet<>();
		for (ComparisonResult comparisonResult : classificationComparisonResults) {
			StringBuilder sb = new StringBuilder();
			// increase src/targ counts if the comparison result added was not a duplicate
			if (comparisonResult.getSourceAttribute() != null) {
				Attribute<?> sourceClassification = comparisonResult.getSourceAttribute();
				boolean added = sourceClassifications.add(sourceClassification.getValue().toString());
				if (added) {
					sourceClassificationCount++;
				}
				sb.append(sourceClassification.getValue().toString());
			}
			if (comparisonResult.getTargetAttribute() != null) {
				Attribute<?> targetClassification = comparisonResult.getTargetAttribute();
				boolean added = targetClassifications.add(targetClassification.getValue().toString());
				if (added) {
					targetClassificationCount++;
				}
				sb.append(targetClassification.getValue().toString());
			}
		}

		return new AttributeComparisonResult<>("classificationCount", sourceClassificationCount,
				targetClassificationCount, compareCounts(sourceClassificationCount, targetClassificationCount),
				getDifference(sourceClassificationCount, targetClassificationCount));
	}

	private AttributeComparisonResult<?> getVariableCountComparisonResult(
			List<ComparisonResult> variableComparisonResults) {
		int sourceAttributeCount = 0;
		int targetAttributeCount = 0;
		for (ComparisonResult comparisonResult : variableComparisonResults) {
			if (comparisonResult.getSourceAttribute() != null) {
				sourceAttributeCount++;
			}
			if (comparisonResult.getTargetAttribute() != null) {
				targetAttributeCount++;
			}
		}
		return new AttributeComparisonResult<>("variableCount", sourceAttributeCount, targetAttributeCount,
				compareCounts(sourceAttributeCount, targetAttributeCount),
				getDifference(sourceAttributeCount, targetAttributeCount));
	}

	private AttributeComparisonResult<?> getRecordCountComparisonResult(Report report) {
		int numberOfSourceRecords = report.getDs1().getRecordCount();
		int numberOfTargetRecords = report.getDs2().getRecordCount();
		return new AttributeComparisonResult<>("recordCount", numberOfSourceRecords, numberOfTargetRecords,
				compareCounts(numberOfSourceRecords, numberOfTargetRecords),
				getDifference(numberOfSourceRecords, numberOfTargetRecords));
	}

	/**
	 * determine if two integers are equal
	 * 
	 * @param sourceAttributeCount
	 * @param targetAttributeCount
	 * @return
	 */
	private boolean compareCounts(int sourceAttributeCount, int targetAttributeCount) {
		boolean numbersMatch = false;
		if (targetAttributeCount == sourceAttributeCount) {
			numbersMatch = true;
		}
		return numbersMatch;
	}

	/**
	 * determine if two integers are greater than or less than and return the
	 * corresponding result type. if they are equal, a null result type is
	 * returned so that it will not be serialized
	 * 
	 * @param src
	 * @param target
	 * @return a result type of GREATER_THAN, LESS_THAN, or null
	 */
	private ResultType getDifference(int src, int target) {
		ResultType type = ResultType.MISMATCH;
		if (target > src) {
			type = ResultType.GREATER_THAN;
		}
		if (target < src) {
			type = ResultType.LESS_THAN;
		}
		if (target == src) {
			type = null;
		}
		return type;
	}

	/**
	 * return a list of attribute comparisons about variables from the report's
	 * list of childResults. also adds attribute comparison result to list of
	 * variable attributes if its a mismatch
	 * 
	 * @param childResults
	 * @param variableAttributes
	 * @return a list of attribute comparison results
	 */
	@SuppressWarnings("unchecked")
	private List<AttributeComparisonResult<?>> addComparisonToVariableAttributeComparisonList(
			List<ComparisonResult> childResults, List<AttributeComparisonResult<?>> variableAttributes) {
		for (ComparisonResult comparisonResult : childResults) {
			AttributeComparisonResult attributeComparisonResult = new AttributeComparisonResult();

			if (comparisonResult.getSourceAttribute() != null) {
				attributeComparisonResult.setSourceValue(comparisonResult.getSourceAttribute().getValue());
				attributeComparisonResult.setName(comparisonResult.getSourceAttribute().getName());
			}
			if (comparisonResult.getTargetAttribute() != null) {
				attributeComparisonResult.setTargetValue(comparisonResult.getTargetAttribute().getValue());
				attributeComparisonResult.setName(comparisonResult.getTargetAttribute().getName());
			}
			ResultType type = null;
			if ((type = comparisonResult.getType()) != null) {
				attributeComparisonResult.setMatch(isMatch(type));
				attributeComparisonResult.setDifference(type);
				// add the sumstats no matter what
				if (Variable.SUMMARY_STATS.equals(attributeComparisonResult.getName())
						&& !comparisonResult.getChildResults().isEmpty()) {
					attributeComparisonResult.setSourceValue(null);
					attributeComparisonResult.setTargetValue(null);
				}
				if (type != ResultType.MATCH && type != ResultType.NO_SOURCE && type != ResultType.NO_TARGET) {
					variableAttributes.add(attributeComparisonResult);

				} // a separate if block so that weight variables without
					// matches can be shown
				if (SummaryStatistics.WEIGHT_VARS.equals(attributeComparisonResult.getName())
						&& type != ResultType.MATCH) {
					variableAttributes.add(attributeComparisonResult);
				}
			}
		}
		return variableAttributes;
	}

	/**
	 * pass in a result type and determine if it is a match or not.
	 * 
	 * @param type
	 * @return boolean isMatch
	 */
	private boolean isMatch(ResultType type) {
		return ResultType.MATCH == type;
	}

}

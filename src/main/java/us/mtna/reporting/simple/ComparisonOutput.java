/**
<Overview>
 
Copyright 2017 Metadata Technology North America Inc.
 
This software is the result of collaborative efforts from all participants of the C2Metadata project (http://www.c2metadata.org)
 
C2Metadata is supported by the Data Infrastructure Building Blocks (DIBBs) program of the National Science Foundation through grant NSF ACI-1640575.
 
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 
**/
package us.mtna.reporting.simple;

import us.mtna.config.ComparisonConfig;

/**
 * This is the object that will be output when a call is made to compare
 * datasets. 
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public class ComparisonOutput {

	private String service;
	private String version;
	private ComparisonRequest request;
	private ComparisonOutputResults results;
	private ComparisonConfig config;

	public ComparisonOutput() {

	}

	public String getService() {
		return service;
	}

	public void setService(String service) {
		this.service = service;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public ComparisonRequest getRequest() {
		return request;
	}

	public void setRequest(ComparisonRequest request) {
		this.request = request;
	}

	public ComparisonOutputResults getResults() {
		return results;
	}

	public void setResults(ComparisonOutputResults results) {
		this.results = results;
	}

	public ComparisonConfig getConfig() {
		return config;
	}

	public void setConfig(ComparisonConfig config) {
		this.config = config;
	}

}

/**
<Overview>
 
Copyright 2017 Metadata Technology North America Inc.
 
This software is the result of collaborative efforts from all participants of the C2Metadata project (http://www.c2metadata.org)
 
C2Metadata is supported by the Data Infrastructure Building Blocks (DIBBs) program of the National Science Foundation through grant NSF ACI-1640575.
 
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 
**/
package us.mtna.reporting.simple;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import us.mtna.config.VariablePair;
import us.mtna.pojo.Classification;

/**
 * Provides details about the comparison of two {@linkplain Classification}s,
 * including the variables that use it, its codes, as well as all of the values
 * defined in {@linkplain SimpleComparisonResult}
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public class ClassificationComparisonResult extends SimpleComparisonResult {

	private List<VariablePair> variablePairs;
	private List<String> additionalSourceVariables;
	private List<String> additionalTargetVariables;
	private int sourceVariableCount;
	private int targetVariableCount;
	private LinkedHashMap<String, SimpleComparisonResult> codes;

	public ClassificationComparisonResult() {
		codes = new LinkedHashMap<>();
		variablePairs = new ArrayList<>();
	}

	public List<String> getAdditionalSourceVariables() {
		return additionalSourceVariables;
	}

	public void setAdditionalSourceVariables(List<String> additionalSourceVariables) {
		this.additionalSourceVariables=additionalSourceVariables;
	}
	
	public void addAdditionalSourceVariables(String...variables){
		for(String v : variables){
			this.additionalSourceVariables.add(v);
		}
	}

	public List<String> getAdditionalTargetVariables() {
		return additionalTargetVariables;
	}

	public void setAdditionalTargetVariables(List<String> additionalTargetVariables) {
		this.additionalTargetVariables=additionalTargetVariables;
	}
	
	public void addAdditionalTargetVariables(String...variables){
		for(String v : variables){
			this.additionalTargetVariables.add(v);
		}
	}

	public List<VariablePair> getVariablePairs() {
		return variablePairs;
	}

	public void setVariablePairs(List<VariablePair> variablePairs) {
		this.variablePairs.clear();
		this.variablePairs.addAll(variablePairs);
	}
	
	public void addVariableNamePair(VariablePair...pairs){
		for(VariablePair vp : pairs){
			this.variablePairs.add(vp);
		}
	}

	public int getSourceVariableCount() {
		return sourceVariableCount;
	}

	public void setSourceVariableCount(int sourceVariableCount) {
		this.sourceVariableCount = sourceVariableCount;
	}

	public int getTargetVariableCount() {
		return targetVariableCount;
	}

	public void setTargetVariableCount(int targetVariableCount) {
		this.targetVariableCount = targetVariableCount;
	}

	public Map<String, SimpleComparisonResult> getCodes() {
		return codes;
	}

	public void setCodes(Map<String, SimpleComparisonResult> codes) {
		this.codes.clear();
		this.codes.putAll(codes);
	}

	public void addCodes(LinkedHashMap<String, SimpleComparisonResult> codes) {
		for (Entry<String, SimpleComparisonResult> entry : codes.entrySet()) {
			this.codes.put(entry.getKey(), entry.getValue());
		}
	}

}

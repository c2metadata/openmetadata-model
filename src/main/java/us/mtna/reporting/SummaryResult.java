/**
<Overview>
 
Copyright 2017 Metadata Technology North America Inc.
 
This software is the result of collaborative efforts from all participants of the C2Metadata project (http://www.c2metadata.org)
 
C2Metadata is supported by the Data Infrastructure Building Blocks (DIBBs) program of the National Science Foundation through grant NSF ACI-1640575.
 
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 
**/
package us.mtna.reporting;

/**
 * Only one SummaryResult is added to each report. Gives summary info detailing
 * the number of unique variables in each dataset and a list of those variables,
 * as well as the total number of variables in each set.
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public class SummaryResult {

	private long uniqueSourceVariableCount;
	private long uniqueTargetVariableCount;
	private long totalSourceVariables;
	private long totalTargetVariables;

	public SummaryResult(long uniqueSourceVariableCount, long uniqueTargetVariableCount, long totalSourceVariables,
			long totalTargetVariables) {
		this.uniqueSourceVariableCount = uniqueSourceVariableCount;
		this.uniqueTargetVariableCount = uniqueTargetVariableCount;
		this.totalSourceVariables = totalSourceVariables;
		this.totalTargetVariables = totalTargetVariables;
	}
	
	public long getUniqueSourceVariableCount() {
		return uniqueSourceVariableCount;
	}

	public void setUniqueSourceVariableCount(long uniqueSourceVariableCount) {
		this.uniqueSourceVariableCount = uniqueSourceVariableCount;
	}

	public long getUniqueTargetVariableCount() {
		return uniqueTargetVariableCount;
	}

	public void setUniqueTargetVariableCount(long uniqueTargetVariableCount) {
		this.uniqueTargetVariableCount = uniqueTargetVariableCount;
	}

	public long getTotalSourceVariables() {
		return totalSourceVariables;
	}

	public void setTotalSourceVariables(long totalSourceVariables) {
		this.totalSourceVariables = totalSourceVariables;
	}

	public long getTotalTargetVariables() {
		return totalTargetVariables;
	}

	public void setTotalTargetVariables(long totalTargetVariables) {
		this.totalTargetVariables = totalTargetVariables;
	}
}

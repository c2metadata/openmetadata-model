/**
<Overview>
 
Copyright 2017 Metadata Technology North America Inc.
 
This software is the result of collaborative efforts from all participants of the C2Metadata project (http://www.c2metadata.org)
 
C2Metadata is supported by the Data Infrastructure Building Blocks (DIBBs) program of the National Science Foundation through grant NSF ACI-1640575.
 
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 
**/
package us.mtna.reporting;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import us.mtna.config.ComparisonConfig;
import us.mtna.pojo.DataSet;
import us.mtna.pojo.Variable;

/**
 * Holds lists of comparison results about variables and classifications, as well
 * as other information needed to pass to the simpleMapper to populate a
 * comparison output
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public class Report {

	private List<ComparisonResult> varComparisonResultList = new ArrayList<>();
	private List<ComparisonResult> classComparisonResultList = new ArrayList<>();
	private SummaryResult summaryResult;
	private DataSet ds1;
	private DataSet ds2;
	private Map<String, List<Variable>> srcVarToClassifMap;
	private Map<String, List<Variable>> targetVarToClassifMap;
	private int ds1RecordCount;
	private int ds2RecordCount;
	private ComparisonConfig configuration;

	public Report() {
		srcVarToClassifMap = new HashMap<>();
		targetVarToClassifMap = new HashMap<>();
	}

	public Report(List<ComparisonResult> varComparisonResultList) {
		this.varComparisonResultList = new ArrayList<>();
	}

	/**
	 * get a count of records in the source dataset
	 * 
	 * @return
	 */
	public int getSourceDsRecordCount() {
		return ds1RecordCount;
	}

	/**
	 * set record count for source dataset
	 * 
	 * @param ds1RecordCount
	 */
	public void setSourceDsRecordCount(int ds1RecordCount) {
		this.ds1RecordCount = ds1RecordCount;
	}

	/**
	 * get a count of records in the target datset
	 * 
	 * @return
	 */
	public int getTargetDsRecordCount() {
		return ds2RecordCount;
	}

	/**
	 * set the record count for the target dataset
	 * 
	 * @param ds2RecordCount
	 */
	public void setTargetDsRecordCount(int ds2RecordCount) {
		this.ds2RecordCount = ds2RecordCount;
	}

	/**
	 * get the source dataset from the report
	 * 
	 * @return
	 */
	public DataSet getDs1() {
		return ds1;
	}

	/**
	 * set the source dataset on the report
	 * 
	 * @param ds1
	 */
	public void setDs1(DataSet ds1) {
		this.ds1 = ds1;
	}

	/**
	 * get the target dataset from the report
	 * 
	 * @return
	 */
	public DataSet getDs2() {
		return ds2;
	}

	/**
	 * set the target dataset on the report.
	 * 
	 * @param ds2
	 */
	public void setDs2(DataSet ds2) {
		this.ds2 = ds2;
	}

	/**
	 * get a map of classification ids to a list of variables in the source
	 * dataset that use them
	 * 
	 * @return
	 */
	public Map<String, List<Variable>> getSrcVarToClassifMap() {
		return srcVarToClassifMap;
	}

	/**
	 * Set a list of variables from the source dataset that use the same
	 * classification. Key should be classification id
	 * 
	 * @param srcVarToClassifMap
	 */
	public void setSrcVarToClassifMap(Map<String, List<Variable>> srcVarToClassifMap) {
		this.srcVarToClassifMap.clear();
		this.srcVarToClassifMap.putAll(srcVarToClassifMap);
	}

	/**
	 * get a map of classification id to a list of variables that use that
	 * classification in the target dataset
	 * 
	 * @return
	 */
	public Map<String, List<Variable>> getTargetVarToClassifMap() {
		return targetVarToClassifMap;
	}

	/**
	 * Set a list of variables from the target dataset that use the same
	 * classification. Key should be classification id
	 * 
	 * @param srcVarToClassifMap
	 */
	public void setTargetVarToClassifMap(Map<String, List<Variable>> targetVarToClassifMap) {
		this.targetVarToClassifMap.clear();
		this.targetVarToClassifMap.putAll(targetVarToClassifMap);
	}

	/**
	 * get a list of ComparisonResults about variables in the datasets.
	 * 
	 * @return
	 */
	public List<ComparisonResult> getVarComparisonResultList() {
		return new ArrayList<ComparisonResult>(varComparisonResultList);
	}

	/**
	 * Set the vcrList all at once, or add individual lists with
	 * addVcrListToReport()
	 * 
	 * @param vcrList
	 */
	public void setVarComparisonResultList(List<ComparisonResult> varComparisonResultList) {
		this.varComparisonResultList = (varComparisonResultList == null) ? new ArrayList<>()
				: new ArrayList<>(varComparisonResultList);
	}

	/**
	 * get a list of ComparisonResults about classifications in the datasets
	 * 
	 * @return
	 */
	public List<ComparisonResult> getClassComparisonResultList() {
		return classComparisonResultList;
	}

	/**
	 * List of classification comparison results : Set the ccrList all at once,
	 * or add indiviual lists with addVcrListToReport()
	 * 
	 * @param vcr
	 */
	public void setClassComparisonResultList(List<ComparisonResult> classComparisonResultList) {
		this.classComparisonResultList = classComparisonResultList;
	}

	/**
	 * add a list of ComparisonResults about variables to the report's list of
	 * variable comparison results.
	 * 
	 * @param varComparisonResultList
	 */
	public void addVarComparisonResultListToReport(List<ComparisonResult> varComparisonResultList) {
		this.varComparisonResultList.addAll(varComparisonResultList);
	}

	/**
	 * add a list of ComparisonResults about classifications to the report's
	 * list of classification comparison results.
	 * 
	 * @param varComparisonResultList
	 */
	public void addClassComparisonResultListToReport(List<ComparisonResult> classComparisonResultList) {
		this.classComparisonResultList.addAll(classComparisonResultList);
	}

	public SummaryResult getSummaryResult() {
		return summaryResult;
	}

	public void setSummaryResult(SummaryResult summaryResult) {
		this.summaryResult = summaryResult;
	}

	/**
	 * get the comparison configuration used in the DatasetComparisonManagr to
	 * create a report
	 * 
	 * @return
	 */
	public ComparisonConfig getConfiguration() {
		return configuration;
	}

	/**
	 * set the comparison configuration that is to be used to create a report.
	 * 
	 * @param configuration
	 */
	public void setConfiguration(ComparisonConfig configuration) {
		this.configuration = configuration;
	}

}

/**
<Overview>
 
Copyright 2017 Metadata Technology North America Inc.
 
This software is the result of collaborative efforts from all participants of the C2Metadata project (http://www.c2metadata.org)
 
C2Metadata is supported by the Data Infrastructure Building Blocks (DIBBs) program of the National Science Foundation through grant NSF ACI-1640575.
 
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 
**/
package us.mtna.reporting;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import us.mtna.config.ComparisonConfig;
import us.mtna.config.VariablePair;
import us.mtna.pojo.Attribute;
import us.mtna.pojo.Classification;
import us.mtna.pojo.Code;
import us.mtna.pojo.DataSet;
import us.mtna.pojo.Metadata;
import us.mtna.pojo.SummaryStatistics;
import us.mtna.pojo.Variable;

/**
 * A test class for comparing two data set objects. Here, it compares test data
 * sets from {@link DataSets} and populates a {@link Report} with
 * {@link ComparisonResult}s about Classifications and Variables. Users can
 * compare single variables with
 * {@link #getVariableComparisonResult(Variable, Variable)}, and single
 * classifications with {@link #compareCodes(Classification, Classification)}
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public class DataSetComparisonManager {

	// private Log log = LogFactory.getLog(getClass());
	private ComparisonConfig configuration;

	/**
	 * get a report about variables and classifications in the two data sets
	 * you're comparing using variable automatching
	 * 
	 * @param ds1
	 * @param ds2
	 * @return {@link Report} about the differences between the two datasets
	 */
	public Report compareDataSets(DataSet ds1, DataSet ds2, ComparisonConfig configuration) {
		Metadata metadata1 = ds1.getMetadata();
		Metadata metadata2 = ds2.getMetadata();
		Report report = new Report();
		int ds1RecordCount = ds1.getRecordCount();
		int ds2RecordCount = ds2.getRecordCount();
		report.setConfiguration(configuration);
		report.setDs1(ds1);
		report.setDs2(ds2);
		report.setSourceDsRecordCount(ds1RecordCount);
		report.setTargetDsRecordCount(ds2RecordCount);
		if (configuration == null) {
			configuration = new ComparisonConfig();
		} // determine whether to use automatching or variable pairs
		if (!configuration.getVariableComparison().getVariablePairs().isEmpty()) {
			compareVariablePairs(metadata1, metadata2, report, configuration);
		} else {
			compareVariablesWithAutomatching(metadata1, metadata2, report, configuration);
		}
		return report;
	}

	/**
	 * compare the codes of two variables' classifications and add the
	 * comparison result to a list
	 * 
	 * @param metadata1
	 * @param metadata2
	 * @param v1
	 * @param v2
	 * @return
	 */
	private List<ComparisonResult> getClassifComparisonResults(Metadata metadata1, Metadata metadata2, Variable v1,
			Variable v2) {
		List<ComparisonResult> classificationComparisonResultList = new ArrayList<>();
		Classification c1 = metadata1.lookupClassificationById(v1.getClassificationId());
		Classification c2 = metadata2.lookupClassificationById(v2.getClassificationId());

		if (c1 != null & c2 != null) {
			ComparisonResult classificationComparisonResult = compareCodes(c1, c2);
			classificationComparisonResultList.add(classificationComparisonResult);
		}
		return classificationComparisonResultList;
	}

	/**
	 * compare variables based on pre-matched pairs passed into the
	 * configuration, and add results to report
	 * 
	 * @param metadata1
	 * @param metadata2
	 * @param report
	 * @param configuration
	 */
	public void compareVariablePairs(Metadata metadata1, Metadata metadata2, Report report,
			ComparisonConfig configuration) {
		List<ComparisonResult> comparisonResultList = new ArrayList<>();
		// map of all dataset variables to their classifications
		Map<String, Variable> variableNameMap1 = metadata1.getVarNameMap();
		Map<String, Variable> variableNameMap2 = metadata2.getVarNameMap();
		Map<String, List<Variable>> sourceVariableClassificationMap = new HashMap<>();
		Map<String, List<Variable>> targetVariableClassificationMap = new HashMap<>();
		ComparisonResult comparisonResult;
		Variable v1;
		Variable v2;

		List<VariablePair> variablePairs = configuration.getVariableComparison().getVariablePairs();
		for (VariablePair pair : variablePairs) {

			v1 = variableNameMap1.get(pair.getSource());
			v2 = variableNameMap2.get(pair.getTarget());
			comparisonResult = compareVariableAttributes(v1, v2, configuration);
			comparisonResultList.add(comparisonResult);

			// compare classifications
			if (configuration.getClassificationComparison().isCompared()) {
				List<ComparisonResult> classificationComparisonResultList = getClassifComparisonResults(metadata1,
						metadata2, v1, v2);
				sourceVariableClassificationMap = populateVarClassifMap(v1, sourceVariableClassificationMap);
				targetVariableClassificationMap = populateVarClassifMap(v2, targetVariableClassificationMap);
				report.setTargetVarToClassifMap(targetVariableClassificationMap);
				report.setSrcVarToClassifMap(sourceVariableClassificationMap);
				report.addClassComparisonResultListToReport(classificationComparisonResultList);
			}
		}
		report.addVarComparisonResultListToReport(comparisonResultList);
	}

	/**
	 * match variables based on name, then compare their attributes and
	 * classifications.
	 * 
	 * @param metadata1
	 * @param metadata2
	 * @param report
	 * @param configuration
	 */
	public void compareVariablesWithAutomatching(Metadata metadata1, Metadata metadata2, Report report,
			ComparisonConfig configuration) {
		ComparisonResult comparisonResult;
		List<Variable> ds1Variables = metadata1.getVariables();
		List<Variable> ds2Variables = metadata2.getVariables();
		List<String> uniqueSourceVariables = new ArrayList<>();
		List<Variable> uniqueTargetVariables = new ArrayList<>();
		List<Variable> targetVarsToRemove = new ArrayList<>();
		List<ComparisonResult> comparisonResultList = new ArrayList<>();
		List<ComparisonResult> classificationComparisonResultList = new ArrayList<>();
		Map<String, List<Variable>> sourceVariableClassificationMap = new HashMap<>();
		Map<String, List<Variable>> targetVariableClassificationMap = new HashMap<>();
		Map<String, Variable> ds2Vars = new HashMap<>();
		Map<String, Variable> ds2VarsLower = new HashMap<>();
		Map<String, String> ds2LowerCase = new HashMap<>();
		Map<String, String> ds2Spacing = new HashMap<>();
		Map<String, String> ds2SpecialChars = new HashMap<>();
		Map<String, String> ds2CaseSpace = new HashMap<>();
		Map<String, String> ds2CaseSpaceChars = new HashMap<>();
		ResultType type;
		Attribute<String> v1Name;
		Attribute<String> v2Name;
		for (Variable v2 : ds2Variables) {
			// make lists of ds2 variable names with different formatting
			// combinations for determining mismatches

			ds2VarsLower.put(v2.getName().toLowerCase(), v2);
			ds2Vars.put(v2.getName(), v2);
			targetVariableClassificationMap = populateVarClassifMap(v2, targetVariableClassificationMap);
			// lowercase all characters
			ds2LowerCase.put(v2.getName().toLowerCase(), v2.getName());
			// replace all spaces with empty string
			ds2Spacing.put(v2.getName().replaceAll("\\s+", ""), v2.getName());
			// replace all non-alphanumeric characters with an empty string
			ds2SpecialChars.put(v2.getName().replaceAll("[^A-Za-z0-9]", ""), v2.getName());
			// lowercase all & replace all spaces with empty string
			ds2CaseSpace.put(v2.getName().toLowerCase().replaceAll("\\s+", ""), v2.getName());
			// replace all space&special chars with empty string, lowercase all
			ds2CaseSpaceChars.put(v2.getName().toLowerCase().replaceAll("/[^A-Za-z0-9]+/", ""), v2.getName());
		}
		// for every var in ds1, see if v2 matches on a variety of formats, then
		// create a comparison result
		for (Variable v1 : ds1Variables) {
			sourceVariableClassificationMap = populateVarClassifMap(v1, sourceVariableClassificationMap);
			v1Name = v1.getNameAttr();

			if (ds2LowerCase.values().contains(v1.getName())) {
				// original variable names match
				Variable v2 = ds2VarsLower.get(v1.getName().toLowerCase());
				// compare classifications and attributes
				Classification c1 = metadata1.lookupClassificationById(v1.getClassificationId());
				Classification c2 = metadata2.lookupClassificationById(v2.getClassificationId());

				if (configuration.getClassificationComparison().isCompared()) {
					if (c1 != null & c2 != null) {
						if (!c1.getCodeList().isEmpty() && !c2.getCodeList().isEmpty()) {
							ComparisonResult classificationComparisonResult = compareCodes(c1, c2);
							classificationComparisonResultList.add(classificationComparisonResult);
						}
					}
				}
				comparisonResult = compareVariableAttributes(v1, v2, configuration);
				comparisonResultList.add(comparisonResult);
				// get v2 object from its name
				String name = ds2LowerCase.get(v1.getName().toLowerCase());
				Variable v = ds2Vars.get(name);
				targetVarsToRemove.add(v);
			} else if (ds2Spacing.containsKey(v1.getName().replaceAll("\\s+", ""))) {
				if (!configuration.getVariableComparison().getLabelComparison().isSpaceMismatch()) {
					type = ResultType.MISMATCH;
				} else {
					type = ResultType.SPACE_MISMATCH;
				}
				String name = ds2Spacing.get(v1.getName().replaceAll("\\s+", ""));
				Variable v = ds2Vars.get(name);
				v2Name = v.getNameAttr();
				targetVarsToRemove.add(v);
				comparisonResult = new ComparisonResult(v1Name, v2Name, type);
				comparisonResultList.add(comparisonResult);

			} else if (ds2LowerCase.containsKey(v1.getName().toLowerCase())) {
				if (!configuration.getVariableComparison().getLabelComparison().isCaseMismatch()) {
					type = ResultType.MISMATCH;
				} else {
					type = ResultType.CASE_MISMATCH;
				}
				String name = ds2LowerCase.get(v1.getName().toLowerCase());
				Variable v = ds2Vars.get(name);
				v2Name = v.getNameAttr();
				targetVarsToRemove.add(v);
				comparisonResult = new ComparisonResult(v1Name, v2Name, type);
				comparisonResultList.add(comparisonResult);
			}
			// TODO make sure close match accounts for char mismatch combos as
			// well
			else if (ds2SpecialChars.containsKey(v1.getName().replaceAll("[^A-Za-z0-9]", ""))) {
				if (!configuration.getVariableComparison().getLabelComparison().isSpecialCharMismatch()) {
					type = ResultType.MISMATCH;
				} else {
					type = ResultType.SPECIAL_CHAR_MISMATCH;
				}
				String name = ds2SpecialChars.get(v1.getName().replaceAll("[^A-Za-z0-9]", ""));
				Variable v = ds2Vars.get(name);
				v2Name = v.getNameAttr();
				targetVarsToRemove.add(v);
				comparisonResult = new ComparisonResult(v1Name, v2Name, type);
				comparisonResultList.add(comparisonResult);
			}
			// if its not one of the above mismatches, but does match when var
			// is lowercased / all spaces & special characters are removed,
			// it's a close match
			else if (ds2CaseSpaceChars.containsKey(v1.getName().toLowerCase().replaceAll("/[^A-Za-z0-9]+/", ""))) {

				if (!configuration.getVariableComparison().getLabelComparison().isCloseMatch()) {
					type = ResultType.MISMATCH;
				} else {
					type = ResultType.CLOSE_MATCH;
				}
				String name = ds2CaseSpace.get(v1.getName().toLowerCase().replaceAll("\\s+", ""));
				Variable v = ds2Vars.get(name);
				v2Name = v.getNameAttr();
				targetVarsToRemove.add(v);
				comparisonResult = new ComparisonResult(v1Name, v2Name, type);
				comparisonResultList.add(comparisonResult);

			} else {
				// variable only exists in dataset1
				uniqueSourceVariables.add(v1.getName().toLowerCase());
				type = ResultType.NO_TARGET;
				v2Name = null;
				comparisonResult = new ComparisonResult(v1Name, v2Name, type);
				comparisonResultList.add(comparisonResult);
			}
		}
		// for every v2 that matched, take out of a list of ds2Vars
		// variable only exists in dataset 2
		uniqueTargetVariables.addAll(ds2Variables);
		uniqueTargetVariables.removeAll(targetVarsToRemove);
		for (Variable v : uniqueTargetVariables) {
			comparisonResult = new ComparisonResult(null, v.getNameAttr(), ResultType.NO_SOURCE);
			comparisonResultList.add(comparisonResult);
		}
		SummaryResult summaryResult = new SummaryResult(uniqueSourceVariables.size(), uniqueTargetVariables.size(),
				ds1Variables.size(), ds2Variables.size());
		report.setSrcVarToClassifMap(sourceVariableClassificationMap);
		report.setTargetVarToClassifMap(targetVariableClassificationMap);
		report.setSummaryResult(summaryResult);
		report.addClassComparisonResultListToReport(classificationComparisonResultList);
		report.addVarComparisonResultListToReport(comparisonResultList);
	}

	/**
	 * return a map of a variable's classification id to a list of all the
	 * variables that also use that id
	 * 
	 * @param v
	 * @return
	 */
	private Map<String, List<Variable>> populateVarClassifMap(Variable v,
			Map<String, List<Variable>> variableClassificationMap) {
		List<Variable> variableList = variableClassificationMap.getOrDefault(v.getClassificationId(),
				new ArrayList<Variable>());
		variableList.add(v);
		variableClassificationMap.put(v.getClassificationId(), variableList);
		return variableClassificationMap;
	}

	/**
	 * compares variable's attributes using {@linkplain compareAttributes}, then
	 * decides if 2 variables are a match based on those results
	 * 
	 * @param v1
	 * @param v2
	 * @return
	 */
	public ComparisonResult compareVariableAttributes(Variable v1, Variable v2, ComparisonConfig configuration) {
		List<Attribute<?>> v1Attributes = v1.getAttributes();
		List<Attribute<?>> v2Attributes = v2.getAttributes();
		List<ComparisonResult> childList = new ArrayList<>();
		List<ComparisonResult> attributeComparisonResults = new ArrayList<>();
		Map<String, Attribute<?>> v1AttributeMap = populateAttMap(v1Attributes);
		Map<String, Attribute<?>> v2AttributeMap = populateAttMap(v2Attributes);
		ResultType resultType = ResultType.MATCH;

		for (Entry<String, Attribute<?>> entry : v1AttributeMap.entrySet()) {
			if (v2AttributeMap.containsKey(entry.getKey())) {
				Attribute<?> sourceAttribute = entry.getValue();
				Attribute<?> targetAttribute = v2AttributeMap.get(entry.getKey());
				attributeComparisonResults = compareAttributes(sourceAttribute, targetAttribute, configuration);

				// if any of the attributes didn't match up, the whole variable
				// is now a mismatch
				for (ComparisonResult cr : attributeComparisonResults) {
					if (cr.getType() != ResultType.MATCH) {
						resultType = ResultType.MISMATCH;
					}
				}
			}
			childList.addAll(attributeComparisonResults);
		}
		ComparisonResult comparisonResult = new ComparisonResult(v1.getNameAttr(), v2.getNameAttr(), resultType);
		comparisonResult.addChildResultsToList(childList);
		return comparisonResult;
	}

	/**
	 * Populates a map with a classification's code and label, then compares
	 * codelist size and contents. Both of these are added as childresults. The
	 * overall classification comparison result is determined by the results of
	 * the child results.
	 * 
	 * @param c1
	 * @param c2
	 * @return
	 */
	public ComparisonResult compareCodes(Classification c1, Classification c2) {
		Map<String, Attribute<Code>> c1Map = populateMapWithCodesAndLabels(c1);
		Map<String, Attribute<Code>> c2Map = populateMapWithCodesAndLabels(c2);
		List<ComparisonResult> childList = new ArrayList<>();
		childList.add(compareClassificationSize(c1, c2));
		childList.addAll(compareCodeMaps(c1Map, c2Map));

		ResultType resultType = ResultType.MATCH;
		for (ComparisonResult child : childList) {
			if (child.getType() != ResultType.MATCH) {
				resultType = ResultType.MISMATCH;
				break;
			}
		}

		// create final classification result
		ComparisonResult cr = new ComparisonResult(c1.getIdAttribute(), c2.getIdAttribute(), resultType);
		cr.addChildResultsToList(childList);
		return cr;
	}

	/**
	 * Compare code labels based on their codes, then the label string. return a
	 * list of comparison results
	 * 
	 * @param c1Map
	 * @param c2Map
	 * @return
	 */
	private List<ComparisonResult> compareCodeMaps(Map<String, Attribute<Code>> c1Map,
			Map<String, Attribute<Code>> c2Map) {

		List<ComparisonResult> crList = new ArrayList<>();
		List<String> removals = new ArrayList<>();

		// compare labels based on codes first
		for (Entry<String, Attribute<Code>> entry : c1Map.entrySet()) {
			Attribute<Code> sourceCode = entry.getValue();

			// if both contain the code, then look at the label attached to both
			if (c2Map.containsKey(entry.getKey())) {
				Attribute<Code> targetCode = c2Map.get(entry.getKey());

				ResultType resultType = compareCodes(sourceCode.getValue(), targetCode.getValue());
				crList.add(new ComparisonResult(sourceCode, targetCode, resultType));
				removals.add(entry.getKey());
			}
		}

		for (String key : removals) {
			c1Map.remove(key);
			c2Map.remove(key);
		}

		for (Attribute<Code> code : c1Map.values()) {
			crList.add(new ComparisonResult(code, null, ResultType.NO_TARGET));
		}

		for (Attribute<Code> code : c2Map.values()) {
			crList.add(new ComparisonResult(null, code, ResultType.NO_SOURCE));
		}
		return crList;
	}

	/**
	 * check if codes are a match, mismatch, close match, or space/case mismatch
	 * based on their labels
	 * 
	 * @param code1
	 * @param code2
	 * @return
	 */
	private ResultType compareCodes(Code code1, Code code2) {
		// TODOO if code labels are both null should they show a mismatch?
		if (code1.getLabel() != null && code2.getLabel() != null) {
			if (compareCodeLabels(code1, code2))
				return ResultType.MATCH;

			if (compareCodeLabelsIgnoreCase(code1, code2))
				return ResultType.CASE_MISMATCH;

			if (compareCodeLabelsIgnoreSpace(code1, code2))
				return ResultType.SPACE_MISMATCH;

			if (compareCodeLabelsIgnoreSpecialCharacters(code1, code2))
				return ResultType.SPECIAL_CHAR_MISMATCH;

			if (compareCodeLabelsIgnoreCaseSpaceChars(code1, code2))
				return ResultType.CLOSE_MATCH;
		} else if (code1.getLabel() == null && code2.getLabel() == null) {
			return ResultType.MATCH;
		}
		return ResultType.MISMATCH;
	}

	private boolean compareCodeLabels(Code code1, Code code2) {
		return code1.getLabel().equals(code2.getLabel());
	}

	private boolean compareCodeLabelsIgnoreCase(Code code1, Code code2) {
		return code1.getLabel().equalsIgnoreCase(code2.getLabel());
	}

	private boolean compareCodeLabelsIgnoreSpace(Code code1, Code code2) {
		String label1 = code1.getLabel().replaceAll("\\s+", "");
		String label2 = code2.getLabel().replaceAll("\\s+", "");
		return label1.equals(label2);
	}

	private boolean compareCodeLabelsIgnoreSpecialCharacters(Code code1, Code code2) {
		String label1 = code1.getLabel().replaceAll("[^A-Za-z0-9]", "");
		String label2 = code2.getLabel().replaceAll("[^A-Za-z0-9]", "");
		return label1.equals(label2);
	}

	// if you use this, you'd have to make combos for every type of mismatch?
	private boolean compareCodeLabelsIgnoreCaseAndSpace(Code code1, Code code2) {
		String label1 = code1.getLabel().replaceAll("\\s+", "");
		String label2 = code2.getLabel().replaceAll("\\s+", "");
		return label1.equalsIgnoreCase(label2);
	}

	private boolean compareCodeLabelsIgnoreCaseSpaceChars(Code code1, Code code2) {
		String label1 = code1.getLabel().replaceAll("/[^A-Za-z0-9]+/", "");
		String label2 = code2.getLabel().replaceAll("/[^A-Za-z0-9]+/", "");
		return label1.equals(label2);
	}

	/**
	 * populate a map with <code value, code label> for a single classification
	 * 
	 * @param c
	 * @return
	 */
	private Map<String, Attribute<Code>> populateMapWithCodesAndLabels(Classification c) {
		Map<String, Attribute<Code>> codeLabelMap = new HashMap<>();
		if (c.getCodes() != null) {
			for (Attribute<Code> code : c.getCodes()) {
				codeLabelMap.put(code.getValue().getCodeValue(), code);
			}
		}
		return codeLabelMap;
	}

	/**
	 * compare code counts between two classifications. returns a comparison
	 * result of type match, greater_than, or less_than
	 * 
	 * @param c1
	 * @param c2
	 * @return
	 */
	private ComparisonResult compareClassificationSize(Classification c1, Classification c2) {
		int c1Size = c1.getCodeCount().getValue();
		int c2Size = c2.getCodeCount().getValue();
		ResultType resultType = ResultType.MATCH;
		if (c1Size != c2Size) {
			if (c1Size < c2Size) {
				// Target is greater than source
				resultType = ResultType.GREATER_THAN;
			} else {
				resultType = ResultType.LESS_THAN;
			}
		}
		return new ComparisonResult(c1.getCodeCount(), c2.getCodeCount(), resultType);
	}

	/**
	 * Compare class, name, and value of two attributes with a comparison result
	 * for each property. if the attribute is a SummaryStastic, do a deeper
	 * comparison of each measure
	 * 
	 * @return List<ComparisonResult> comparisonResultList - this will hold a
	 *         comparison about every property of attributes. Users can then
	 *         detemine what to display in the simpleMapper
	 */
	private List<ComparisonResult> compareAttributes(Attribute<?> attribute1, Attribute<?> attribute2,
			ComparisonConfig configuration) {
		List<ComparisonResult> comparisonResultList = new ArrayList<>();
		String reportString = "";
		ComparisonResult childResult;
		// if classes dont match, make a comparison result about them.
		if (!attribute1.getClazz().equals(attribute2.getClazz())) {
			reportString += "The class for the Attribute in DataSet1 is " + attribute1.getClazz() + " & "
					+ "The class for the Attribute in Data Set 2 is " + attribute2.getClazz() + ". "
					+ System.lineSeparator();
			childResult = new ComparisonResult(attribute1, attribute2, ResultType.MISMATCH);
			comparisonResultList.add(childResult);
		} else {
			// commented out for now due to filtering issues in the simple
			// mapper
			// childResult = new ComparisonResult(attribute1, attribute2,
			// ResultType.MATCH);
			// comparisonResultList.add(childResult);
		}
		if (!attribute1.getName().equals(attribute2.getName())) {
			reportString += "The name for the Attribute in DataSet1 is " + attribute1.getName() + " & "
					+ "The name for the Attribute in Data Set 2 is " + attribute2.getName() + ". "
					+ System.lineSeparator();
			childResult = new ComparisonResult(attribute1, attribute2, ResultType.MISMATCH);
			comparisonResultList.add(childResult);

		} else {
			// childResult = new ComparisonResult(attribute1, attribute2,
			// ResultType.MATCH);
			// comparisonResultList.add(childResult);
		}

		// if attributes are not summaryStatistics, compare values
		// regularly
		if (!attribute1.getName().equals(Variable.SUMMARY_STATS)) {
			if (!attribute1.getValue().equals(attribute2.getValue())) {
				reportString += "The value for the Attribute in DataSet 1 is " + attribute1.getValue().toString()
						+ " & " + "The value for the Attribute in DataSet 2 is " + attribute2.getValue().toString()
						+ ". " + System.lineSeparator();
				childResult = new ComparisonResult(attribute1, attribute2, ResultType.MISMATCH);
				comparisonResultList.add(childResult);
			} else {
				// childResult = new ComparisonResult(attribute1, attribute2,
				// ResultType.MATCH);
				// comparisonResultList.add(childResult);
			}

		} else {
			// if it is a sumStat, do a deeper comparison
			if (configuration.getVariableComparison().getSumStatsComparison().isCompared()) {
				List<ComparisonResult> statisticalMeasuresComparisons = new ArrayList<>();
				// add statisticalMeasureComparisons as a child result
				statisticalMeasuresComparisons = compareSummaryStatisticsMeasures(attribute1, attribute2,
						statisticalMeasuresComparisons, configuration);

				ResultType rt = ResultType.MATCH;
				for (ComparisonResult cr : statisticalMeasuresComparisons) {
					if (cr.getType() != ResultType.MATCH) {
						rt = ResultType.MISMATCH;
					}
				}
				childResult = new ComparisonResult(attribute1, attribute2, rt);
				childResult.addChildResultsToList(statisticalMeasuresComparisons);
				comparisonResultList.add(childResult);
			}
		}
		if (reportString != "") {
			// log.info(reportString);
		}
		return comparisonResultList;
	}

	/**
	 * specifically used to compare weighted variable references. cast the
	 * attributes to a hashset, and determine if the
	 * 
	 * @param weightedVariableNamesAttribute1
	 * @param weightedVariableNamesAttribute2
	 * @param statisticalMeasuresComparisons
	 */
	@SuppressWarnings("unchecked")
	private void compareWeightedVarRefs(Attribute<?> weightedVariableNamesAttribute1,
			Attribute<?> weightedVariableNamesAttribute2, List<ComparisonResult> statisticalMeasuresComparisons) {

		ResultType resultType = ResultType.MISMATCH;
		HashSet<String> wv1 = (HashSet<String>) weightedVariableNamesAttribute1.getValue();
		HashSet<String> wv2 = (HashSet<String>) weightedVariableNamesAttribute2.getValue();
		if (!wv1.isEmpty() || !wv2.isEmpty()) {

			// DDI2.5 can only have 1 weighted var per dataset, so if both lists
			// contain the variable, they match.
			if (!wv1.isEmpty() && !wv2.isEmpty()) {
				for (String att : wv1) {
					if (wv2.contains(att)) {
						resultType = ResultType.MATCH;
					}
				}
			} else if (wv1.isEmpty() && !wv2.isEmpty()) {
				resultType = ResultType.NO_SOURCE;
			} else if (wv2.isEmpty() && !wv1.isEmpty()) {
				resultType = ResultType.NO_TARGET;
			} else {
				resultType = ResultType.MISMATCH;
			}

			statisticalMeasuresComparisons.add(
					new ComparisonResult(weightedVariableNamesAttribute1, weightedVariableNamesAttribute2, resultType));

		}
	}

	/**
	 * produce a child result about the values of a statistical measure across
	 * datasets only if both are non-null values, then add it to the list of
	 * statistical measure comparisons
	 * 
	 * @param measure1
	 * @param measure2
	 * @param m1Att
	 * @param m2Att
	 * @param statisticalMeasuresComparisons
	 */
	public void compareStatisticalMeasures(Double measure1, Double measure2, Attribute<?> m1Att, Attribute<?> m2Att,
			List<ComparisonResult> statisticalMeasuresComparisons) {
		ComparisonResult childResult = new ComparisonResult(null, null, null);
		if (measure1 != null && measure2 != null) {
			if (Double.compare(measure1, measure2) == 0) {
				childResult = new ComparisonResult(m1Att, m2Att, ResultType.MATCH);
			} else if (Double.compare(measure1, measure2) < 0) {
				childResult = new ComparisonResult(m1Att, m2Att, ResultType.GREATER_THAN);
			} else if (Double.compare(measure1, measure2) > 0) {
				childResult = new ComparisonResult(m1Att, m2Att, ResultType.LESS_THAN);
			}
			statisticalMeasuresComparisons.add(childResult);
		}
	}

	/**
	 * For each statistical measure, first check to see if it is set to be
	 * compared in the configuration. Then, compare summary statistics
	 * attributes by individual measure. if both variables contain the same
	 * measure (non-null), then the values for that measure will be compared and
	 * a comparison result created. Comparisons are based on double values, and
	 * value comparisons are in the format "target value is greater than/less
	 * than/equal to source value"
	 * 
	 * @param attribute1
	 * @param attribute2
	 * @param statisticalMeasuresComparisons
	 * @return List<ComparisonResult> statisticalMeasuresComparisons: a list of
	 *         comparisons about measures contained in both datasets
	 */
	private List<ComparisonResult> compareSummaryStatisticsMeasures(Attribute<?> attribute1, Attribute<?> attribute2,
			List<ComparisonResult> statisticalMeasuresComparisons, ComparisonConfig configure) {
		SummaryStatistics sumStats1 = (SummaryStatistics) attribute1.getValue();
		SummaryStatistics sumStats2 = (SummaryStatistics) attribute2.getValue();

		if (configure.getVariableComparison().getSumStatsComparison().isUnweighted()) {
			// add child result to statMeasureComparisons for each measure
			if (configure.getVariableComparison().getSumStatsComparison().isMax()) {
				compareStatisticalMeasures(sumStats1.getMax(), sumStats2.getMax(), sumStats1.getMaxAttribute(),
						sumStats2.getMaxAttribute(), statisticalMeasuresComparisons);
			}
			if (configure.getVariableComparison().getSumStatsComparison().isMin()) {
				compareStatisticalMeasures(sumStats1.getMin(), sumStats2.getMin(), sumStats1.getMinAttribute(),
						sumStats2.getMinAttribute(), statisticalMeasuresComparisons);
			}
			if (configure.getVariableComparison().getSumStatsComparison().isMean()) {
				compareStatisticalMeasures(sumStats1.getMean(), sumStats2.getMean(), sumStats1.getMeanAttribute(),
						sumStats2.getMeanAttribute(), statisticalMeasuresComparisons);
			}
			if (configure.getVariableComparison().getSumStatsComparison().isMode()) {
				compareStatisticalMeasures(sumStats1.getMode(), sumStats2.getMode(), sumStats1.getModeAttribute(),
						sumStats2.getModeAttribute(), statisticalMeasuresComparisons);
			}
			if (configure.getVariableComparison().getSumStatsComparison().isInvalid()) {
				compareStatisticalMeasures(sumStats1.getInvalid(), sumStats2.getInvalid(),
						sumStats1.getInvalidAttribute(), sumStats2.getInvalidAttribute(),
						statisticalMeasuresComparisons);
			}
			if (configure.getVariableComparison().getSumStatsComparison().isValid()) {
				compareStatisticalMeasures(sumStats1.getValid(), sumStats2.getValid(), sumStats1.getValidAttribute(),
						sumStats2.getValidAttribute(), statisticalMeasuresComparisons);
			}
			if (configure.getVariableComparison().getSumStatsComparison().isStdDev()) {
				compareStatisticalMeasures(sumStats1.getStdDev(), sumStats2.getStdDev(), sumStats1.getStdDevAttribute(),
						sumStats2.getStdDevAttribute(), statisticalMeasuresComparisons);
			}
			if (configure.getVariableComparison().getSumStatsComparison().isOther()) {
				compareStatisticalMeasures(sumStats1.getOther(), sumStats2.getOther(), sumStats1.getOtherAttribute(),
						sumStats2.getOtherAttribute(), statisticalMeasuresComparisons);
			}
		}
		// compare weighted measures if configuration specifies it
		if (configure.getVariableComparison().getSumStatsComparison().isWeighted()) {

			// first compare the sets of weight variables
			compareWeightedVarRefs(sumStats1.getWeightedVariableNamesAttribute(),
					sumStats2.getWeightedVariableNamesAttribute(), statisticalMeasuresComparisons);

			if (configure.getVariableComparison().getSumStatsComparison().isMax()) {
				compareStatisticalMeasures(sumStats1.getWeightedMax(), sumStats2.getWeightedMax(),
						sumStats1.getWeightedMaxAttribute(), sumStats2.getWeightedMaxAttribute(),
						statisticalMeasuresComparisons);
			}

			if (configure.getVariableComparison().getSumStatsComparison().isMin()) {
				compareStatisticalMeasures(sumStats1.getWeightedMin(), sumStats2.getWeightedMin(),
						sumStats1.getWeightedMinAttribute(), sumStats2.getWeightedMinAttribute(),
						statisticalMeasuresComparisons);
			}

			if (configure.getVariableComparison().getSumStatsComparison().isMean()) {
				compareStatisticalMeasures(sumStats1.getWeightedMean(), sumStats2.getWeightedMean(),
						sumStats1.getWeightedMeanAttribute(), sumStats2.getWeightedMeanAttribute(),
						statisticalMeasuresComparisons);
			}

			if (configure.getVariableComparison().getSumStatsComparison().isMode()) {
				compareStatisticalMeasures(sumStats1.getWeightedMode(), sumStats2.getWeightedMode(),
						sumStats1.getWeightedModeAttribute(), sumStats2.getWeightedModeAttribute(),
						statisticalMeasuresComparisons);
			}

			if (configure.getVariableComparison().getSumStatsComparison().isInvalid()) {
				compareStatisticalMeasures(sumStats1.getWeightedInvalid(), sumStats2.getWeightedInvalid(),
						sumStats1.getWeightedInvalidAttribute(), sumStats2.getWeightedInvalidAttribute(),
						statisticalMeasuresComparisons);
			}

			if (configure.getVariableComparison().getSumStatsComparison().isValid()) {
				compareStatisticalMeasures(sumStats1.getWeightedValid(), sumStats2.getWeightedValid(),
						sumStats1.getWeightedValidAttribute(), sumStats2.getWeightedValidAttribute(),
						statisticalMeasuresComparisons);
			}

			if (configure.getVariableComparison().getSumStatsComparison().isStdDev()) {
				compareStatisticalMeasures(sumStats1.getWeightedStdDev(), sumStats2.getWeightedStdDev(),
						sumStats1.getWeightedStdDevAttribute(), sumStats2.getWeightedStdDevAttribute(),
						statisticalMeasuresComparisons);
			}

			if (configure.getVariableComparison().getSumStatsComparison().isOther()) {
				compareStatisticalMeasures(sumStats1.getWeightedOther(), sumStats2.getWeightedOther(),
						sumStats1.getWeightedOtherAttribute(), sumStats2.getWeightedOtherAttribute(),
						statisticalMeasuresComparisons);
			}
		}
		return statisticalMeasuresComparisons;
	}

	/**
	 * populate a hashmap with attribute name and attribute
	 * 
	 * @param variableAttributes
	 * @return
	 */
	private Map<String, Attribute<?>> populateAttMap(List<Attribute<?>> variableAttributes) {
		Map<String, Attribute<?>> variableAtttributeMap = new HashMap<>();
		for (Attribute<?> attribute : variableAttributes) {
			variableAtttributeMap.put(attribute.getName(), attribute);
		}
		return variableAtttributeMap;
	}

	public ComparisonConfig getConfiguration() {
		return configuration;
	}

	public void setConfiguration(ComparisonConfig configuration) {
		this.configuration = configuration;
	}

}
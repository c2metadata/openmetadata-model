/**
<Overview>
 
Copyright 2017 Metadata Technology North America Inc.
 
This software is the result of collaborative efforts from all participants of the C2Metadata project (http://www.c2metadata.org)
 
C2Metadata is supported by the Data Infrastructure Building Blocks (DIBBs) program of the National Science Foundation through grant NSF ACI-1640575.
 
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 
**/
package us.mtna.reporting;

import java.util.ArrayList;
import java.util.List;

import us.mtna.pojo.Attribute;
import us.mtna.pojo.Code;

/**
 * Holds the source {@link #sourceAttribute} and target {@link #targetAttribute}
 * attributes you're comparing, and the result's {@link #resultType}.
 * ComparisonResults are then added to {@link Report}. {@link ChildResults} are
 * a sublist of results attached to the ComparisonResult that give details about
 * the result - used to report on {@link Code}s for Classification comparisons
 * or {@link Attribute}s for Variable comparisons.
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public class ComparisonResult {

	private Attribute<?> sourceAttribute;
	private Attribute<?> targetAttribute;
	private ResultType resultType;
	private List<ComparisonResult> childResults;

	public ComparisonResult(Attribute<?> sourceAttribute, Attribute<?> targetAttribute, ResultType resultType) {
		this.sourceAttribute = sourceAttribute;
		this.targetAttribute = targetAttribute;
		this.resultType = resultType;
		this.childResults = new ArrayList<>();
	}
	
	public Attribute<?> getSourceAttribute() {
		return sourceAttribute;
	}

	public void setSourceAttribute(Attribute<?> sourceAttribute) {
		this.sourceAttribute = sourceAttribute;
	}

	public Attribute<?> getTargetAttribute() {
		return targetAttribute;
	}

	public void setTargetAttribute(Attribute<?> targetAttribute) {
		this.targetAttribute = targetAttribute;
	}
	/**
	 * get the ResultType of the comparison
	 * @return
	 */
	public ResultType getType() {
		return resultType;
	}

	/**
	 * set the ResultType for the comparison 
	 * @param type
	 */
	public void setType(ResultType type) {
		this.resultType = type;
	}

	public List<ComparisonResult> getChildResults() {
		return childResults;
	}

	public void setChildResults(List<ComparisonResult> childResults) {
		this.childResults = childResults;
	}

	/**
	 * add a single childResult to the master list
	 * 
	 * @param childResult
	 */
	public void addChildResultToList(ComparisonResult childResult) {
		this.childResults.add(childResult);
	}

	/**
	 * add a list of chldResults to the master list
	 * 
	 * @param crList
	 */
	public void addChildResultsToList(List<ComparisonResult> childResultList) {
		this.childResults = new ArrayList<>();
		this.childResults.addAll(childResultList);
	}

}

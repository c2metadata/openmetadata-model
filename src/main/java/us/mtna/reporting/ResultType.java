/**
<Overview>
 
Copyright 2017 Metadata Technology North America Inc.
 
This software is the result of collaborative efforts from all participants of the C2Metadata project (http://www.c2metadata.org)
 
C2Metadata is supported by the Data Infrastructure Building Blocks (DIBBs) program of the National Science Foundation through grant NSF ACI-1640575.
 
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 
**/
package us.mtna.reporting;

/**
 * Gives more information about a {@linkplain ComparisonResult}.
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public enum ResultType {
	/**
	 * The attribute does not have a source attribute to compare to
	 */
	NO_SOURCE,
	/**
	 * The attribute does not have a target attribute to compare to
	 */
	NO_TARGET,
	/**
	 * The values were compared, but did not match
	 */
	MISMATCH,
	/**
	 * A type of {@link #MISMATCH} indicating that this attribute is greater
	 * than the one it is compared to. Intended to be used with
	 * {@link SummaryStatistics}
	 */
	GREATER_THAN,
	/**
	 * A type of {@link #MISMATCH} indicating that this attribute is less than
	 * the one it is compared to. Intended to be used with
	 * {@link SummaryStatistics}
	 */
	LESS_THAN,
	/**
	 * Labels match, but their case is different
	 */
	CASE_MISMATCH,
	/**
	 * Labels match when multiple spaces are reduces to a single space
	 */
	SPACE_MISMATCH,
	/**
	 * Labels match when special characters (not including numbers) are removed
	 */
	SPECIAL_CHAR_MISMATCH,
	/**
	 * If values have more than one type of mismatch (CASE_MISMATCH and
	 * SPACE_MISMATCH, e.g.), use this value instead of specifying every
	 * combination of mismatches
	 */
	CLOSE_MATCH,
	/**
	 * The two values match at the specified level
	 */
	MATCH,
	/**
	 * Generally reserved for things that have gone wrong as opposed to
	 * comparisons. (Empty DataSet, e.g.)
	 */
	ERROR
}
